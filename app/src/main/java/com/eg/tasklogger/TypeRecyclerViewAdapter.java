// Package and libraries used
package com.eg.tasklogger;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

/**
 * CLASS
 *
 * NAME
 *      TypeRecyclerViewAdapter
 *
 * SYNOPSIS
 *      public class TypeRecyclerViewAdapter
 *          extends RecyclerView.Adapter<TypeRecyclerViewAdapter.TypeViewHolder>
 *
 * DESCRIPTION
 *      This class is an adapter that links the recycler view to an array holding all task types.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 3/16/2019
 */
class TypeRecyclerViewAdapter
        extends RecyclerView.Adapter<TypeRecyclerViewAdapter.TypeViewHolder>
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Context m_context; // Context to be used by recycler view
    private List<Type> m_type; // Array/List of Type type
    private int m_lastPosition = -1; // Allows to remember the last item shown on screen

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * CLASS
     *
     * NAME
     *      TypeViewHolder --> Helper class which holds task type data.
     *
     * SYNOPSIS
     *      public static class TypeViewHolder extends RecyclerView.ViewHolder
     *
     * DESCRIPTION
     *      This class holds task type data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    static class TypeViewHolder extends RecyclerView.ViewHolder
    {

        private TextView mt_name;
        private TextView mt_usedTime;
        private TextView mt_Color;
        private LinearLayout mt_parentLayout;

        /**
         * METHOD
         *
         * NAME
         *      TypeViewHolder --> Class constructor.
         *
         * SYNOPSIS
         *      TypeViewHolder(@NonNull View itemView)
         *      @param a_itemView --> View: View holding task type data
         *
         * DESCRIPTION
         *      This method is the class constructor for the inner class.
         *
         * RETURNS
         *      void
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 3/16/2019
         */
        TypeViewHolder(@NonNull View a_itemView)
        {
            super(a_itemView);

            // Data to display
            mt_name = a_itemView.findViewById(R.id.typeItem_name);
            mt_usedTime =  a_itemView.findViewById(R.id.typeItem_description);
            mt_Color =  a_itemView.findViewById(R.id.typeItem_Color);
            mt_parentLayout = a_itemView.findViewById(R.id.layoutTypeItem);
        }
    }

    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      TypeRecyclerViewAdapter --> Class constructor.
     *
     * SYNOPSIS
     *      public TypeRecyclerViewAdapter(Context a_context, List<Type> a_type)
     *      @param a_context --> Context: Context to set
     *      @param a_type --> List<Type>: List/Array of Type objects
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    TypeRecyclerViewAdapter(Context a_context, List<Type> a_type)
    {
        this.m_context = a_context;
        this.m_type = a_type;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateViewHolder --> Android Studio generated method, this method executes
     *          when the view holder is created.
     *
     * SYNOPSIS
     *      public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup,
     *          int a_viewType)
     *      @param a_viewGroup --> ViewGroup: Group of views.
     *      @param a_viewType --> int: Type of view.
     *
     * DESCRIPTION
     *      This method executes when the view holder is created.
     *
     * RETURNS
     *      @return TypeViewHolder: View Holder for Types.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    @NonNull
    @Override
    public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup, int a_viewType)
    {
        View view;
        view = LayoutInflater.from(m_context).inflate(R.layout.fragitem_type
                ,a_viewGroup,false);
        return new TypeViewHolder(view);
    }

    /**
     * METHOD
     *
     * NAME
     *      onBindViewHolder --> Android Studio generated method, this method executes
     *          when the view is bound.
     *
     * SYNOPSIS
     *      public void onBindViewHolder(@NonNull final TypeViewHolder a_holder,
     *          final int a_position)
     *      @param a_holder --> TypeViewHolder: View Holder for Types.
     *      @param a_position --> int: Index in list.
     *
     * DESCRIPTION
     *      This method executes  when the view is bound.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    @Override
    public void onBindViewHolder(@NonNull final TypeViewHolder a_holder, final int a_position)
    {
        // Task type data
        a_holder.mt_name.setText(m_type.get(a_position).Getm_name());

        int timesUsed = m_type.get(a_position).Getm_usedTimes();
        String usedTimes = "Used " + Integer.toString(timesUsed)
                + ((timesUsed == 1)? " time " : " times ");
        a_holder.mt_usedTime.setText(usedTimes);

        GradientDrawable drawable = (GradientDrawable) a_holder.mt_Color.getBackground();
        drawable.setColor(m_type.get(a_position).Getm_color());

        // Animation when the view is bound
        SetAnimation(a_holder.itemView, a_position);

        a_holder.mt_parentLayout.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the user presses on a Task Type item on the list.
             *      The Edit activity is called with a message that includes the action type,
             *      Type record ID, and the position in the array/recycler of the item clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view){
                Intent myIntent = new Intent(m_context, TypeAEActivity.class);
                String message = "edit|" +  Integer.toString(m_type.get(a_position).Getm_id())
                        + "|" + Integer.toString(a_position);
                myIntent.putExtra("message",message);
                m_context.startActivity(myIntent);

            }
        });
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      getItemCount --> Android Studio generated method, this method returns the number
     *          of task types in the recycler.
     *
     * SYNOPSIS
     *      public int getItemCount()
     *
     * DESCRIPTION
     *      This method returns the number of task types in the recycler.
     *
     * RETURNS
     *      @return int --> Number of task types in the recycler.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    @Override
    public int getItemCount()
    {
        return m_type.size();
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SetAnimation --> Sets an animation when a new bound view is added to the recycler.
     *
     * SYNOPSIS
     *      private void SetAnimation(View a_viewToAnimate, int a_position)
     *      @param a_viewToAnimate --> View: View to animate when bound.
     *      @param a_position --> int: Index number.
     *
     * DESCRIPTION
     *      This method sets a animation that executes when a new bound view is added
     *      to the recycler.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    private void SetAnimation(View a_viewToAnimate, int a_position)
    {
        // If the bound view wasn't previously displayed on screen, animate it
        if (a_position > m_lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(m_context, android.R.anim.slide_in_left);
            a_viewToAnimate.startAnimation(animation);
            m_lastPosition = a_position;
        }
    }
}
