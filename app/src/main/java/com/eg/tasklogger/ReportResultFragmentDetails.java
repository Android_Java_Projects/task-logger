// Package and libraries used
package com.eg.tasklogger;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import static com.eg.tasklogger.TLApplication.COMPLETELOG;
import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;
import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;

/**
 * CLASS
 *
 * NAME
 *      ReportResultFragmentDetails
 *
 * SYNOPSIS
 *      public class ReportResultFragmentDetails extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Detailed Report Results fragment.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/23/2019
 */
public class ReportResultFragmentDetails extends Fragment
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private List<Log> m_lstLog;                         // Holds all the log data
    private LogRecyclerViewAdapter m_recyclerAdapter;   // Adapter that links recycler to data array
    DatabaseHelper m_databaseHelper;                    // Database connector

    // 3rd: UI Components ---------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      ReportResultFragmentDetails --> Class constructor.
     *
     * SYNOPSIS
     *      public ReportResultFragmentDetails()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/23/2019
     */
    public ReportResultFragmentDetails()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *          when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/23/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_report_result_fragment_details,
                a_container, false);

        // Initialize class
        Initialize(view);

        // Inflate the layout for this fragment
        return view;
    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the fragment becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the fragment becomes active. It checks if there is
     *      any "pending" data that will update the array/recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/24/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Check if a should be updated in the recycler
        if (getActivity() != null)
        {
            int pendingRecordID = ((TLApplication) getActivity()
                    .getApplication()).Getm_pendingRecordID();
            if (pendingRecordID != NOTANINDEXNORID) UpdateRecycler(pendingRecordID);
        }
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      UpdateRecycler --> Updates the recycler view.
     *
     * SYNOPSIS
     *      private void UpdateRecycler(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the array related to the recycler view. It also refreshes the
     *      Recycler adapter to display the updated data.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    private void UpdateRecycler(int a_pendingRecordID)
    {
        int pendingIndex = 0;
        int pendingAction = 0;

        if (getActivity() != null)
        {
            pendingIndex = ((TLApplication) getActivity().getApplication()).Getm_pendingIndex();
            pendingAction = ((TLApplication) getActivity().getApplication()).Getm_pendingAction();
        }

        switch (pendingAction)
        {
            case EDITRECORD:
                Log editedLog = m_databaseHelper.GetLog(a_pendingRecordID);
                m_lstLog.set(pendingIndex,editedLog);
                break;

            case DELETERECORD:
                m_lstLog.remove(pendingIndex);
                break;
        }

        // Update recycler and reset pending data.
        m_recyclerAdapter.notifyDataSetChanged();
        ((TLApplication) getActivity().getApplication()).ResetPending();
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        m_databaseHelper = new DatabaseHelper(getContext());

        // Get the From and To dates stored in the activity holding this fragment
        String fromDate="";
        String toDate="";
        if (getActivity() != null)
        {
            fromDate = ((ReportResultActivity) getActivity()).Getm_fromDate();
            toDate = ((ReportResultActivity) getActivity()).Getm_toDate();
        }

        // Set member variables
        m_lstLog = new ArrayList<>();
        m_lstLog = m_databaseHelper.GetMultipleLogs(COMPLETELOG,fromDate,
                toDate, "ASC");

        // Set the Recycler/List
        RecyclerView recyclerView = a_view.findViewById(R.id.recyclerView_ReportResultCompleted);
        m_recyclerAdapter = new LogRecyclerViewAdapter(getContext(),m_lstLog,
                0, COMPLETELOG );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(m_recyclerAdapter);
    }
}
