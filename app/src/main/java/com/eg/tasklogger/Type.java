// Package and libraries used
package com.eg.tasklogger;

/**
 * CLASS
 *
 * NAME
 *      Type
 *
 * SYNOPSIS
 *      public class Type
 *
 * DESCRIPTION
 *      This class is used to encapsulate task Type information.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 3/16/2019
 */
public class Type
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private int m_id;  // Task type id
    private String m_name;  // Task type name
    private int m_usedTimes;  // How many times this type has been used
    private int m_color;  // Task type color
    private String m_colorHex; // Task type color in hex string

    // 3rd: UI Components ---------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Type --> Class constructor.
     *
     * SYNOPSIS
     *      public Type()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    public Type()
    {
        this.m_id = 0;
        this.m_name = "";
        this.m_usedTimes = 0;
        this.m_color = 0;
        this.m_colorHex = "";
    }

    /**
     * METHOD
     *
     * NAME
     *      Type --> Overloaded Class constructor.
     *
     * SYNOPSIS
     *      public Type(int a_id, String a_name, int a_usedTimes, int a_color, String a_colorHex)
     *      @param a_id --> int: Task type id.
     *      @param a_name --> String: Task type name to set.
     *      @param a_usedTimes --> int: How many times this type has been used.
     *      @param a_color --> int: Integer representing a color.
     *      @param a_colorHex --> String: String representing a Hex color.
     *
     * DESCRIPTION
     *      This method is an overloaded class constructor. Used to set member variables on
     *      instantiation.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    public Type(int a_id, String a_name, int a_usedTimes, int a_color, String a_colorHex)
    {
        this.m_id = a_id;
        this.m_name = a_name;
        this.m_usedTimes = a_usedTimes;
        this.m_color = a_color;
        this.m_colorHex = a_colorHex;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Getm_Id --> Returns the type id.
     *
     * SYNOPSIS
     *      public int Getm_id()
     *
     * DESCRIPTION
     *      This method returns the task type id.
     *
     * RETURNS
     *      @return int --> Task type id.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    public int Getm_id()
    {
        return m_id;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_name --> Returns the type name.
     *
     * SYNOPSIS
     *      public String Getm_name()
     *
     * DESCRIPTION
     *      This method returns the task type name.
     *
     * RETURNS
     *      @return String --> Task type name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    public String Getm_name()
    {
        return m_name;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_usedTimes --> Returns the number of times this type has been used.
     *
     * SYNOPSIS
     *      public int Getm_usedTimes()
     *
     * DESCRIPTION
     *      This method returns the number of times this type has been used.
     *
     * RETURNS
     *      @return int --> Number of times this type has been used.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    int Getm_usedTimes()
    {
        return m_usedTimes;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_color --> Returns the task type color.
     *
     * SYNOPSIS
     *      public int Getm_color()
     *
     * DESCRIPTION
     *      This method returns the task type color.
     *
     * RETURNS
     *      @return int --> Number representing the task type color.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    int Getm_color()
    {
        return m_color;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_colorHex --> Returns the task type Hex color as a string.
     *
     * SYNOPSIS
     *      public String Getm_colorHex()
     *
     * DESCRIPTION
     *      This method returns the task type Hex color as a string.
     *
     * RETURNS
     *      @return String --> String representing the task type Hex color.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    String Getm_colorHex()
    {
        return m_colorHex;
    }

    // 7th: Mutators --------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Setm_id --> Sets the task type id.
     *
     * SYNOPSIS
     *      public void Setm_id(int a_id)
     *      @param a_id --> int: Task type id.
     *
     * DESCRIPTION
     *      This method sets the task type id.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    void Setm_id(int a_id)
    {
        this.m_id = a_id;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_name --> Sets the task type name.
     *
     * SYNOPSIS
     *      public void Setm_name(String a_name)
     *      @param a_name --> String: Task type name.
     *
     * DESCRIPTION
     *      This method sets the task type name.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    void Setm_name(String a_name)
    {
        this.m_name = a_name;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_usedTimes --> Sets the number of times this task type has been used.
     *
     * SYNOPSIS
     *      Setm_usedTimes(int a_usedTimes)
     *      @param a_usedTimes --> String: Number of times this task type has been used.
     *
     * DESCRIPTION
     *      This method sets the number of times this task type has been used.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    void Setm_usedTimes(int a_usedTimes)
    {
        this.m_usedTimes = a_usedTimes;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_color --> Sets the task type color.
     *
     * SYNOPSIS
     *      public void Setm_color(int a_color)
     *      @param a_color --> int: Number representing the task type color.
     *
     * DESCRIPTION
     *      This method sets the task type color.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    void Setm_color(int a_color)
    {
        this.m_color = a_color;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_colorHex --> Sets the task type Hex color as a string.
     *
     * SYNOPSIS
     *      Setm_colorHex(String a_color)
     *      @param a_colorHex --> String: String representing the task type hex color.
     *
     * DESCRIPTION
     *      This method Sets the task type Hex color as a string.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    void Setm_colorHex(String a_colorHex)
    {
        this.m_colorHex = a_colorHex;
    }

    // 8th: Any utility (private) methods -----------------------------------------------------

}
