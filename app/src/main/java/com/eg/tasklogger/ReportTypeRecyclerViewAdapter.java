// Package and libraries used
package com.eg.tasklogger;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import java.util.List;

/**
 * CLASS
 *
 * NAME
 *      ReportTypeRecyclerViewAdapter
 *
 * SYNOPSIS
 *      class ReportTypeRecyclerViewAdapter
 *         extends RecyclerView.Adapter<ReportTypeRecyclerViewAdapter.TypeViewHolder>
 *
 * DESCRIPTION
 *      This class is an adapter that links the recycler view to list holding ReportType objects.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/26/2019
 */
class ReportTypeRecyclerViewAdapter
        extends RecyclerView.Adapter<ReportTypeRecyclerViewAdapter.TypeViewHolder>
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Context m_context;          // Context to be used by recycler view
    private List<ReportType> m_type;    // Array/List of ReportType objects
    private int m_lastPosition;         // Allows to remember the last item shown on screen

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * CLASS
     *
     * NAME
     *      TypeViewHolder --> Helper class which holds report type data.
     *
     * SYNOPSIS
     *      static class TypeViewHolder extends RecyclerView.ViewHolder
     *
     * DESCRIPTION
     *      This class holds report type data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    static class TypeViewHolder extends RecyclerView.ViewHolder
    {

        private TextView mt_Color;
        private TextView mt_name;
        private TextView mt_totalTime;

        /**
         * METHOD
         *
         * NAME
         *      TypeViewHolder --> Class constructor.
         *
         * SYNOPSIS
         *      public TypeViewHolder(@NonNull View itemView)
         *      @param a_itemView --> View: View holding report type data
         *
         * DESCRIPTION
         *      This method is the class constructor for the inner class.
         *
         * RETURNS
         *      void
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 4/26/2019
         */
        TypeViewHolder(@NonNull View a_itemView)
        {
            super(a_itemView);

            // Data to display
            mt_Color =  a_itemView.findViewById(R.id.typeItem_Color);
            mt_name = a_itemView.findViewById(R.id.typeItem_name);
            mt_totalTime =  a_itemView.findViewById(R.id.typeItem_description);
        }
    }

    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      ReportTypeRecyclerViewAdapter --> Class constructor.
     *
     * SYNOPSIS
     *      ReportTypeRecyclerViewAdapter(Context a_context, List<ReportType> a_reportType)
     *      @param a_context --> Context: Context to set
     *      @param a_reportType --> List<ReportType>: List of ReportType objects
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    ReportTypeRecyclerViewAdapter(Context a_context, List<ReportType> a_reportType)
    {
        this.m_context = a_context;
        this.m_type = a_reportType;
        this.m_lastPosition = -1;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateViewHolder --> Android Studio generated method, this method executes
     *          when the view holder is created.
     *
     * SYNOPSIS
     *      public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup,
     *          int a_viewType)
     *      @param a_viewGroup --> ViewGroup: Group of views.
     *      @param a_viewType --> int: Type of view.
     *
     * DESCRIPTION
     *      This method executes when the view holder is created.
     *
     * RETURNS
     *      @return TypeViewHolder: View Holder for Types.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    @NonNull
    @Override
    public TypeViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup, int a_viewType)
    {
        View view;
        view = LayoutInflater.from(m_context).inflate(R.layout.fragitem_type
                ,a_viewGroup,false);
        return new TypeViewHolder(view);
    }

    /**
     * METHOD
     *
     * NAME
     *      onBindViewHolder --> Android Studio generated method, this method executes
     *          when the view is bound.
     *
     * SYNOPSIS
     *      public void onBindViewHolder(@NonNull final TypeViewHolder a_holder,
     *          final int a_position)
     *      @param a_holder --> TypeViewHolder: View Holder for Types.
     *      @param a_position --> int: Index in list.
     *
     * DESCRIPTION
     *      This method executes  when the view is bound.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    @Override
    public void onBindViewHolder(@NonNull final TypeViewHolder a_holder, final int a_position)
    {
        // Type color
        GradientDrawable drawable = (GradientDrawable) a_holder.mt_Color.getBackground();
        drawable.setColor(m_type.get(a_position).Getm_typeColor());

        // Type name
        a_holder.mt_name.setText(m_type.get(a_position).Getm_typeName());

        // Total time
        Double timeUHR = m_type.get(a_position).Getm_totalTime();
        String timePassed = ((TLApplication) m_context.getApplicationContext()).GetReadableTime(timeUHR);
        a_holder.mt_totalTime.setText(timePassed);

        // Animation when the view is bound
        SetAnimation(a_holder.itemView, a_position);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      getItemCount --> Android Studio generated method, this method returns the number
     *          of ReportTypes in the recycler.
     *
     * SYNOPSIS
     *      public int getItemCount()
     *
     * DESCRIPTION
     *      This method returns the number of ReportTypes in the recycler.
     *
     * RETURNS
     *      @return int --> Number of ReportTypes in the recycler.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    @Override
    public int getItemCount()
    {
        return m_type.size();
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SetAnimation --> Sets an animation when a new bound view is added to the recycler.
     *
     * SYNOPSIS
     *      private void SetAnimation(View a_viewToAnimate, int a_position)
     *      @param a_viewToAnimate --> View: View to animate when bound.
     *      @param a_position --> int: Index number.
     *
     * DESCRIPTION
     *      This method sets a animation that executes when a new bound view is added
     *      to the recycler.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    private void SetAnimation(View a_viewToAnimate, int a_position)
    {
        // If the bound view wasn't previously displayed on screen, animate it
        if (a_position > m_lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(m_context,  android.R.anim.fade_in);
            a_viewToAnimate.startAnimation(animation);
            m_lastPosition = a_position;
        }
    }
}
