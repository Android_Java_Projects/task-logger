// Package and libraries used
package com.eg.tasklogger;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * CLASS
 *
 * NAME
 *      ReportResultActivity
 *
 * SYNOPSIS
 *      public class ReportResultActivity extends TLBaseActivity
 *
 * DESCRIPTION
 *      This class handles tabs(fragments) which display Report Results.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/23/2019
 */
public class ReportResultActivity extends TLBaseActivity
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private String m_fromDate;              // Report start date (yyyy-MM-dd HH:mm:ss)
    private String m_toDate;                // Report end date (yyyy-MM-dd HH:mm:ss)
    private String m_fromToDatePieTitle;    // Report date range (From...To)
    private String m_JSLibraries;           // JavaScript libraries used by the pie charts

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      DeleteRecord --> No action to perform.
     *
     * SYNOPSIS
     *      public void DeleteRecord()
     *
     * DESCRIPTION
     *      No implementation necessary. This method prototype is required since the parent class
     *      is an abstract. Record deletion is not required in this module.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/23/2019
     */
    public void DeleteRecord(){}

    /**
     * CLASS
     *
     * NAME
     *      SectionsPagerAdapter
     *
     * SYNOPSIS
     *      public class SectionsPagerAdapter extends FragmentPagerAdapter
     *
     * DESCRIPTION
     *      This class is an adapter that links the tab layout to the fragments contained in them.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter
    {
        // Private member variable
        int ma_NumberOfTabs;    // Number of tabs

        /**
         * METHOD
         *
         * NAME
         *      SectionsPagerAdapter --> Class constructor.
         *
         * SYNOPSIS
         *      SectionsPagerAdapter(FragmentManager fm, int a_numberOfTabs)
         *      @param a_fm --> FragmentManager: A fragment manager.
         *      @param a_numberOfTabs --> int: Number of tabs to display
         *
         * DESCRIPTION
         *      This method is the class constructor.
         *
         * RETURNS
         *      Not applicable
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 4/23/2019
         */
        SectionsPagerAdapter(FragmentManager a_fm, int a_numberOfTabs)
        {
            super(a_fm);
            this.ma_NumberOfTabs = a_numberOfTabs;
        }

        /**
         * METHOD
         *
         * NAME
         *      getItem --> Android Studio generated method. Returns a fragment.
         *
         * SYNOPSIS
         *      public Fragment getItem(int a_position)
         *      @param a_position --> int: Tab position (The first one is 0).
         *
         * DESCRIPTION
         *      This method returns a fragment according to the tab selected. From left to right:
         *      0: A fragment that displays a report result based on types.
         *      1: A fragment that displays a report result based on tasks.
         *      2: A fragment that displays a detail report results.
         *
         * RETURNS
         *      @return fragment --> The fragment to display.
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 4/23/2019
         */
        @Override
        public Fragment getItem(int a_position)
        {
            switch (a_position) {
                case 0:
                    return new ReportResultFragmentByType();
                case 1:
                    return new ReportResultFragmentByTask();
                case 2:
                    return new ReportResultFragmentDetails();
                default:
                    return null;
            }
        }

        /**
         * METHOD
         *
         * NAME
         *      getCount --> Android Studio generated method. Returns the number of tabs.
         *
         * SYNOPSIS
         *      public int getCount()
         *
         * DESCRIPTION
         *      This method returns returns the number of tabs in the Tab layout/container.
         *
         * RETURNS
         *      @return int --> Number of tabs.
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 4/23/2019
         */
        @Override
        public int getCount()
        {
            return ma_NumberOfTabs;
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      GetFileContents
     *
     * SYNOPSIS
     *      public String GetFileContents(String a_fileName)
     *      @param a_fileName --> String: File name to extract contents from.
     *
     * DESCRIPTION
     *      This method extracts the contents of a file stored in the assets resource directory.
     *
     * RETURNS
     *      @return String --> File contents.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/28/2019
     */
    public String GetFileContents(String a_fileName)
    {
        String fileAsString="";

        // Read file contents
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(getAssets()
                        .open(a_fileName))))
        {
            // Put file contents into a string to return
            StringBuilder sb = new StringBuilder();
            String mLine;
            while ((mLine = reader.readLine()) != null) sb.append(mLine).append("\n");

            return sb.toString();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return fileAsString;
    }

    // 4th: Constructor -----------------------------------------------------------------------
    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the layout/activity opens.
     *
     * SYNOPSIS
     *      protected void onCreate(Bundle a_savedInstanceState)
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of your Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the layout/class. It also receives a
     *      message sent by the calling Activity.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/21/2019
     */
    @Override
    protected void onCreate(Bundle a_savedInstanceState)
    {
        super.onCreate(a_savedInstanceState);
        setContentView(R.layout.activity_report_result);

        // Get the message from the calling activity.
        Intent intent = getIntent();
        final String message = intent.getStringExtra("message");

        // Initialize Class
        Initialize(message);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Getm_fromDate --> Returns the From date.
     *
     * SYNOPSIS
     *      public String Getm_fromDate()
     *
     * DESCRIPTION
     *      This method returns the From date. Called by the fragments held by this activity.
     *
     * RETURNS
     *      @return String --> From date as a string.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/24/2019
     */
    public String Getm_fromDate()
    {
        return m_fromDate;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_toDate --> Returns the To date.
     *
     * SYNOPSIS
     *      public String Getm_toDate()
     *
     * DESCRIPTION
     *      This method returns the To date. Mainly called by the fragments held by this.
     *
     * RETURNS
     *      @return String --> To date as a string.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/24/2019
     */
    public String Getm_toDate()
    {
        return m_toDate;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_fromToDatePieTitle --> Returns the From and To date.
     *
     * SYNOPSIS
     *      public String Getm_fromToDatePieTitle()
     *
     * DESCRIPTION
     *      This method returns the From...To date as a string. Used by the pie chart title.
     *
     * RETURNS
     *      @return String --> From...To date.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    public String Getm_fromToDatePieTitle(){ return m_fromToDatePieTitle; }

    /**
     * METHOD
     *
     * NAME
     *      Getm_JSLibraries --> Returns the JS libraries used by pie charts.
     *
     * SYNOPSIS
     *      public String Getm_JSLibraries()
     *
     * DESCRIPTION
     *      This method returns the string containing all necessary Javascript libraries used by
     *      the pie charts.
     *
     * RETURNS
     *      @return String --> JS libraries.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/24/2019
     */
    public String Getm_JSLibraries()
    {
        return m_JSLibraries;
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current activity.
     *
     * SYNOPSIS
     *      private void Initialize(String a_message)
     *      @param a_message --> String: message sent by the calling activity.
     *
     * DESCRIPTION
     *      This method initializes this class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/24/2019
     */
    private void Initialize(String a_message)
    {
        // Update the top bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.reportResult_title);

        // Enable up button
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set super member variables
        SetMemberVariables("noRecord|0|0");

        // Set From, To, and From...To dates
        String[] messageArray = a_message.split(Pattern.quote("|"));
        m_fromDate = messageArray[0];
        m_toDate = messageArray[1];
        m_fromToDatePieTitle = messageArray[2];
        m_JSLibraries = GetFileContents("TLoggerJSLibraries.txt");

        // Set the tabs
        TabLayout tabLayout = findViewById(R.id.reportResultTabs);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.reportResult_byType));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.reportResult_byTask));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.reportResult_details));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Set the adapter for the tabs
        final ViewPager viewPager = findViewById(R.id.reportResultContainer);
        final SectionsPagerAdapter adapter = new SectionsPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onTabSelected --> Android Studio generated method. Runs when
             *          the user clicks on a tab.
             *
             * SYNOPSIS
             *      public void onTabSelected(TabLayout.Tab tab)
             *
             * DESCRIPTION
             *      This method runs when the user clicks on a tab.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 4/23/2019
             */
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager.setCurrentItem(tab.getPosition());
            }

            // Required empty event handler
            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            // Required empty event handler
            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        });
    }
}
