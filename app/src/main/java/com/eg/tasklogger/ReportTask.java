// Package and libraries used
package com.eg.tasklogger;

/**
 * CLASS
 *
 * NAME
 *      ReportTask
 *
 * SYNOPSIS
 *      public class ReportTask
 *
 * DESCRIPTION
 *      This class is used to manage Tasks used in reports.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/28/2019
 */
public class ReportTask
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Double m_totalTime;     // Hours in UHR format
    private Task m_task;            // Task object

    // 3rd: UI Components ---------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      ReportTask --> Class constructor.
     *
     * SYNOPSIS
     *      ReportTask(Task a_task, Double a_totalTime)
     *      @param a_task --> Type: A Task object.
     *      @param a_totalTime --> Double: Hours in UHR format.
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    ReportTask(Task a_task, Double a_totalTime)
    {
        this.m_task = a_task;
        this.m_totalTime = a_totalTime;
    }
    // 5th: Event handlers --------------------------------------------------------------------
    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Getm_typeColor --> Returns the task type color.
     *
     * SYNOPSIS
     *      public int Getm_typeColor()
     *
     * DESCRIPTION
     *      This method returns the task type color.
     *
     * RETURNS
     *      @return int --> Number representing the task type color.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    int Getm_typeColor()
    {
        return m_task.Getm_typeColor();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typeColorHex --> Returns the task type color as hexadecimal.
     *
     * SYNOPSIS
     *      String Getm_typeColorHex()
     *
     * DESCRIPTION
     *      This method returns the task type color as a hexadecimal string.
     *
     * RETURNS
     *      @return String --> Hexadecimal color representing the task type color.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    String Getm_typeColorHex() { return m_task.Getm_typeColorHex(); }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typeName --> Returns the type name.
     *
     * SYNOPSIS
     *      public String Getm_typeName()
     *
     * DESCRIPTION
     *      This method returns the task type name.
     *
     * RETURNS
     *      @return String --> Task type name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    String Getm_typeName()
    {
        return m_task.Getm_typeName();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_taskName --> Returns the task name.
     *
     * SYNOPSIS
     *      String Getm_typeName()
     *
     * DESCRIPTION
     *      This method returns the task name.
     *
     * RETURNS
     *      @return String --> Task name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    String Getm_taskName() { return m_task.Getm_name(); }

    /**
     * METHOD
     *
     * NAME
     *      Getm_totalTime --> Returns the time in hours.
     *
     * SYNOPSIS
     *      Double Getm_totalTime()
     *
     * DESCRIPTION
     *      This method returns the time in UHR Hours.
     *
     * RETURNS
     *      @return Double --> UHR hours.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    Double Getm_totalTime()
    {
        return m_totalTime;
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
}
