// Package and libraries used
package com.eg.tasklogger;
import android.app.Application;

/**
 * CLASS
 *
 * NAME
 *      TLApplication
 *
 * SYNOPSIS
 *      public class TLApplication extends Application
 *
 * DESCRIPTION
 *      This class inherits from the Application class, which is Android generated
 *      and it’s the default class that handles all Android applications.
 *      Constants and shared methods are implemented here.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      4:28pm 2/21/2019
 */
public class TLApplication extends Application
{
    // android.util.Log.v("MyTag","I entered here stop");

    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    public static final int NOTANINDEXNORID = -999;
    public static final int NORECORD = 0;
    public static final int NEWRECORD = 1;
    public static final int EDITRECORD = 2;
    public static final int DELETERECORD = 3;
    public static final int RUNNINGLOG = 0;
    public static final int COMPLETELOG = 1;

    // 2nd: Private member variables ----------------------------------------------------------
    private int m_pendingRecordID;  // Pending record Id number
    private int m_pendingIndex;     // Pending list index number
    private int m_pendingAction;    // Pending action to perform

    // 3rd: UI Components --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      GetReadableTime --> Returns the UHR time as a string.
     *
     * SYNOPSIS
     *      public String GetReadableTime(Double a_timeUHR)
     *      @param a_timeUHR --> Double: A double in Units of Hour (UHR)
     *
     * DESCRIPTION
     *      This method returns the equivalent of a UHR double value in days, hours, and minutes.
     *
     * RETURNS
     *      @return String --> The number of days, hours, and minutes.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/08/2019
     */
    public String GetReadableTime(Double a_timeUHR)
    {
        Double hourUHR = a_timeUHR;
        long days;
        Long hours;
        double minutes;

        days = hourUHR.longValue() / 24;
        hourUHR = hourUHR - ((double) days * 24);
        hours = hourUHR.longValue();
        minutes = (hourUHR - hours) * 60;
        int min = (int) Math.round(minutes);

        // Build string to return
        String toReturn = "";
        if (days > 0) toReturn = days + ((days > 1) ? " days " : " day ");
        if (hours > 0) toReturn += hours + ((hours > 1) ? " hours " : " hour ");
        if (minutes > 0) toReturn += min + ((min > 1) ? " minutes " : " minute ");

        return toReturn;
    }

    // 4th: Constructor -----------------------------------------------------------------------

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the application opens.
     *
     * SYNOPSIS
     *      public void onCreate()
     *
     * DESCRIPTION
     *      This method creates/updates application objects.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/23/2019
     */
    @Override
    public void onCreate()
    {
        super.onCreate();
        ResetPending();
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Getm_pendingRecordID --> Returns a pending record ID.
     *
     * SYNOPSIS
     *      public int Getm_pendingRecordID()
     *
     * DESCRIPTION
     *      This method returns the pending record ID to process. A record ID is held momentarily
     *      while activities are being swapped. Mostly used to set/update recyclers and/or calling
     *      activities.
     *
     * RETURNS
     *      @return int --> Record ID number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public int Getm_pendingRecordID()
    {
        return m_pendingRecordID;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_pendingIndex --> Returns a pending index number.
     *
     * SYNOPSIS
     *      public int Getm_pendingIndex()
     *
     * DESCRIPTION
     *      This method returns the pending index number to process. An index number is held
     *      momentarily while activities are being swapped. Mostly used to set/update recyclers
     *      and/or calling activities.
     *
     * RETURNS
     *      @return int --> Index number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public int Getm_pendingIndex()
    {
        return m_pendingIndex;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_pendingAction --> Returns the pending action number.
     *
     * SYNOPSIS
     *      public int Getm_pendingAction()
     *
     * DESCRIPTION
     *      This method returns the pending action to process. A pending action number is held
     *      momentarily while activities are being swapped. Mostly used to set/update recyclers
     *      and/or calling activities.
     *
     * RETURNS
     *      @return int --> Action number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public int Getm_pendingAction()
    {
        return m_pendingAction;
    }

    // 7th: Mutators --------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Setm_pending --> Sets all "pending" member variables.
     *
     * SYNOPSIS
     *      public void Setm_pending(int a_recordID, int a_index, int a_action)
     *      @param a_recordID --> int: A record ID.
     *      @param a_index --> int: An array/list index.
     *      @param a_action --> int: An action number.
     *
     * DESCRIPTION
     *      This method sets all member variables with pending on their name. A "pending" variable
     *      value is held momentarily while activities are being swapped. Mostly used to
     *      set/update recyclers and/or calling activities.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public void Setm_pending(int a_recordID, int a_index, int a_action)
    {
        m_pendingRecordID = a_recordID;
        m_pendingIndex = a_index;
        m_pendingAction = a_action;
    }

    /**
     * METHOD
     *
     * NAME
     *      ResetPending --> Re-sets all "pending" member variables.
     *
     * SYNOPSIS
     *      public void ResetPending()
     *
     * DESCRIPTION
     *      This method re-sets all member variables, with pending on their name, to a safe state.
     *      A "pending" variable value is held momentarily while activities are being swapped.
     *      Mostly used to set/update recyclers and/or calling activities.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public void ResetPending()
    {
        m_pendingRecordID = NOTANINDEXNORID;
        m_pendingIndex = NOTANINDEXNORID;
        m_pendingAction = NOTANINDEXNORID;
    }

    // 8th: Any utility (private) methods -----------------------------------------------------
}
