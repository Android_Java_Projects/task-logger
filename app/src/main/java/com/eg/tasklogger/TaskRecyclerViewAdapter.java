package com.eg.tasklogger;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

/**
 * CLASS
 *
 * NAME
 *      TaskRecyclerViewAdapter
 *
 * SYNOPSIS
 *      public class TaskRecyclerViewAdapter
 *          extends RecyclerView.Adapter<TaskRecyclerViewAdapter.TaskViewHolder>
 *
 * DESCRIPTION
 *      This class is an adapter that links the recycler view to an array holding all tasks.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 3/16/2019
 */
public class TaskRecyclerViewAdapter
        extends RecyclerView.Adapter<TaskRecyclerViewAdapter.TaskViewHolder>
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Context m_context; // Context to be used by recycler view
    private List<Task> m_task; // Array/List of Task type
    private int m_lastPosition = -1; // Allows to remember the last item shown on screen

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * CLASS
     *
     * NAME
     *      TaskViewHolder --> Helper class which holds task data.
     *
     * SYNOPSIS
     *      static class TaskViewHolder extends RecyclerView.ViewHolder
     *
     * DESCRIPTION
     *      This class holds task data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    static class TaskViewHolder extends RecyclerView.ViewHolder
    {

        private TextView mt_typeColor;
        private TextView mt_typeName;
        private TextView mt_name;
        private TextView mt_description;
        private LinearLayout mt_parentLayout;

        /**
         * METHOD
         *
         * NAME
         *      TaskViewHolder --> Class constructor.
         *
         * SYNOPSIS
         *      public TaskViewHolder(@NonNull View itemView)
         *      @param a_itemView --> View: View holding task data
         *
         * DESCRIPTION
         *      This method is the class constructor for the inner class.
         *
         * RETURNS
         *      void
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 3/24/2019
         */
        TaskViewHolder(@NonNull View a_itemView)
        {
            super(a_itemView);

            // Data to display
            mt_typeColor = a_itemView.findViewById(R.id.taskItem_typeColor);
            mt_typeName = a_itemView.findViewById(R.id.taskItem_typeName);
            mt_name = a_itemView.findViewById(R.id.taskItem_name);
            mt_description = a_itemView.findViewById(R.id.taskItem_description);
            mt_parentLayout = a_itemView.findViewById(R.id.layoutTaskItem);
        }
    }

    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      TaskRecyclerViewAdapter --> Class constructor.
     *
     * SYNOPSIS
     *      TaskRecyclerViewAdapter(Context a_context, List<Task> a_task)
     *      @param a_context --> Context: Context to set
     *      @param a_task --> List<Task>: List/Array of Task objects
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    TaskRecyclerViewAdapter(Context a_context, List<Task> a_task)
    {
        this.m_context = a_context;
        this.m_task = a_task;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateViewHolder --> Android Studio generated method, this method executes
     *          when the view holder is created.
     *
     * SYNOPSIS
     *      public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup,
     *          int a_viewType)
     *      @param a_viewGroup --> ViewGroup: Group of views.
     *      @param a_viewType --> int: Type of view.
     *
     * DESCRIPTION
     *      This method executes when the view holder is created.
     *
     * RETURNS
     *      @return TypeViewHolder: View Holder for Tasks.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup, int a_viewType)
    {
        View view;
        view = LayoutInflater.from(m_context).inflate(R.layout.fragitem_task
                ,a_viewGroup,false);
        return new TaskRecyclerViewAdapter.TaskViewHolder(view);
    }

    /**
     * METHOD
     *
     * NAME
     *      onBindViewHolder --> Android Studio generated method, this method executes
     *          when the view is bound.
     *
     * SYNOPSIS
     *      public void onBindViewHolder(final TaskRecyclerViewAdapter.TaskViewHolder a_holder,
     *          final int a_position)
     *      @param a_holder --> TypeViewHolder: View Holder for Tasks.
     *      @param a_position --> int: Index in list.
     *
     * DESCRIPTION
     *      This method executes  when the view is bound.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    @Override
    public void onBindViewHolder(final TaskRecyclerViewAdapter.TaskViewHolder a_holder,
                                 final int a_position)
    {
        // Task data
        a_holder.mt_name.setText(m_task.get(a_position).Getm_name());
        a_holder.mt_description.setText(m_task.get(a_position).Getm_description());

        GradientDrawable drawable = (GradientDrawable) a_holder.mt_typeColor.getBackground();
        drawable.setColor(m_task.get(a_position).Getm_typeColor());

        a_holder.mt_typeName.setText(m_task.get(a_position).Getm_typeName());

        // Animation when the view is bound
        SetAnimation(a_holder.itemView, a_position);

        a_holder.mt_parentLayout.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the user presses on a Task item on the list.
             *      The Edit activity is called with a message that includes the action type,
             *      Task record ID, and the position in the array/recycler of the item clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00pm 3/25/2019
             */
            @Override
            public void onClick(View a_view){
                Intent myIntent = new Intent(m_context, TaskAEActivity.class);
                String message = "edit|" +  Integer.toString(m_task.get(a_position).Getm_id())
                        + "|" + Integer.toString(a_position);
                myIntent.putExtra("message",message);
                m_context.startActivity(myIntent);
            }
        });
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      getItemCount --> Android Studio generated method, this method returns the number
     *          of task in the recycler.
     *
     * SYNOPSIS
     *      public int getItemCount()
     *
     * DESCRIPTION
     *      This method returns the number of task in the recycler.
     *
     * RETURNS
     *      @return int --> Number of tasks in the recycler.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    @Override
    public int getItemCount()
    {
        return m_task.size();
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SetAnimation --> Sets an animation when a new bound view is added to the recycler.
     *
     * SYNOPSIS
     *      private void SetAnimation(View a_viewToAnimate, int a_position)
     *      @param a_viewToAnimate --> View: View to animate when bound.
     *      @param a_position --> int: Index number.
     *
     * DESCRIPTION
     *      This method sets a animation that executes when a new bound view is added
     *      to the recycler.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    private void SetAnimation(View a_viewToAnimate, int a_position)
    {
        // If the bound view wasn't previously displayed on screen, animate it
        if (a_position > m_lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(m_context, android.R.anim.slide_in_left);
            a_viewToAnimate.startAnimation(animation);
            m_lastPosition = a_position;
        }
    }
}
