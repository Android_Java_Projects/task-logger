// Package and libraries used
package com.eg.tasklogger;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.eg.tasklogger.TLApplication.COMPLETELOG;
import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;
import static com.eg.tasklogger.TLApplication.NEWRECORD;
import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;

/**
 * CLASS
 *
 * NAME
 *      CompletedFragment
 *
 * SYNOPSIS
 *      public class CompletedFragment extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Completed Tasks fragment. This fragment contains the list
 *      of completed logs within a specified (on settings) range of days.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      10:00am 2/22/2019
 */
public class CompletedFragment extends Fragment
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private List<Log> m_lstLog;                         // Holds all the log data
    private LogRecyclerViewAdapter m_recyclerAdapter;   // Adapter that links recycler to data array
    DatabaseHelper m_databaseHelper;                    // Database connector
    private String m_fromDate;                          // Start date bracket to display
    private String m_toDate;                            // End date bracket to display

    // 3rd: UI Components --------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      CompletedFragment --> Class constructor.
     *
     * SYNOPSIS
     *      public CompletedFragment()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    public CompletedFragment()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *           when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_completed, a_container, false);

        // Initialize Class
        Initialize(view);

        return view;
    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the fragment becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the fragment becomes active. It checks if there is
     *      any "pending" data that will update the array/recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Check if a log should be added/updated in the recycler
        if (getActivity() != null)
        {
            int pendingRecordID = ((TLApplication) getActivity()
                    .getApplication()).Getm_pendingRecordID();
            if (pendingRecordID != NOTANINDEXNORID) UpdateRecycler(pendingRecordID);
        }
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      UpdateRecycler --> Updates the recycler view.
     *
     * SYNOPSIS
     *      private void UpdateRecycler(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the array referenced by the recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    private void UpdateRecycler(int a_pendingRecordID)
    {
        int pendingIndex = 0;
        int pendingAction = 0;

        if (getActivity() != null)
        {
            pendingIndex = ((TLApplication) getActivity().getApplication()).Getm_pendingIndex();
            pendingAction = ((TLApplication) getActivity().getApplication()).Getm_pendingAction();
        }

        switch (pendingAction)
        {
            case NEWRECORD:
                Log newLog = m_databaseHelper.GetLog(a_pendingRecordID);
                m_lstLog.add(newLog);
                break;

            case EDITRECORD:
                Log editedLog = m_databaseHelper.GetLog(a_pendingRecordID);
                m_lstLog.set(pendingIndex,editedLog);
                break;

            case DELETERECORD:
                m_lstLog.remove(pendingIndex);
                break;
        }

        // Update recycler and reset pending to process data.
        m_recyclerAdapter.notifyDataSetChanged();
        ((TLApplication) getActivity().getApplication()).ResetPending();
    }

    /**
     * METHOD
     *
     * NAME
     *      AddCompleted --> Calls the activity that adds a completed task.
     *
     * SYNOPSIS
     *      private void AddCompleted()
     *
     * DESCRIPTION
     *      This method calls the activity used to add/edit a completed task.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    private void AddCompleted()
    {
        if (getActivity() != null)
        {
            Intent myIntent = new Intent(getActivity(), CompletedAEActivity.class);
            myIntent.putExtra("message", "new|0|0" );
            getActivity().startActivity(myIntent);
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      InitializeDateRange --> Initializes a date range.
     *
     * SYNOPSIS
     *      private void InitializeDateRange()
     *
     * DESCRIPTION
     *      This method sets a date range used to pull completed tasks from the database.
     *      The fromDate is from the moment the log was started and the toDate is when the log
     *      was completed.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void InitializeDateRange()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        // Set the toDate to the current timestamp
        m_toDate = dateFormat.format(cal.getTime());

        // Get the completedTasks days setting
        // This will set the from date according to the days entered in the settings
        int completedTaskDaysSetting = 0;
        JSONObject currentSettings = m_databaseHelper.GetSettings();
        try
        {
            String completedDays = currentSettings.getString("completedDays");
            completedTaskDaysSetting = Integer.parseInt(completedDays) * -1;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        cal.add(Calendar.DATE, completedTaskDaysSetting);
        m_fromDate = dateFormat.format(cal.getTime());
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_view --> View: Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        m_databaseHelper = new DatabaseHelper(getContext());

        // Update the title to reflect the "Show completed tasks..." setting.
        JSONObject currentSettings = m_databaseHelper.GetSettings();
        try
        {
            String completedDays = currentSettings.getString("completedDays");
            String newTitle = getResources().getString(R.string.complete_title)
                    + " - " + completedDays + " days" ;

            if (getActivity() != null) getActivity().setTitle(newTitle);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // Initialize date brackets to display
        InitializeDateRange();

        // Set member variables
        m_lstLog = new ArrayList<>();
        m_lstLog = m_databaseHelper.GetMultipleLogs(COMPLETELOG,m_fromDate,
                m_toDate, "DESC");

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = a_view.findViewById(R.id.fabCompleted);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Add New action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                AddCompleted();
            }
        });

        // Set the Recycler/List
        RecyclerView recyclerView = a_view.findViewById(R.id.recyclerView_Completed);
        m_recyclerAdapter = new LogRecyclerViewAdapter(getContext(),m_lstLog,1, COMPLETELOG );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(m_recyclerAdapter);
    }
}
