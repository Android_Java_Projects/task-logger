// Package and libraries used
package com.eg.tasklogger;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.eg.tasklogger.TLApplication.COMPLETELOG;
import static com.eg.tasklogger.TLApplication.EDITRECORD;
import static com.eg.tasklogger.TLApplication.NEWRECORD;
import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;

/**
 * CLASS
 *
 * NAME
 *      CompletedAEActivity
 *
 * SYNOPSIS
 *      public class CompletedAEActivity extends AppCompatActivity
 *
 * DESCRIPTION
 *      This class handles the Completed Task Activity and its components. It extends,
 *      inherits from, the TLBaseActivity. The Completed Task Activity is used to save
 *      completed tasks or logs into the database.  This activity is used when the user wants
 *      to enter the start and end time of a completed task, which once saved, becomes a log.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00am 3/2/2019
 */
public class CompletedAEActivity extends TLBaseActivity
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Log m_log;                              // Log Object
    private List<Task> m_lstTask;                   // Holds all the task data
    private List<String> m_tasks;                   // Holds all the task names
    private int m_lstTaskChosenIndex;               // Index of the task chosen
    private ArrayAdapter<String> m_dropDownAdapter; // Adapter used by the task drop down list
    private Spinner m_dropDown;                     // Task drop down

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      DeleteRecord --> Deletes a Log record.
     *
     * SYNOPSIS
     *      public void DeleteRecord()
     *
     * DESCRIPTION
     *      This method asks the user to confirm deletion of the current Log record, if
     *      confirmation is required.  If not, it calls the method to execute record deletion.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    public void DeleteRecord()
    {
        // Prompt the user, if required.
        if (Getm_promptDelete())
        {
            //Put up a Yes/No message box
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setIcon(R.drawable.ic_warning)
                    .setTitle("Delete Log Record")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //Yes button clicked
                            ExecuteDelete(m_log);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else ExecuteDelete(m_log);
    }

    // 4th: Constructor -----------------------------------------------------------------------
    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the layout/activity opens.
     *
     * SYNOPSIS
     *      protected void onCreate(Bundle a_savedInstanceState)
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of your Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the layout/class.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    @Override
    protected void onCreate(Bundle a_savedInstanceState)
    {
        super.onCreate(a_savedInstanceState);
        setContentView(R.layout.activity_completed_ae);

        // Get the transferred data from calling activity.
        Intent intent = getIntent();
        final String message = intent.getStringExtra("message");

        // Initialize Class
        Initialize(message);
    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the activity becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the activity becomes active. It checks if there is
     *      any "pending" data that will update the task spinner (Drop down list).
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/5/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Check if a new task was added. Updates the task spinner (drop down)
        int pendingRecordID = ((TLApplication) this.getApplication()).Getm_pendingRecordID();
        if (pendingRecordID != NOTANINDEXNORID) UpdateSpinner(pendingRecordID);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      LoadLog --> Loads Log data into the appropriate objects.
     *
     * SYNOPSIS
     *      private void LoadLog()
     *
     * DESCRIPTION
     *      This method loads Log data into the appropriate layout objects.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    private void LoadLog()
    {
        // Check if new or edit
        if (Getm_actionType() == EDITRECORD)
        {
            m_log = new Log();
            m_log = m_databaseHelper.GetLog(Getm_recordID());

            // Populate Task
            m_dropDown.setSelection(m_dropDownAdapter.getPosition(m_log.Getm_taskName()));
            m_lstTaskChosenIndex = m_dropDown.getSelectedItemPosition();

            // Populate Dates and times
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEE, MMM dd yyyy", Locale.getDefault());
            SimpleDateFormat timeFormat = new SimpleDateFormat(
                    "hh:mm aa", Locale.getDefault());

            date.setTime(m_log.Getm_start().getTime());
            EditText inputStartDate = findViewById(R.id.editCLogStartDate);
            inputStartDate.setText(dateFormat.format(date));
            EditText inputStartTime = findViewById(R.id.editCLogStartTime);
            inputStartTime.setText(timeFormat.format(date));

            date.setTime(m_log.Getm_end().getTime());
            EditText inputEndDate = findViewById(R.id.editCLogEndDate);
            inputEndDate.setText(dateFormat.format(date));
            EditText inputEndTime = findViewById(R.id.editCLogEndTime);
            inputEndTime.setText(timeFormat.format(date));
        }
        else
        {
            // This is a new record, set defaults.
            m_log = new Log(0,COMPLETELOG,""
                    ,m_lstTask.get(m_lstTaskChosenIndex));
        }

        // Populate notes
        EditText inputNotes = findViewById(R.id.editCLogNotes);
        inputNotes.setText(m_log.Getm_notes());
    }

    /**
     * METHOD
     *
     * NAME
     *      InitializeTaskDropDown --> Initializes the task drop down.
     *
     * SYNOPSIS
     *      private void InitializeTaskDropDown()
     *
     * DESCRIPTION
     *      This method initializes the task spinner.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/5/2019
     */
    private void InitializeTaskDropDown()
    {
        // Get all tasks
        m_lstTask = new ArrayList<>();
        m_lstTask = m_databaseHelper.GetAllTasks();

        // Build the string array needed by the spinner
        m_tasks = new ArrayList<>();
        for(Task task : m_lstTask) m_tasks.add(task.Getm_name());
        m_tasks.add("Add New...");

        // Set spinner
        m_dropDown = findViewById(R.id.spinnerCLogTaskName);
        m_dropDownAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, m_tasks);
        m_dropDown.setAdapter(m_dropDownAdapter);

        m_dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onItemSelected --> Android Studio generated method, this method runs when an
             *          item is selected in the task drop down.
             *
             * SYNOPSIS
             *      public void onItemSelected(AdapterView<?> parent, View view,
             *          int position, long id)
             *      @param a_parent --> AdapterView<?>: Adapter View object.
             *      @param a_view --> View: Calling view object.
             *      @param a_position --> int: Position of a list item
             *      @param a_id --> long: Id of the list item
             *
             * DESCRIPTION
             *      This method runs when an item is selected in the task drop down.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00pm 4/5/2019
             */
            @Override
            public void onItemSelected(AdapterView<?> a_parent, View a_view,
                                       int a_position, long a_id)
            {
                // Check if Add New...was selected
                int addNewPosition = m_dropDownAdapter.getPosition("Add New...");
                if (m_dropDown.getSelectedItemPosition() == addNewPosition)
                {
                    m_dropDown.setSelection(0);
                    // Call the Add Task activity.
                    AddTask();
                }
                else
                {
                    // Store the selected task index and update the type data
                    m_lstTaskChosenIndex = m_dropDown.getSelectedItemPosition();
                    SetTypeData(m_lstTask.get(m_lstTaskChosenIndex));
                }
            }

            // Empty required event definition, not used.
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    /**
     * METHOD
     *
     * NAME
     *      AddTask --> Calls the activity that adds a task.
     *
     * SYNOPSIS
     *      private void AddTask()
     *
     * DESCRIPTION
     *      This method calls the activity used to add a task to the database.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/5/2019
     */
    private void AddTask()
    {
        Intent myIntent = new Intent(this, TaskAEActivity.class);
        myIntent.putExtra("message", "new|0|0" );
        startActivity(myIntent);
    }

    /**
     * METHOD
     *
     * NAME
     *      SetTypeData --> Sets type data.
     *
     * SYNOPSIS
     *      private void SetTypeData(Task a_task)
     *      @param a_task --> int: Drop down selected task.
     *
     * DESCRIPTION
     *      This method sets the type name and color. The type is related to the task chosen.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/24/2019
     */
    private void SetTypeData(Task a_task)
    {
        int typeColor = a_task.Getm_typeColor();
        String typeName = a_task.Getm_typeName();

        // Set the type button color
        Button typeColorButton = findViewById( R.id.buttonCLogTypeColor);
        GradientDrawable drawable = (GradientDrawable) typeColorButton.getBackground();
        drawable.setColor(typeColor);

        // Set the type name
        TextView typeNameText = findViewById(R.id.labelCLogTypeName);
        typeNameText.setText(typeName);
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateSpinner --> Updates the spinner object.
     *
     * SYNOPSIS
     *      private void UpdateSpinner(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the spinner(Drop down list) object used to select a task.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/2/2019
     */
    private void UpdateSpinner(int a_pendingRecordID)
    {
        // Get tasks from the DB
        m_lstTask = m_databaseHelper.GetAllTasks();
        Task newTask = m_databaseHelper.GetTask(a_pendingRecordID);

        // Re-Build the string array needed by the spinner
        m_tasks.clear();
        for(Task task : m_lstTask) m_tasks.add(task.Getm_name());
        m_tasks.add("Add New...");
        m_dropDownAdapter.notifyDataSetChanged();

        // Resets pending
        ((TLApplication) this.getApplication()).ResetPending();

        // Set the task to the new task added
        m_dropDown.setSelection(m_dropDownAdapter.getPosition(newTask.Getm_name()));
        m_lstTaskChosenIndex = m_dropDown.getSelectedItemPosition();
        SetTypeData(m_lstTask.get(m_lstTaskChosenIndex));
    }

    /**
     * METHOD
     *
     * NAME
     *      SetUpDateEditBox --> Sets up a date edit box
     *
     * SYNOPSIS
     *      private void SetUpDateEditBox(EditText a_dateEditText)
     *      @param a_dateEditText --> EditText: Edit box to set.
     *
     * DESCRIPTION
     *      This method sets a date edit box. It clears the text (If is a new log), removes the
     *      key press listener, makes it un-focusable, removes the cursor, and sets an
     *      onClick listener.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    private void SetUpDateEditBox(EditText a_dateEditText)
    {
        if (Getm_actionType() == NEWRECORD) a_dateEditText.setText("");
        a_dateEditText.setKeyListener(null);
        a_dateEditText.setFocusable(false);
        a_dateEditText.setCursorVisible(false);
        a_dateEditText.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method, this method is called when
             *          the user clicks on a date edit box.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Date Edit box.
             *
             * DESCRIPTION
             *      This method is called when the user clicks on a date edit box.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:04pm 4/3/2019
             */
            @Override
            public void onClick(View a_view)
            {
                PickADate(a_view);
            }

        });
    }

    /**
     * METHOD
     *
     * NAME
     *      SetUpTimeEditBox --> Sets up a time edit box
     *
     * SYNOPSIS
     *      private void SetUpTimeEditBox(EditText a_timeEditText)
     *      @param a_timeEditText --> EditText: Edit box to set.
     *
     * DESCRIPTION
     *      This method sets a time edit box. It clears the text (If is a new record), removes
     *      the key press listener, makes it un-focusable, removes the cursor, and
     *      sets an onClick listener.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    private void SetUpTimeEditBox(EditText a_timeEditText)
    {
        if (Getm_actionType() == NEWRECORD) a_timeEditText.setText("");
        a_timeEditText.setKeyListener(null);
        a_timeEditText.setFocusable(false);
        a_timeEditText.setCursorVisible(false);
        a_timeEditText.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method, this method is called when
             *          the user clicks on a time edit box.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Time edit box.
             *
             * DESCRIPTION
             *      This method is called when the user clicks on a time edit box.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:04pm 4/3/2019
             */
            @Override
            public void onClick(View a_view)
            {
                PickATime(a_view);
            }

        });
    }

    /**
     * METHOD
     *
     * NAME
     *      PickADate --> Display a date picker.
     *
     * SYNOPSIS
     *      private void PickADate(View a_view)
     *      @param a_view --> View: Date Edit box calling this method.
     *
     * DESCRIPTION
     *      This method displays a date picker and saves the date.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    private void PickADate(View a_view)
    {
        final int clickedDateEditBox = a_view.getId();

        // Get the log's day, month, and year
        long ts;
        if (clickedDateEditBox == R.id.editCLogStartDate) ts = m_log.Getm_start().getTime();
        else ts = m_log.Getm_end().getTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ts);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        DatePickerDialog datePicker = new DatePickerDialog(this
                , new DatePickerDialog.OnDateSetListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onDateSet --> Android Studio generated method, this method is called when
             *          the user sets a date in the date picker.
             *
             * SYNOPSIS
             *      public void onDateSet(DatePicker datePicker, int a_year, int a_month
             *          , int a_day)
             *      @param datePicker --> DatePicker: Date picker object.
             *      @param a_year --> int: Number representing a year.
             *      @param a_month --> int: Number representing a month.
             *      @param a_day --> int: Number representing a day.
             *
             * DESCRIPTION
             *      This method is called when the user sets a date in the date picker.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 4/4/2019
             */
            @Override
            public void onDateSet(DatePicker datePicker, int a_year, int a_month, int a_day)
            {
                // Edit box to set the date to
                EditText editDate = findViewById(clickedDateEditBox);

                // Set date into format yyyy-MM-dd
                String dateText = Integer.toString(a_year) + "-"
                        + Integer.toString(a_month+1) + "-" + Integer.toString(a_day);
                try
                {
                    SimpleDateFormat sdfyMd = new SimpleDateFormat(
                            "yyyy-MM-dd", Locale.getDefault());
                    SimpleDateFormat sdfDMdy = new SimpleDateFormat(
                            "EEE, MMM dd yyyy", Locale.getDefault());

                    Date date = sdfyMd.parse(dateText);
                    editDate.setText(sdfDMdy.format(date));

                    // Check if the start date was clicked
                    if (clickedDateEditBox == R.id.editCLogStartDate)
                    {
                        // Update start timestamp
                        m_log.UpdateDate("start",dateText);

                        // if end date is empty, set to start date
                        EditText endDate = findViewById(R.id.editCLogEndDate);
                        if (endDate.getText().toString().equals(""))
                        {
                            endDate.setText(editDate.getText());
                            m_log.UpdateDate("end",dateText);
                        }
                    }
                    else
                    {
                        // Update end timestamp
                        m_log.UpdateDate("end",dateText);
                    }
                    CheckTimeDifference();
                }
                catch (ParseException exp)
                {
                    exp.printStackTrace();
                }
            }
        }, year, month, day);
        datePicker.show();
    }

    /**
     * METHOD
     *
     * NAME
     *      PickATime --> Display a time picker.
     *
     * SYNOPSIS
     *      private void PickATime(View a_view)
     *      @param a_view --> View: Time Edit box calling this method.
     *
     * DESCRIPTION
     *      This method displays a time picker and saves the time.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    private void PickATime(View a_view)
    {
        final int clickedTimeEditBox = a_view.getId();

        // Get the log's hour and minute
        long ts;
        if (clickedTimeEditBox == R.id.editCLogStartTime) ts = m_log.Getm_start().getTime();
        else ts = m_log.Getm_end().getTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ts);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onDateSet --> Android Studio generated method, this method is called when
             *          the user sets a time in the time picker.
             *
             * SYNOPSIS
             *      public void onTimeSet(TimePicker timePicker, int a_hour, int a_minute)
             *      @param timePicker --> TimePicker: Time picker object.
             *      @param a_hour --> int: Number representing an hour.
             *      @param a_minute --> int: Number representing a minute.
             *
             * DESCRIPTION
             *      This method is called when the user sets a date in the date picker.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 4/4/2019
             */
            @Override
            public void onTimeSet(TimePicker timePicker, int a_hour, int a_minute)
            {
                // Edit box to set the time to
                EditText editTime = findViewById(clickedTimeEditBox);

                // Set time into format yyyy-MM-dd hh:mm:ss
                String timeText = Integer.toString(a_hour) + ":"
                        + Integer.toString(a_minute) + ":00";
                String timeTextFormat = "1981-01-17 " + Integer.toString(a_hour) + ":"
                        + Integer.toString(a_minute) + ":00";
                try
                {
                    SimpleDateFormat sdfhms = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat sdfhma = new SimpleDateFormat(
                            "hh:mm aa", Locale.getDefault());

                    Date time = sdfhms.parse(timeTextFormat);
                    editTime.setText(sdfhma.format(time));

                    // Update the timestamp
                    if (clickedTimeEditBox == R.id.editCLogStartTime)
                        m_log.UpdateTime("start", timeText);
                    else m_log.UpdateTime("end",timeText);

                    CheckTimeDifference();
                }
                catch (ParseException exp)
                {
                    exp.printStackTrace();
                }
            }
        }, hour, minute, android.text.format.DateFormat.is24HourFormat(this));
        timePicker.show();
    }

    /**
     * METHOD
     *
     * NAME
     *      CheckTimeDifference --> Display time difference between start and end date.
     *
     * SYNOPSIS
     *      private void CheckTimeDifference()
     *
     * DESCRIPTION
     *      This method checks if start and end date/time are valid and then calls
     *      the method to display the time difference between start and end.
     *      A message is displayed in a toast object.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    private void CheckTimeDifference()
    {
        // Check if all fields are not empty
        EditText startDate = findViewById(R.id.editCLogStartDate);
        EditText startTime = findViewById(R.id.editCLogStartTime);
        EditText endDate = findViewById(R.id.editCLogEndDate);
        EditText endTime = findViewById(R.id.editCLogEndTime);
        if (startDate.getText().toString().equals("")
                || startTime.getText().toString().equals("")
                || endDate.getText().toString().equals("")
                || endTime.getText().toString().equals("")
        )
            return;

        DisplayTimeDifference(m_log);
    }

    /**
     * METHOD
     *
     * NAME
     *      SaveLogData --> Saves the Log data entered.
     *
     * SYNOPSIS
     *      private void SaveLogData()
     *
     * DESCRIPTION
     *      This method saves the entered Log data into a database.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/8/2019
     */
    private void SaveLogData()
    {
        // Check if all required data was entered
        if (!IsRequiredEntered()) return;

        // Check if the end date is greater than the start date
        if (!IsEndAfterStart()) return;

        // Set name, description, and type
        m_log.Setm_task(m_lstTask.get(m_lstTaskChosenIndex));
        EditText logNotes = findViewById(R.id.editCLogNotes);
        m_log.Setm_notes(logNotes.getText().toString());

        // Edit/Add a log in the database
        boolean updateResult;
        int action = Getm_actionType();
        if (action == EDITRECORD) updateResult = m_databaseHelper.UpdateLog(m_log);
        else
        {
            updateResult = m_databaseHelper.AddLog(m_log);
            m_log.Setm_id(m_databaseHelper.GetLastID("log"));
        }

        // Notify user
        if (updateResult)
        {
            String actionMessage = ((Getm_actionType() == EDITRECORD) ? "Updated" : "Saved");
            Toast.makeText(this, actionMessage + " successfully",
                    Toast.LENGTH_SHORT).show();

            // Set pending data
            ((TLApplication) this.getApplication()).Setm_pending(m_log.Getm_id()
                    ,Getm_position(),action);

            finish();
        }
        else Toast.makeText(this, "There was an error, please try again",
                Toast.LENGTH_SHORT).show();
    }

    /**
     * METHOD
     *
     * NAME
     *      IsRequiredEntered --> Checks if all required data was entered.
     *
     * SYNOPSIS
     *      private boolean IsRequiredEntered()
     *
     * DESCRIPTION
     *      This method checks if all required data was entered. If false, a warning to the user
     *      is displayed.
     *
     * RETURNS
     *      @return boolean --> True if all required data was entered.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    private boolean IsRequiredEntered()
    {
        // Set up date and time edit boxes
        EditText startDate = findViewById(R.id.editCLogStartDate);
        EditText startTime = findViewById(R.id.editCLogStartTime);
        EditText endDate = findViewById(R.id.editCLogEndDate);
        EditText endTime = findViewById(R.id.editCLogEndTime);
        SetWarningVisibility(R.id.labelCLogStartWarning,R.id.buttonCLogStartWarning,View.INVISIBLE);
        SetWarningVisibility(R.id.labelCLogEndWarning,R.id.buttonCLogEndWarning, View.INVISIBLE);

        // Check if the start date/time is/are empty
        boolean notEmptyFlag = true;
        if (startDate.getText().toString().equals("")
                || startTime.getText().toString().equals(""))
        {
            SetWarningVisibility(R.id.labelCLogStartWarning,R.id.buttonCLogStartWarning,View.VISIBLE);
            notEmptyFlag = false;
        }

        // Check if the end date/time is/are empty
        if (endDate.getText().toString().equals("")
                || endTime.getText().toString().equals(""))
        {
            SetWarningVisibility(R.id.labelCLogEndWarning,R.id.buttonCLogEndWarning, View.VISIBLE);
            notEmptyFlag = false;
        }

        return notEmptyFlag;
    }

    /**
     * METHOD
     *
     * NAME
     *      IsEndAfterStart --> Checks if the end date/time is after the start date/time.
     *
     * SYNOPSIS
     *      private boolean IsEndAfterStart()
     *
     * DESCRIPTION
     *      This method checks if the end date/time is after the start date/time. If not, it
     *      displays a warning to the user.
     *
     * RETURNS
     *      @return boolean --> True if end date/time is after start date/time.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    private boolean IsEndAfterStart()
    {
        boolean correctSequenceFlag = true;
        if (!m_log.IsEndAfterStartTime())
        {
            // Display a warning message
            SetWarningVisibility(R.id.labelCLogEndWarning,R.id.buttonCLogEndWarning,View.VISIBLE);
            TextView warningLabel = findViewById(R.id.labelCLogEndWarning);
            warningLabel.setText(R.string.completeWarning_SafterE);
            correctSequenceFlag = false;
        }
        return correctSequenceFlag;
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current activity.
     *
     * SYNOPSIS
     *      private void Initialize(String a_message)
     *      @param a_message --> String: Message passed by the caller.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void Initialize(String a_message)
    {
        // Update the top bar
        SetTopBar();

        // Set super member variables
        SetMemberVariables(a_message);

        // Set Activity title
        SetActivityTitle(R.string.completeNew_title, R.string.completeEdit_title);

        // Populate task list
        InitializeTaskDropDown();

        // Set the data
        LoadLog();

        // Set up date and time edit boxes
        SetUpDateEditBox((EditText) findViewById(R.id.editCLogStartDate));
        SetUpDateEditBox((EditText) findViewById(R.id.editCLogEndDate));
        SetUpTimeEditBox((EditText) findViewById(R.id.editCLogStartTime));
        SetUpTimeEditBox((EditText) findViewById(R.id.editCLogEndTime));

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = findViewById(R.id.fabAECompleted);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: FAB object.
             *
             * DESCRIPTION
             *      This method runs whenever the Save Completed task action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                // Execute when fab save button is clicked
                SaveLogData();
            }
        });
    }
}
