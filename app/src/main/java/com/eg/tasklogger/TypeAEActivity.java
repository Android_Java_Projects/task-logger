// Package and libraries used
package com.eg.tasklogger;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;

/**
 * CLASS
 *
 * NAME
 *      TypeAEActivity
 *
 * SYNOPSIS
 *      public class TypeAEActivity extends TLBaseActivity
 *
 * DESCRIPTION
 *      This class handles the New/Edit Type Activity.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00am 3/2/2019
 */
public class TypeAEActivity extends TLBaseActivity
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Type m_type; // Task type object

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      PickColor --> Displays a color picker.
     *
     * SYNOPSIS
     *      public void PickColor(View a_view)
     *      @param a_view --> View: object calling this method.
     *
     * DESCRIPTION
     *      This method displays a color picker.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/10/2019
     */
    public void PickColor(View a_view)
    {
        ColorPickerDialogBuilder
                .with(this)
                .setTitle("Choose a color")
                .initialColor(m_type.Getm_color())
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton("ok", new ColorPickerClickListener()
                {
                    /**
                     * METHOD
                     *
                     * NAME
                     *      onClick --> Android Studio generated method. Runs on click.
                     *
                     * SYNOPSIS
                     *      public void onClick(DialogInterface a_dialog, int a_selectedColor
                     *          , Integer[] a_allColors)
                     *      @param a_dialog --> DialogInterface: Dialog object.
                     *      @param a_selectedColor --> int: A color represented as an integer.
                     *      @param a_allColors --> Integer[]: Array on int colors.
                     *
                     * DESCRIPTION
                     *      This method runs whenever the Ok button is clicked in the dialog box.
                     *
                     * RETURNS
                     *      void
                     *
                     * AUTHOR
                     *      Edgar Garcia
                     *
                     * DATE
                     *      8:00am 3/23/2019
                     */
                    @Override
                    public void onClick(DialogInterface a_dialog, int a_selectedColor
                            , Integer[] a_allColors)
                    {
                        // On ok, save the color selected with opacity removed.
                        String hexColor = Integer.toHexString(a_selectedColor).substring(2);
                        SetColor(a_selectedColor, hexColor);
                    }
                })
                .setNegativeButton("cancel", null)
                .build()
                .show();
    }

    /**
     * METHOD
     *
     * NAME
     *      DeleteRecord --> Deletes a Type record.
     *
     * SYNOPSIS
     *      public void DeleteRecord()
     *
     * DESCRIPTION
     *      This method sends a delete command to the database to remove the current Type record.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public void DeleteRecord()
    {
        // Do not allow deletion if the type is being used.
        if (m_databaseHelper.GetTypeUsageCount(m_type.Getm_id()) > 0)
        {
            SimpleDialog("Wait!","This task type is being used. If I allow you to " +
                    "delete, the data will become inconsistent. Imagine having a task related to " +
                    "a type that no longer exists...Sorry!");
            return;
        }

        // Prompt the user, if required.
        if (Getm_promptDelete())
        {
            //Put up a Yes/No message box
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setIcon(R.drawable.ic_warning)
                    .setTitle("Delete Task Type Record")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //Yes button clicked
                            ExecuteDelete(m_type);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else ExecuteDelete(m_type);
    }

    // 4th: Constructor -----------------------------------------------------------------------
    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the layout/activity opens.
     *
     * SYNOPSIS
     *      protected void onCreate(Bundle a_savedInstanceState)
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of your Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the layout/class.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    @Override
    protected void onCreate(Bundle a_savedInstanceState)
    {
        super.onCreate(a_savedInstanceState);
        setContentView(R.layout.activity_type_ae);

        // Get the transferred data from calling activity.
        Intent intent = getIntent();
        final String message = intent.getStringExtra("message");

        // Initialize other objects
        Initialize(message);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SaveTypeData --> Saves the Task Type data entered.
     *
     * SYNOPSIS
     *      private void SaveTypeData()
     *
     * DESCRIPTION
     *      This method saves the Task Type data entered, into a database.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    private void SaveTypeData()
    {
        // Check if the required data was entered
        if (!IsRequiredEntered()) return;

        // Set name
        EditText typeName = findViewById(R.id.editTypeName);
        m_type.Setm_name(typeName.getText().toString());

        // Edit/Add a type in the database
        boolean updateResult;
        int action = Getm_actionType();
        if (action == EDITRECORD) updateResult = m_databaseHelper.UpdateType(m_type);
        else
        {
            updateResult = m_databaseHelper.AddType(m_type);
            m_type.Setm_id(m_databaseHelper.GetLastID("type"));
        }

        // Notify user
        if (updateResult)
        {
            String actionMessage = ((Getm_actionType() == EDITRECORD) ? "Updated" : "Saved");
            Toast.makeText(this, actionMessage + " successfully",
                    Toast.LENGTH_SHORT).show();

            // Set pending data
            ((TLApplication) this.getApplication()).Setm_pending(m_type.Getm_id()
                    ,Getm_position(),action);

            finish();
        }
        else Toast.makeText(this, "There was an error, please try again",
                    Toast.LENGTH_SHORT).show();
    }

    /**
     * METHOD
     *
     * NAME
     *      IsRequiredEntered --> Checks if all required data was entered.
     *
     * SYNOPSIS
     *      private boolean IsRequiredEntered()
     *
     * DESCRIPTION
     *      This method checks if all required data was entered. If false, the application
     *      displays a warning to the user.
     *
     * RETURNS
     *      @return boolean --> True if all required data was entered.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    private boolean IsRequiredEntered()
    {
        // Check if the name is empty
        EditText typeName = findViewById(R.id.editTypeName);
        if (typeName.getText().toString().equals(""))
        {
            TextView nameWarningLabel = findViewById(R.id.labelTypeNameWarning);
            nameWarningLabel.setVisibility(View.VISIBLE);

            Button nameWarningButton = findViewById(R.id.buttonTypeNameWarning);
            nameWarningButton.setVisibility(View.VISIBLE);

            Toast.makeText(this, "Oops! You forgot the name.",
                    Toast.LENGTH_SHORT).show();
            return  false;
        }
        return true;
    }

    /**
     * METHOD
     *
     * NAME
     *      LoadType --> Loads Type data into the appropriate objects.
     *
     * SYNOPSIS
     *      private void LoadType()
     *
     * DESCRIPTION
     *      This method loads Type data into the appropriate objects.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    private void LoadType()
    {
        // Check if new or edit
        if (Getm_actionType() == EDITRECORD)
        {
            // Trying to edit a record
            m_type = new Type();
            m_type = m_databaseHelper.GetType(Getm_recordID());
        }
        else
        {
            // This is a new record, set defaults.
            m_type = new Type(0,"",0,-948174,"f1af42");
        }

        SetColor(m_type.Getm_color(),m_type.Getm_colorHex() );

        // Populate name
        EditText inputName = findViewById(R.id.editTypeName);
        inputName.setText(m_type.Getm_name());
    }

    /**
     * METHOD
     *
     * NAME
     *      SetColor --> Sets the type color.
     *
     * SYNOPSIS
     *      private void SetColor(int a_color, String a_hexColor)
     *      @param a_color --> int: color represented by an integer.
     *      @param a_hexColor --> String: color represented by a hexadecimal String.
     *
     * DESCRIPTION
     *      This method sets the type color according to user selection and class initialization.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/10/2019
     */
    private void SetColor(int a_color, String a_hexColor)
    {
        // Change button color
        Button colorButton = findViewById( R.id.buttonTypeColor);
        GradientDrawable drawable = (GradientDrawable) colorButton.getBackground();
        drawable.setColor(a_color);

        // Display color in hex
        EditText inputColor = findViewById(R.id.editTypeColor);
        inputColor.setText(a_hexColor);

        // Save color data
        m_type.Setm_color(a_color);
        m_type.Setm_colorHex(a_hexColor);
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current activity.
     *
     * SYNOPSIS
     *      private void Initialize(String a_message)
     *      @param a_message --> String: message sent by the calling activity.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void Initialize(String a_message)
    {
        // Update the top bar
        SetTopBar();

        // Set super member variables
        SetMemberVariables(a_message);

        // Set Activity title
        SetActivityTitle(R.string.typeNew_title, R.string.typeEdit_title);

        // Disable and set a click listener to the hex color edit box
        EditText hexColor = findViewById(R.id.editTypeColor);
        hexColor.setKeyListener(null);
        hexColor.setFocusable(false);
        hexColor.setCursorVisible(false);
        hexColor.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method, this method is called when
             *          the user clicks on the hexadecimal edit box.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Hexadecimal Edit box.
             *
             * DESCRIPTION
             *      This method is called when the user clicks on the hexadecimal edit box.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      4:04pm 2/21/2019
             */
            @Override
            public void onClick(View a_view)
            {
                PickColor(findViewById( R.id.buttonTypeColor));
            }

        });

        // Set the data
        LoadType();

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = findViewById(R.id.fabAEType);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Save Task Type action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                SaveTypeData();
            }
        });
    }
}
