// Package and libraries used
package com.eg.tasklogger;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.TextView;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;
import static com.eg.tasklogger.TLApplication.NEWRECORD;
import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;
import static com.eg.tasklogger.TLApplication.RUNNINGLOG;

/**
 * CLASS
 *
 * NAME
 *      HomeFragment
 *
 * SYNOPSIS
 *      public class HomeFragment extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Home fragment. This is where all the "running" tasks are.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      10:00am 2/22/2019
 */
public class HomeFragment extends Fragment
{
     /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private List<Log> m_lstLog;                        // Holds all the tasks data
    private LogRecyclerViewAdapter m_recyclerAdapter;  // Adapter that links recycler to data array
    DatabaseHelper m_databaseHelper;                   // Database connector
    private String m_fromDate;                         // Start date bracket to display
    private String m_toDate;                           // End date bracket to display
    private View m_view;                               // Current fragment view

    // 3rd: UI Components --------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      HomeFragment --> Class constructor.
     *
     * SYNOPSIS
     *      public HomeFragment()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    public HomeFragment()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *          when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_home, a_container, false);

        // Initialize Class
        Initialize(view);

        return view;
    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the fragment becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the fragment becomes active. It checks if there is
     *      any "pending" data that will update the array/recycler view. It also sets the clock
     *      to its corresponding state.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Sets the clock
        SetClock(m_view);

        // Check if a new task type should be added/updated in the recycler
        if (getActivity() != null)
        {
            int pendingRecordID = ((TLApplication) getActivity().getApplication()).Getm_pendingRecordID();
            if (pendingRecordID != NOTANINDEXNORID) UpdateRecycler(pendingRecordID);
            else UpdateEndTime();
        }
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      UpdateRecycler --> Updates the recycler view.
     *
     * SYNOPSIS
     *      private void UpdateRecycler(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the array related to the recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/14/2019
     */
    private void UpdateRecycler(int a_pendingRecordID)
    {
        int pendingIndex = 0;
        int pendingAction = 0;

        if (getActivity() != null)
        {
            pendingIndex = ((TLApplication) getActivity().getApplication()).Getm_pendingIndex();
            pendingAction = ((TLApplication) getActivity().getApplication()).Getm_pendingAction();
        }

        switch (pendingAction)
        {
            case NEWRECORD:
                Log newLog = m_databaseHelper.GetLog(a_pendingRecordID);
                m_lstLog.add(newLog);
                break;

            case EDITRECORD:
                Log editedLog = m_databaseHelper.GetLog(a_pendingRecordID);
                m_lstLog.set(pendingIndex,editedLog);
                break;

            case DELETERECORD:
                m_lstLog.remove(pendingIndex);
                break;
        }

        // Update the log end times to now
        UpdateEndTime();

        // Set the clock
        SetClock(m_view);
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateEndTime --> Updates the log end times.
     *
     * SYNOPSIS
     *      private void UpdateEndTime()
     *
     * DESCRIPTION
     *      This method updates the end timestamp on all logs in the recycler object. All
     *      logs end times are updated to the current time.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/14/2019
     */
    private void UpdateEndTime()
    {
        // Update all log end times to current time
        long currentTime = System.currentTimeMillis();
        Timestamp TS = new Timestamp(currentTime);
        for (Log l:m_lstLog) if (l.Getm_end().before(TS)) l.Setm_end(TS);

        // Update recycler and reset pending to process data.
        m_recyclerAdapter.notifyDataSetChanged();

        if (getActivity() != null)
            ((TLApplication) getActivity().getApplication()).ResetPending();
    }

    /**
     * METHOD
     *
     * NAME
     *      AddRunningTask --> Calls the activity that set a task to be tracked.
     *
     * SYNOPSIS
     *      private void AddRunningTask()
     *
     * DESCRIPTION
     *      This method calls the activity in charge of setting a task to be tracked.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void AddRunningTask()
    {
        if (getActivity() != null)
        {
            Intent myIntent = new Intent(getActivity(), StartTaskActivity.class);
            myIntent.putExtra("message", "new|0|0" );
            getActivity().startActivity(myIntent);
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      InitializeDateRange --> Initializes a date range.
     *
     * SYNOPSIS
     *      private void InitializeDateRange()
     *
     * DESCRIPTION
     *      This method sets a date range used to pull running tasks from the database.
     *      The fromDate is set as far as possible and the toDate is set to the current
     *      timestamp + 3 minutes. The 3 minutes in the future is a safeguard, since the end
     *      time of a newly created log is to 1 minute into the future.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void InitializeDateRange()
    {
        // Set the From date as far as possible
        m_fromDate = "0001-01-01 00:00:00";

        // Set to the current timestamp + 3 minutes
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        cal.add(Calendar.MINUTE, 3);
        m_toDate = dateFormat.format(cal.getTime());
    }

    /**
     * METHOD
     *
     * NAME
     *      SetClock --> Initializes the clock.
     *
     * SYNOPSIS
     *      private void SetClock(View a_view)
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the clock. If there are running tasks the clock position
     *      is set to the screen's bottom left, the source html code is updated, and
     *      the "Nothing running..." label's visibility is set to GONE. Otherwise, of the clock's
     *      source html code is updated and other initial settings are preserved.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void SetClock(View a_view)
    {
        WebView tlClock = a_view.findViewById(R.id.home_clock);
        WebSettings webSettings = tlClock.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Check if there is any running log
        String urlPath;
        int gravitySet;
        int textVisibility;
        if (m_recyclerAdapter.getItemCount() > 0)
        {
            urlPath = "file:///android_asset/TLclockSmall.html";
            gravitySet = Gravity.BOTTOM|Gravity.START;
            textVisibility = View.INVISIBLE;
        }
        else
        {
            urlPath = "file:///android_asset/TLclock.html";
            gravitySet = Gravity.FILL_HORIZONTAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
            textVisibility = View.VISIBLE;
        }

        // Position the clock accordingly
        tlClock.loadUrl(urlPath);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                gravitySet);
        tlClock.setLayoutParams(params);

        // Hide/un-hide the Nothing running message
        TextView nothingRunning = a_view.findViewById(R.id.main_nothing);
        nothingRunning.setVisibility(textVisibility);
    }

    /**
     * METHOD
     *
     * NAME
     *      SetRefreshTimer --> Sets a timer
     *
     * SYNOPSIS
     *      private void SetRefreshTimer()
     *
     * DESCRIPTION
     *      This method sets a timer that runs every 1 minute.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/15/2019
     */
    private void SetRefreshTimer()
    {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable()
        {
            /**
             * METHOD
             *
             * NAME
             *      run( --> Android Studio generated method, this method runs a method every
             *      1 minute.
             *
             * SYNOPSIS
             *      public void run()
             *
             * DESCRIPTION
             *      This method runs a method every 1 minute. The method updates the end time of
             *      every log displayed in the recycler.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00pm 4/15/2019
             */
            @Override
            public void run()
            {
                try
                {
                    // Update the log end times to now
                    UpdateEndTime();
                }
                catch (Exception e)
                {
                    // Exception
                }
                finally
                {
                    //also call the same runnable to call it at regular interval
                    handler.postDelayed(this, 60000);
                }
            }
        };
        handler.postDelayed(runnable, 60000);
    }
    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        m_view = a_view;

        // Initialize date brackets to display
        InitializeDateRange();

        // Set member variables
        m_databaseHelper = new DatabaseHelper(getContext());
        m_lstLog = new ArrayList<>();
        m_lstLog = m_databaseHelper.GetMultipleLogs(RUNNINGLOG,m_fromDate,
                m_toDate, "ASC");

        // Update the top bar
        if (getActivity() != null)
            ((MainActivity) getActivity()).SetActionBarTitle(R.string.main_title);

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = a_view.findViewById(R.id.fabNewLog);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Add New Running Task action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                AddRunningTask();
            }
        });

        // Set the Recycler/List
        RecyclerView recyclerView = a_view.findViewById(R.id.recyclerView_Running);
        m_recyclerAdapter = new LogRecyclerViewAdapter(getContext(),m_lstLog,
                1, RUNNINGLOG );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(m_recyclerAdapter);

        // Set a log refresh timer
        SetRefreshTimer();
    }
}
