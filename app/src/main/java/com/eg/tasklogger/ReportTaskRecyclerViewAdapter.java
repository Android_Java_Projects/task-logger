// Package and libraries used
package com.eg.tasklogger;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import java.util.List;

/**
 * CLASS
 *
 * NAME
 *      ReportTaskRecyclerViewAdapter
 *
 * SYNOPSIS
 *      class ReportTaskRecyclerViewAdapter
 *         extends RecyclerView.Adapter<ReportTaskRecyclerViewAdapter.TaskViewHolder>
 *
 * DESCRIPTION
 *      This class is an adapter that links the recycler view to list holding ReportTask objects.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/26/2019
 */
class ReportTaskRecyclerViewAdapter
        extends RecyclerView.Adapter<ReportTaskRecyclerViewAdapter.TaskViewHolder>
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Context m_context;          // Context to be used by recycler view
    private List<ReportTask> m_task;    // Array/List of ReportTask objects
    private int m_lastPosition;         // Allows to remember the last item shown on screen

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * CLASS
     *
     * NAME
     *      TaskViewHolder --> Helper class which holds report task data.
     *
     * SYNOPSIS
     *      static class TaskViewHolder extends RecyclerView.ViewHolder
     *
     * DESCRIPTION
     *      This class holds report task data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    static class TaskViewHolder extends RecyclerView.ViewHolder
    {
        private TextView mt_TypeColor;
        private TextView mt_TypeName;
        private TextView mt_TaskName;
        private TextView mt_totalTime;

        /**
         * METHOD
         *
         * NAME
         *      TaskViewHolder --> Class constructor.
         *
         * SYNOPSIS
         *      TaskViewHolder(@NonNull View a_itemView)
         *      @param a_itemView --> View: View holding report task data
         *
         * DESCRIPTION
         *      This method is the class constructor for the inner class.
         *
         * RETURNS
         *      void
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 4/28/2019
         */
        TaskViewHolder(@NonNull View a_itemView)
        {
            super(a_itemView);

            // Data to display
            mt_TypeColor =  a_itemView.findViewById(R.id.taskItem_typeColor);
            mt_TypeName = a_itemView.findViewById(R.id.taskItem_typeName);
            mt_TaskName = a_itemView.findViewById(R.id.taskItem_name);
            mt_totalTime =  a_itemView.findViewById(R.id.taskItem_description);
        }
    }

    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      ReportTaskRecyclerViewAdapter --> Class constructor.
     *
     * SYNOPSIS
     *      ReportTaskRecyclerViewAdapter(Context a_context, List<ReportTask> a_reportTask)
     *      @param a_context --> Context: Context to set
     *      @param a_reportTask --> List<ReportTask>: List of ReportTask objects
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    ReportTaskRecyclerViewAdapter(Context a_context, List<ReportTask> a_reportTask)
    {
        this.m_context = a_context;
        this.m_task = a_reportTask;
        this.m_lastPosition = -1;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateViewHolder --> Android Studio generated method, this method executes
     *          when the view holder is created.
     *
     * SYNOPSIS
     *      public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup,
     *          int a_viewType)
     *      @param a_viewGroup --> ViewGroup: Group of views.
     *      @param a_viewType --> int: Type of view.
     *
     * DESCRIPTION
     *      This method executes when the view holder is created.
     *
     * RETURNS
     *      @return TypeViewHolder: View Holder for Types.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup a_viewGroup, int a_viewType)
    {
        View view;
        view = LayoutInflater.from(m_context).inflate(R.layout.fragitem_task
                ,a_viewGroup,false);
        return new TaskViewHolder(view);
    }

    /**
     * METHOD
     *
     * NAME
     *      onBindViewHolder --> Android Studio generated method, this method executes
     *          when the view is bound.
     *
     * SYNOPSIS
     *      public void onBindViewHolder(@NonNull final TaskViewHolder a_holder,
     *          final int a_position)
     *      @param a_holder --> TaskViewHolder: View Holder for Tasks.
     *      @param a_position --> int: Index in list.
     *
     * DESCRIPTION
     *      This method executes  when the view is bound.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    @Override
    public void onBindViewHolder(@NonNull final TaskViewHolder a_holder, final int a_position)
    {
        // Type color
        GradientDrawable drawable = (GradientDrawable) a_holder.mt_TypeColor.getBackground();
        drawable.setColor(m_task.get(a_position).Getm_typeColor());

        // Type name
        a_holder.mt_TypeName.setText(m_task.get(a_position).Getm_typeName());

        // Task name
        a_holder.mt_TaskName.setText(m_task.get(a_position).Getm_taskName());

        // Total time
        Double timeUHR = m_task.get(a_position).Getm_totalTime();
        String timePassed = ((TLApplication) m_context.getApplicationContext()).GetReadableTime(timeUHR);
        a_holder.mt_totalTime.setText(timePassed);

        // Animation when the view is bound
        SetAnimation(a_holder.itemView, a_position);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      getItemCount --> Android Studio generated method, this method returns the number
     *          of ReportTasks in the recycler.
     *
     * SYNOPSIS
     *      public int getItemCount()
     *
     * DESCRIPTION
     *      This method returns the number of ReportTasks in the recycler.
     *
     * RETURNS
     *      @return int --> Number of ReportTasks in the recycler.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    @Override
    public int getItemCount()
    {
        return m_task.size();
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SetAnimation --> Sets an animation when a new bound view is added to the recycler.
     *
     * SYNOPSIS
     *      private void SetAnimation(View a_viewToAnimate, int a_position)
     *      @param a_viewToAnimate --> View: View to animate when bound.
     *      @param a_position --> int: Index number.
     *
     * DESCRIPTION
     *      This method sets a animation that executes when a new bound view is added
     *      to the recycler.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/26/2019
     */
    private void SetAnimation(View a_viewToAnimate, int a_position)
    {
        // If the bound view wasn't previously displayed on screen, animate it
        if (a_position > m_lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(m_context,  android.R.anim.fade_in);
            a_viewToAnimate.startAnimation(animation);
            m_lastPosition = a_position;
        }
    }
}
