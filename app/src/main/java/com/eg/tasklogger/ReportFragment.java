// Package and libraries used
package com.eg.tasklogger;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.eg.tasklogger.TLApplication.COMPLETELOG;

/**
 * CLASS
 *
 * NAME
 *      ReportFragment
 *
 * SYNOPSIS
 *      public class ReportFragment extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Report fragment. This fragments is used to set a start and end
 *      date used as parameters to generate reports.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      10:00am 2/22/2019
 */
public class ReportFragment extends Fragment
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Timestamp m_fromDate;       // Start date bracket to display
    private Timestamp m_toDate;         // End date bracket to display
    private View m_view;                // Report fragment view

    // 3rd: UI Components --------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      ReportFragment --> Class constructor.
     *
     * SYNOPSIS
     *      public ReportFragment()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    public ReportFragment()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *          when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_report, a_container, false);

        // Initialize Class
        Initialize(view);

        return view;
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      IsFromEmpty --> Checks if the From date is empty.
     *
     * SYNOPSIS
     *      private boolean IsFromEmpty()
     *
     * DESCRIPTION
     *      This method checks if the From date is empty. If it is empty, it
     *      displays a warning to the user.
     *
     * RETURNS
     *      @return boolean --> True if the From date is not empty.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/21/2019
     */
    private boolean IsFromEmpty()
    {
        EditText fromDate = m_view.findViewById(R.id.editReportFromDate);
        SetWarningVisibility(R.id.labelReportFromWarning,R.id.buttonReportFromWarning,View.INVISIBLE);
        if (fromDate.getText().toString().equals(""))
        {
            SetWarningVisibility(R.id.labelReportFromWarning,R.id.buttonReportFromWarning,View.VISIBLE);
            return false;
        }
        return true;
    }

    /**
     * METHOD
     *
     * NAME
     *      IsValidDates --> Checks if the To date is after the From date.
     *
     * SYNOPSIS
     *      private boolean IsValidDates()
     *
     * DESCRIPTION
     *      This method checks if the To date is after the From date. If not, it
     *      displays a warning to the user.
     *
     * RETURNS
     *      @return boolean --> True if the To date is after the From date.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/21/2019
     */
    private boolean IsValidDates()
    {
        SetWarningVisibility(R.id.labelReportToWarning,R.id.buttonReportToWarning,View.INVISIBLE);

        if (!m_toDate.after(m_fromDate))
        {
            SetWarningVisibility(R.id.labelReportToWarning,R.id.buttonReportToWarning,View.VISIBLE);
            return false;
        }
        return true;
    }

    /**
     * METHOD
     *
     * NAME
     *      SetWarningVisibility --> Sets the visibility of warning text and buttons.
     *
     * SYNOPSIS
     *      private void SetWarningVisibility(int a_WarningTextView, int a_WarningButton
     *          , int a_visible)
     *      @param a_warningTextView --> int: Text View object id.
     *      @param a_warningButton --> int: Button View object id.
     *      @param a_visible --> int: Visibility code.
     *
     * DESCRIPTION
     *      This method Sets the visibility of warning text and buttons.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    public void SetWarningVisibility(int a_warningTextView, int a_warningButton, int a_visible)
    {
        TextView warningLabel = m_view.findViewById(a_warningTextView);
        warningLabel.setVisibility(a_visible);

        Button warningButton = m_view.findViewById(a_warningButton);
        warningButton.setVisibility(a_visible);
    }

    /**
     * METHOD
     *
     * NAME
     *      SetUpDateEditBox --> Sets up a date edit box
     *
     * SYNOPSIS
     *      private void SetUpDateEditBox(EditText a_dateEditText)
     *      @param a_dateEditText --> EditText: Edit box to set.
     *
     * DESCRIPTION
     *      This method sets a date edit box. Removes the key press listener,
     *      makes it un-focusable, removes the cursor, and sets an onClick listener.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    private void SetUpDateEditBox(EditText a_dateEditText)
    {
        a_dateEditText.setKeyListener(null);
        a_dateEditText.setFocusable(false);
        a_dateEditText.setCursorVisible(false);
        a_dateEditText.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method, this method is called when
             *          the user clicks on a date edit box.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Date Edit box.
             *
             * DESCRIPTION
             *      This method is called when the user clicks on a date edit box.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:04pm 4/3/2019
             */
            @Override
            public void onClick(View a_view)
            {
                PickADate(a_view);
            }
        });
    }

    /**
     * METHOD
     *
     * NAME
     *      PickADate --> Display a date picker.
     *
     * SYNOPSIS
     *      private void PickADate(View a_view)
     *      @param a_view --> View: Date Edit box calling this method.
     *
     * DESCRIPTION
     *      This method displays a date picker and saves the date.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    private void PickADate(View a_view)
    {
        final int clickedDateEditBox = a_view.getId();

        // Get the log's day, month, and year
        long ts;
        if (clickedDateEditBox == R.id.editReportFromDate) ts = m_fromDate.getTime();
        else ts = m_toDate.getTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ts);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        if (getContext() == null) return;

        DatePickerDialog datePicker = new DatePickerDialog(getContext()
                , new DatePickerDialog.OnDateSetListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onDateSet --> Android Studio generated method, this method is called when
             *          the user sets a date in the date picker.
             *
             * SYNOPSIS
             *      public void onDateSet(DatePicker datePicker, int a_year, int a_month
             *          , int a_day)
             *      @param datePicker --> DatePicker: Date picker object.
             *      @param a_year --> int: Number representing a year.
             *      @param a_month --> int: Number representing a month.
             *      @param a_day --> int: Number representing a day.
             *
             * DESCRIPTION
             *      This method is called when the user sets a date in the date picker.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 4/4/2019
             */
            @Override
            public void onDateSet(DatePicker datePicker, int a_year, int a_month, int a_day)
            {
                // Edit box to set the date to
                EditText editDate = m_view.findViewById(clickedDateEditBox);

                // Set date into format yyyy-MM-dd
                String dateText = Integer.toString(a_year) + "-"
                        + Integer.toString(a_month+1) + "-" + Integer.toString(a_day);
                String time = (clickedDateEditBox == R.id.editReportFromDate) ?
                        " 00:00:00" : " 23:59:59";
                dateText = dateText + time;
                try
                {
                    SimpleDateFormat sdfyMd = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat sdfDMdy = new SimpleDateFormat(
                            "EEEE, MMMM dd yyyy", Locale.getDefault());

                    Date date = sdfyMd.parse(dateText);
                    editDate.setText(sdfDMdy.format(date));

                    // Check if the start date was clicked
                    if (clickedDateEditBox == R.id.editReportFromDate)
                    {
                        m_fromDate = new Timestamp(date.getTime());
                        IsFromEmpty();
                    }
                    else m_toDate = new Timestamp(date.getTime());

                    IsValidDates();
                }
                catch (ParseException exp)
                {
                    exp.printStackTrace();
                }
            }
        }, year, month, day);
        datePicker.show();
    }

    /**
     * METHOD
     *
     * NAME
     *      SetFromToDate --> Initializes a date range.
     *
     * SYNOPSIS
     *      private void SetFromToDate()
     *
     * DESCRIPTION
     *      This method initializes the From/To date. The To date is set to the current date.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      3:00pm 4/21/2019
     */
    private void SetFromToDate()
    {
        // Set the From and To dates to today's date
        Date date= new Date();
        Timestamp ts = new Timestamp(date.getTime());
        m_fromDate = ts;
        m_toDate = ts;

        if (getActivity() != null)
        {
            // Set the To date to today's date and display it as a string
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEEE, MMMM dd yyyy", Locale.getDefault());
            EditText toDate = m_view.findViewById(R.id.editReportToDate);
            toDate.setText(dateFormat.format(date));
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      ShowResults --> Calls the activity that displays the Report results.
     *
     * SYNOPSIS
     *      private void ShowResults()
     *
     * DESCRIPTION
     *      This method calls the activity that displays the report results. It sends the
     *      From and To date as a message to the Activity called.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void ShowResults()
    {
        // Check the From/To dates
        if (!IsFromEmpty() || !IsValidDates()) return;

        // Check if there is an activity running
        if (getActivity() == null) return;

        // Check if there is at least one log registered in the date range entered
        DatabaseHelper database = new DatabaseHelper(getContext());
        if (database.GetMultipleLogs(COMPLETELOG,m_fromDate.toString(),
            m_toDate.toString(), "ASC").size() < 1)
        {
            //Put up a Yes/No message box
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder
                    .setTitle("Wait!")
                    .setMessage("There is nothing registered in the date range entered. " +
                            "Please try a different date range.")
                    .setPositiveButton("Ok", null)
                    .show();
            return;
        }

        // Set the pie chart title
        String fromToPieTitle = GetDateFromTS(m_fromDate)
                + "..." + GetDateFromTS(m_toDate);

        // Call the activity to show report results
        String messageToSend = m_fromDate.toString() + "|" + m_toDate.toString()
                + "|" + fromToPieTitle;
        Intent myIntent = new Intent(getActivity(), ReportResultActivity.class);
        myIntent.putExtra("message", messageToSend);
        getActivity().startActivity(myIntent);
    }

    /**
     * METHOD
     *
     * NAME
     *      GetDateFromTS --> Get the date from a timestamp.
     *
     * SYNOPSIS
     *      private String GetDateFromTS(Timestamp a_timeStamp, String a_pattern)
     *      @param a_timeStamp --> Timestamp: Timestamp to extract the date from.
     *
     * DESCRIPTION
     *      This method gets the date from a timestamp. It returns the date as a
     *      string.
     *
     * RETURNS
     *      @return String --> Date as a string.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      3:00pm 4/24/2019
     */
    private String GetDateFromTS(Timestamp a_timeStamp)
    {
        Date date = new Date();
        date.setTime(a_timeStamp.getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MM/dd/yyyy", Locale.getDefault());
        return dateFormat.format(date);
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        m_view = a_view;

        // Update the top bar
        if (getActivity() != null)
            ((MainActivity) getActivity()).SetActionBarTitle(R.string.report_title);

        // Set initial timestamp values
        SetFromToDate();

        // Set up date and time edit boxes
        SetUpDateEditBox((EditText) m_view.findViewById(R.id.editReportFromDate));
        SetUpDateEditBox((EditText) m_view.findViewById(R.id.editReportToDate));

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = a_view.findViewById(R.id.fabReport);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Show Results action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                ShowResults();
            }
        });
    }
}
