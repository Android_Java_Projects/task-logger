// Package and libraries used
package com.eg.tasklogger;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.eg.tasklogger.TLApplication.RUNNINGLOG;

/**
 * CLASS
 *
 * NAME
 *      Log
 *
 * SYNOPSIS
 *      public class Log
 *
 * DESCRIPTION
 *      This class is used to handle Log objects.  Logs are used in many parts of this application.
 *      It is used for running tasks, completed task, and also in the reports module.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/01/2019
 */
public class Log
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private int m_id;           // Log id
    private int m_status;       // Log status
    private String m_notes;     // Log notes
    private Timestamp m_start;  // Log start date and time
    private Timestamp m_end;    // Log end date and time
    private Task m_task;        // Related Task

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      UpdateDate --> Updates the date part of a timestamp.
     *
     * SYNOPSIS
     *      public void UpdateDate(String a_type, String a_date)
     *      @param a_type --> String: The timestamp to update (start or end).
     *      @param a_date --> String: The date to update the timestamp to.
     *
     * DESCRIPTION
     *      This method updates the start/end date part of a timestamp.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    void UpdateDate(String a_type, String a_date)
    {
        Timestamp timestampBackUP = new Timestamp(System.currentTimeMillis());
        try
        {
            String SEtime = (a_type.equals("start")) ? GetTimeString(m_start) : GetTimeString(m_end);
            String dateTime = a_date + " " + SEtime;
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss",Locale.getDefault());
            Date parsedDate = dateFormat.parse(dateTime);
            Timestamp timestampParsed = new java.sql.Timestamp(parsedDate.getTime());

            // Updated the appropriate member variable
            if (a_type.equals("start")) m_start = timestampParsed;
            else m_end = timestampParsed;
        }
        catch(Exception e)
        {
            // If there is an exception, set the member variables to the current timestamp
            if (a_type.equals("start")) m_start = timestampBackUP;
            else m_end = timestampBackUP;
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateTime --> Updates the time part of a timestamp.
     *
     * SYNOPSIS
     *      public void UpdateTime(String a_type, String a_time)
     *      @param a_type --> String: The timestamp to update (start or end).
     *      @param a_time --> String: The time to update the timestamp to.
     *
     * DESCRIPTION
     *      This method updates the start/end time part of a timestamp.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    void UpdateTime(String a_type, String a_time)
    {
        Timestamp timestampBackUP = new Timestamp(System.currentTimeMillis());
        try
        {
            String SEdate = (a_type.equals("start")) ? GetDateString(m_start) : GetDateString(m_end);
            String dateTime = SEdate + " " + a_time;
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date parsedDate = dateFormat.parse(dateTime);
            Timestamp timestampParsed = new java.sql.Timestamp(parsedDate.getTime());

            // Updated the appropriate member variable
            if (a_type.equals("start")) m_start = timestampParsed;
            else m_end = timestampParsed;
        }
        catch(Exception e)
        {
            // If there is an exception, set the member variables to the current timestamp
            if (a_type.equals("start")) m_start = timestampBackUP;
            else m_end = timestampBackUP;
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      IsEndAfterStartTime --> Checks if the end date/time is after the start date/time.
     *
     * SYNOPSIS
     *      public boolean IsEndAfterStartTime()
     *
     * DESCRIPTION
     *      This method checks if the end date/time is after the start date/time.
     *
     * RETURNS
     *      @return --> boolean: True if the end date/time is after the start date/time
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    boolean IsEndAfterStartTime()
    {
        return m_end.after(m_start);
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_startString --> Returns the log start date and time as a string.
     *
     * SYNOPSIS
     *      public String Getm_startString()
     *
     * DESCRIPTION
     *      This method returns the log start timestamp as a string.
     *
     * RETURNS
     *      @return String --> Log star date and time.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    String Getm_startString()
    {
        return GetNiceDate(m_start);
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_endString --> Returns the log end date and time as a string.
     *
     * SYNOPSIS
     *      public String Getm_endString()
     *
     * DESCRIPTION
     *      This method returns the log end date and time as a string.
     *
     * RETURNS
     *      @return String --> Log end date and time.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    String Getm_endString()
    {
        return GetNiceDate(m_end);
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_elapsedTimeUHR --> Returns the elapsed time between start and end.
     *
     * SYNOPSIS
     *      public Double Getm_elapsedTimeUHR()
     *
     * DESCRIPTION
     *      This method returns the elapsed time between start and end. The elapsed time is
     *      a double that represents the amount of elapsed hours. For example,
     *      if 3 hours 30 minutes have passed between start and end time, the return value will
     *      be 3.5 (30 minutes equals to .5 since it is 1/2 an hour).
     *
     * RETURNS
     *      @return Double --> Elapsed time.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/07/2019
     */
    Double Getm_elapsedTimeUHR()
    {
        // Difference in milliseconds
        long lapse = m_end.getTime() - m_start.getTime();

        long secondsInMilli = (long) 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        Long elapsedDays = lapse / daysInMilli;
        lapse = lapse % daysInMilli;

        long elapsedHours = lapse / hoursInMilli;
        lapse = lapse % hoursInMilli;

        Long elapsedMinutes = lapse / minutesInMilli;

        return (elapsedDays.doubleValue() * 24) + elapsedHours
                + (elapsedMinutes.doubleValue()/60);
    }

    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Task --> Class constructor.
     *
     * SYNOPSIS
     *      public Log()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/9/2019
     */
    public Log()
    {
        this.m_id = 0;
        this.m_status = RUNNINGLOG;
        this.m_notes = "";

        long time = System.currentTimeMillis();
        this.m_start = new Timestamp(time);
        this.m_end = new Timestamp(time);

        this.m_task = new Task();
    }

    /**
     * METHOD
     *
     * NAME
     *      Task --> Overloaded Class constructor.
     *
     * SYNOPSIS
     *      public Log(int a_id, int a_status, String a_notes, Task a_task)
     *      @param a_id --> int: Log id.
     *      @param a_status --> int: Log status.
     *      @param a_notes --> String: Log notes.
     *      @param a_task --> Task: Task object related to the log.
     *
     * DESCRIPTION
     *      This method is an overloaded class constructor. Used to set member variables on
     *      instantiation.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/9/2019
     */
    public Log(int a_id, int a_status, String a_notes, Task a_task)
    {
        this.m_id = a_id;
        this.m_status = a_status;
        this.m_notes = a_notes;

        long time = System.currentTimeMillis();
        this.m_start = new Timestamp(time);
        this.m_end = new Timestamp(time);

        this.m_task = new Task();
        m_task = a_task;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Getm_Id --> Returns the log id.
     *
     * SYNOPSIS
     *      public int Getm_id()
     *
     * DESCRIPTION
     *      This method returns the log id.
     *
     * RETURNS
     *      @return int --> Log id.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    public int Getm_id()
    {
        return m_id;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_status --> Returns the log status.
     *
     * SYNOPSIS
     *      public int Getm_status()
     *
     * DESCRIPTION
     *      This method returns the log status.
     *
     * RETURNS
     *      @return int --> Log status.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    int Getm_status()
    {
        return m_status;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_notes --> Returns the log notes.
     *
     * SYNOPSIS
     *      public String Getm_notes()
     *
     * DESCRIPTION
     *      This method returns the log notes.
     *
     * RETURNS
     *      @return String --> Log notes.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    String Getm_notes()
    {
        return m_notes;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_start --> Returns the log start timestamp.
     *
     * SYNOPSIS
     *      public Timestamp Getm_start()
     *
     * DESCRIPTION
     *      This method returns the log start timestamp.
     *
     * RETURNS
     *      @return Timestamp --> Log start timestamp.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    Timestamp Getm_start()
    {
        return m_start;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_end --> Returns the log end timestamp.
     *
     * SYNOPSIS
     *      public Timestamp Getm_end()
     *
     * DESCRIPTION
     *      This method returns the log end timestamp.
     *
     * RETURNS
     *      @return Timestamp --> Log end timestamp.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    Timestamp Getm_end()
    {
        return m_end;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_taskId --> Returns the task id.
     *
     * SYNOPSIS
     *      public int Getm_taskId()
     *
     * DESCRIPTION
     *      This method returns the log's task id.
     *
     * RETURNS
     *      @return int --> Task id number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    int Getm_taskId()
    {
        return m_task.Getm_id();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_taskName --> Returns the task name.
     *
     * SYNOPSIS
     *      public String Getm_taskName()
     *
     * DESCRIPTION
     *      This method returns the log's task name.
     *
     * RETURNS
     *      @return String --> Task name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    String Getm_taskName()
    {
        return m_task.Getm_name();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typeColor --> Returns the task type color.
     *
     * SYNOPSIS
     *      public int Getm_typeColor()
     *
     * DESCRIPTION
     *      This method returns the log's task type color.
     *
     * RETURNS
     *      @return int --> Task type color.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    public int Getm_typeColor()
    {
        return m_task.Getm_typeColor();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typeName --> Returns the task type name.
     *
     * SYNOPSIS
     *      public String Getm_typeName()
     *
     * DESCRIPTION
     *      This method returns the log's task type name.
     *
     * RETURNS
     *      @return String --> Task type name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    public String Getm_typeName()
    {
        return m_task.Getm_typeName();
    }

    // 7th: Mutators --------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Setm_id --> Sets the log id.
     *
     * SYNOPSIS
     *      public void Setm_id(int a_id)
     *      @param a_id --> int: Log Id.
     *
     * DESCRIPTION
     *      This method sets the log id.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/02/2019
     */
    void Setm_id(int a_id)
    {
        this.m_id = a_id;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_status --> Sets the log status.
     *
     * SYNOPSIS
     *      public void Setm_status(int a_status)
     *      @param a_status --> int: Log status.
     *
     * DESCRIPTION
     *      This method sets the log status.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/02/2019
     */
    void Setm_status(int a_status)
    {
        this.m_status = a_status;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_notes --> Sets the log notes.
     *
     * SYNOPSIS
     *      public void Setm_notes(String a_note)
     *      @param a_note --> String: Log notes.
     *
     * DESCRIPTION
     *      This method sets the log notes.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/02/2019
     */
    void Setm_notes(String a_note)
    {
        this.m_notes = a_note;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_start --> Sets the log start timestamp.
     *
     * SYNOPSIS
     *      public void Setm_start(Timestamp a_start)
     *      @param a_start --> Timestamp: Log start timestamp.
     *
     * DESCRIPTION
     *      This method sets the log start timestamp.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/02/2019
     */
    void Setm_start(Timestamp a_start)
    {
        this.m_start = a_start;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_end --> Sets the log end timestamp.
     *
     * SYNOPSIS
     *      public void Setm_end(Timestamp a_end)
     *      @param a_end --> Timestamp: Log end timestamp.
     *
     * DESCRIPTION
     *      This method sets the log end timestamp.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/02/2019
     */
    void Setm_end(Timestamp a_end)
    {
        this.m_end = a_end;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_task --> Sets the log's task.
     *
     * SYNOPSIS
     *      public void Setm_task(Task a_task)
     *      @param a_task --> Task: Task to set.
     *
     * DESCRIPTION
     *      This method sets the log's task.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/02/2019
     */
    void Setm_task(Task a_task)
    {
        this.m_task = a_task;
    }

    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      GetNiceDate --> Gets nicely formatted date string.
     *
     * SYNOPSIS
     *      private String GetNiceDate(Timestamp a_timestamp)
     *      @param a_timestamp --> Timestamp: A timestamp to parse.
     *
     * DESCRIPTION
     *      This method returns the date and time of a timestamp in a more readable format.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    private String GetNiceDate(Timestamp a_timestamp)
    {
        Date date = new Date();
        date.setTime(a_timestamp.getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEEE, MMM dd yyyy", Locale.getDefault());
        SimpleDateFormat timeFormat = new SimpleDateFormat(
                "hh:mm aa", Locale.getDefault());

        return dateFormat.format(date) + " at " + timeFormat.format(date);
    }

    /**
     * METHOD
     *
     * NAME
     *      GetDateString --> Gets the date part of a timestamp as a string.
     *
     * SYNOPSIS
     *      private String GetDateString (Timestamp a_timeStamp)
     *      @param a_timeStamp --> Timestamp: A timestamp to parse.
     *
     * DESCRIPTION
     *      This method returns the date part of a timestamp as a string.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    private String GetDateString(Timestamp a_timeStamp)
    {
        Date date = new Date();
        date.setTime(a_timeStamp.getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(date);
    }

    /**
     * METHOD
     *
     * NAME
     *      GetTimeString --> Gets the time part of a timestamp as a string.
     *
     * SYNOPSIS
     *      private String GetTimeString (Timestamp a_timeStamp)
     *      @param a_timeStamp --> Timestamp: A timestamp to parse.
     *
     * DESCRIPTION
     *      This method returns the time part of a timestamp as a string.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    private String GetTimeString(Timestamp a_timeStamp)
    {
        Date date = new Date();
        date.setTime(a_timeStamp.getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date);
    }
}
