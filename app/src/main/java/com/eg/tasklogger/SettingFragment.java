// Package and libraries used
package com.eg.tasklogger;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * CLASS
 *
 * NAME
 *      SettingFragment
 *
 * SYNOPSIS
 *      public class SettingFragment extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Setting fragment. The application settings are set/saved in
 *      this section.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      10:00am 2/22/2019
 */
public class SettingFragment extends Fragment
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Spinner m_dropDown;                     // Number of days dropdown
    private ArrayAdapter<String> m_dropDownAdapter; // Adapter used by the days drop down list
    DatabaseHelper m_databaseHelper;                // Database connector
    View m_view;                                    // Fragment view

    // 3rd: GUI Components --------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SettingFragment --> Class constructor.
     *
     * SYNOPSIS
     *      public SettingFragment()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    public SettingFragment()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *          when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_setting, a_container, false);

        // Initialize Class
        Initialize(view);

        return view;
    }

    /**
     * METHOD
     *
     * NAME
     *      onDestroy --> Android Studio generated method, this method executes
     *          when the fragment is destroyed.
     *
     * SYNOPSIS
     *      public void onDestroy()
     *
     * DESCRIPTION
     *      This method calls the SaveSettings method when the fragment is destroyed. This
     *      methodology is used because the user should be able to change the settings without
     *      having to press a save button.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/18/2019
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        SaveSettings();
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      LoadData --> Loads the settings previously saved.
     *
     * SYNOPSIS
     *      private void LoadData(View a_view)
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method loads the settings from dhe DB.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/17/2019
     */
    private void LoadData(View a_view)
    {
        // Build the string array needed by the spinner
        List<String> days;
        days = new ArrayList<>();
        days.add("3");
        days.add("5");
        days.add("7");
        days.add("10");
        days.add("14");
        days.add("21");

        // Set spinner
        m_dropDown = a_view.findViewById(R.id.spinnerSettingCdays);

        if (getActivity() != null)
            m_dropDownAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, days);
        m_dropDown.setAdapter(m_dropDownAdapter);

        // Get the settings from the DB
        // Update checkbox and spinner to mimic the settings saved
        m_databaseHelper = new DatabaseHelper(getContext());
        JSONObject currentSettings = m_databaseHelper.GetSettings();
        try
        {
            String delPrompt = currentSettings.getString("promptDelete");
            String comDays = currentSettings.getString("completedDays");

            CheckBox pDelete = a_view.findViewById(R.id.setting_promptCheckBox);
            if (Integer.parseInt(delPrompt) == 1) pDelete.setChecked(true);
            else pDelete.setChecked(false);

            m_dropDown.setSelection(m_dropDownAdapter.getPosition(comDays));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      SaveSettings --> Saves the updated settings.
     *
     * SYNOPSIS
     *      private void SaveSettings()
     *
     * DESCRIPTION
     *      This method saves the settings updated by the user.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/17/2019
     */
    private void SaveSettings()
    {
        CheckBox pDelete = m_view.findViewById(R.id.setting_promptCheckBox);
        int promptDelete = (pDelete.isChecked())? 1 : 0;
        String daysSelected = m_dropDown.getSelectedItem().toString();
        boolean updateResult = m_databaseHelper.UpdateSettings(promptDelete,
                Integer.parseInt(daysSelected));

        // Notify user only there was an error
        if (!updateResult) Toast.makeText(getContext(),
                "There was an error, please try again.", Toast.LENGTH_SHORT).show();
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        // Save fragment view for later reference
        m_view = a_view;

        // Update the top bar
        if (getActivity() != null)
            ((MainActivity) getActivity()).SetActionBarTitle(R.string.setting_title);

        // Load saved settings
        LoadData(a_view);
    }
}
