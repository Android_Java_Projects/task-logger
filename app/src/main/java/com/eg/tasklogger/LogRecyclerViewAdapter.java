// Package and libraries used
package com.eg.tasklogger;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

import static com.eg.tasklogger.TLApplication.RUNNINGLOG;

/**
 * CLASS
 *
 * NAME
 *      LogRecyclerViewAdapter
 *
 * SYNOPSIS
 *      public class LogRecyclerViewAdapter
 *          extends RecyclerView.Adapter<LogRecyclerViewAdapter.LogViewHolder>
 *
 * DESCRIPTION
 *      This class is an adapter that links the recycler view to an Log list. It is mainly
 *      Used in the Home, Completed, and the Report Details fragments.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/12/2019
 */
public class LogRecyclerViewAdapter
        extends RecyclerView.Adapter<LogRecyclerViewAdapter.LogViewHolder>
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Context m_context;     // Context to be used by recycler view
    private List<Log> m_log;       // Array/List of Log type
    private int m_lastPosition;    // Allows to save the last item shown on screen
    private int m_animation;       // Type of animation to use 0 is fade 1 is slide
    private int m_logStatus;       // Log status to display

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * CLASS
     *
     * NAME
     *      LogViewHolder --> Helper class which holds log data.
     *
     * SYNOPSIS
     *      public static class LogViewHolder extends RecyclerView.ViewHolder
     *
     * DESCRIPTION
     *      This class holds log data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/4/2019
     */
    static class LogViewHolder extends RecyclerView.ViewHolder
    {
        // Private member variables
        private TextView mt_typeColor;
        private TextView mt_typeName;
        private TextView mt_name;
        private TextView mt_start;
        private TextView mt_end;
        private TextView mt_time;
        private TextView mt_notes;
        private LinearLayout mt_parentLayout;

        /**
         * METHOD
         *
         * NAME
         *      LogViewHolder --> Class constructor.
         *
         * SYNOPSIS
         *      public LogViewHolder(@NonNull View a_itemView)
         *      @param a_itemView --> View: View holding log data
         *
         * DESCRIPTION
         *      This method is the class constructor for the inner class.
         *
         * RETURNS
         *      Not applicable
         *
         * AUTHOR
         *      Edgar Garcia
         *
         * DATE
         *      8:00pm 3/24/2019
         */
        LogViewHolder(@NonNull View a_itemView)
        {
            super(a_itemView);

            // Data to display
            mt_typeColor = a_itemView.findViewById(R.id.completedLogItem_typeColor);
            mt_typeName = a_itemView.findViewById(R.id.completedLogItem_typeName);
            mt_name = a_itemView.findViewById(R.id.completedLogItem_name);
            mt_start = a_itemView.findViewById(R.id.completedLogItem_start);
            mt_end = a_itemView.findViewById(R.id.completedLogItem_end);
            mt_time = a_itemView.findViewById(R.id.completedLogItem_time);
            mt_notes = a_itemView.findViewById(R.id.completedLogItem_notes);
            mt_parentLayout = a_itemView.findViewById(R.id.layoutCompletedLogItem);
        }
    }

    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      LogRecyclerViewAdapter --> Class constructor.
     *
     * SYNOPSIS
     *      public LogRecyclerViewAdapter(Context a_context, List<Log> a_log)
     *      @param a_context --> Context: Application Context.
     *      @param a_log --> List<Log>: List/Array of Log objects.
     *      @param a_animation --> int: Type of animation to use.
     *      @param a_logStatus --> int: Type of log being displayed.
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/6/2019
     */
    LogRecyclerViewAdapter(Context a_context, List<Log> a_log, int a_animation, int a_logStatus)
    {
        this.m_context = a_context;
        this.m_log = a_log;
        this.m_animation = a_animation;
        this.m_logStatus = a_logStatus;
        this.m_lastPosition = -1;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateViewHolder --> Android Studio generated method, this method executes
     *          when the view holder is created.
     *
     * SYNOPSIS
     *      public LogRecyclerViewAdapter.LogViewHolder onCreateViewHolder
     *          (ViewGroup a_viewGroup, int a_viewType)
     *      @param a_viewGroup --> ViewGroup: Group of views.
     *      @param a_viewType --> int: Type of view.
     *
     * DESCRIPTION
     *      This method executes when the view holder is created.
     *
     * RETURNS
     *      @return TypeViewHolder: View Holder for Tasks.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    @NonNull
    @Override
    public LogRecyclerViewAdapter.LogViewHolder onCreateViewHolder(
            @NonNull ViewGroup a_viewGroup, int a_viewType)
    {
        View view;
        view = LayoutInflater.from(m_context).inflate(R.layout.fragitem_log
                ,a_viewGroup,false);

        return new LogRecyclerViewAdapter.LogViewHolder(view);
    }

    /**
     * METHOD
     *
     * NAME
     *      onBindViewHolder --> Android Studio generated method, this method executes
     *          when the view is bound.
     *
     * SYNOPSIS
     *      public void onBindViewHolder(final LogRecyclerViewAdapter.LogViewHolder a_holder,
     *          final int a_position)
     *      @param a_holder --> TypeViewHolder: View Holder for Tasks.
     *      @param a_position --> int: List index.
     *
     * DESCRIPTION
     *      This method executes  when the view is bound.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/4/2019
     */
    @Override
    public void onBindViewHolder(@NonNull final LogRecyclerViewAdapter.LogViewHolder a_holder,
                                  final int a_position)
    {
        // Type data
        GradientDrawable drawable = (GradientDrawable) a_holder.mt_typeColor.getBackground();
        drawable.setColor(m_log.get(a_position).Getm_typeColor());
        a_holder.mt_typeName.setText(m_log.get(a_position).Getm_typeName());

        // Task data
        a_holder.mt_name.setText(m_log.get(a_position).Getm_taskName());

        // Log data
        if (m_logStatus == RUNNINGLOG)
        {
            // Running log: Display start time only
            String start = "Started on" + m_log.get(a_position).Getm_startString();
            a_holder.mt_start.setText(start);
            a_holder.mt_end.setVisibility(View.GONE);
        }
        else
        {
            // Completed log: Display start and end time
            String from = "From " + m_log.get(a_position).Getm_startString();
            String to = "To " + m_log.get(a_position).Getm_endString();
            a_holder.mt_start.setText(from);
            a_holder.mt_end.setText(to);
        }

        Double timeUHR = m_log.get(a_position).Getm_elapsedTimeUHR();
        String stringUHR =  ((TLApplication) m_context.getApplicationContext()).GetReadableTime(timeUHR);
        a_holder.mt_time.setText(stringUHR);
        a_holder.mt_notes.setText(m_log.get(a_position).Getm_notes());

        // Animation when the view is bound
        SetAnimation(a_holder.itemView, a_position);

        a_holder.mt_parentLayout.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the user presses on a Log item on the list.
             *      The proper activity is called with a message that includes the action type,
             *      Task record ID, and the position in the array/recycler of the item clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00pm 3/25/2019
             */
            @Override
            public void onClick(View a_view)
            {
                Intent myIntent;

                // Call the activity according to log type
                if (m_logStatus == RUNNINGLOG) myIntent = new Intent(m_context, StopTaskActivity.class);
                else myIntent = new Intent(m_context, CompletedAEActivity.class);
                String message = "edit|" +  Integer.toString(m_log.get(a_position).Getm_id())
                        + "|" + Integer.toString(a_position);
                myIntent.putExtra("message",message);
                m_context.startActivity(myIntent);
            }
        });
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      getItemCount --> Android Studio generated method, this method returns the number
     *          of logs in the recycler.
     *
     * SYNOPSIS
     *      public int getItemCount()
     *
     * DESCRIPTION
     *      This method returns the number of logs in the recycler.
     *
     * RETURNS
     *      @return int --> Number of logs in the recycler.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/4/2019
     */
    @Override
    public int getItemCount()
    {
        return m_log.size();
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SetAnimation --> Sets an animation when a new bound view is added to the recycler.
     *
     * SYNOPSIS
     *      private void SetAnimation(View a_viewToAnimate, int a_position)
     *      @param a_viewToAnimate --> View: View to animate when bound.
     *      @param a_position --> int: Index number.
     *
     * DESCRIPTION
     *      This method sets a animation that executes when a new bound view is added
     *      to the recycler.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/4/2019
     */
    private void SetAnimation(View a_viewToAnimate, int a_position)
    {
        // If the bound view wasn't previously displayed on screen, animate it
        if (a_position > m_lastPosition)
        {
            int animationType = (m_animation == 0)?
                    android.R.anim.fade_in : android.R.anim.slide_in_left;

            Animation animation = AnimationUtils.loadAnimation(m_context, animationType);
            a_viewToAnimate.startAnimation(animation);
            m_lastPosition = a_position;
        }
    }
}
