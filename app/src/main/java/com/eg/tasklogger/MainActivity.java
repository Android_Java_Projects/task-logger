// Package and libraries used
package com.eg.tasklogger;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.*;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import java.util.List;

/**
 * CLASS
 *
 * NAME
 *      MainActivity
 *
 * SYNOPSIS
 *      public class MainActivity extends AppCompatActivity
 *         implements NavigationView.OnNavigationItemSelectedListener
 *
 * DESCRIPTION
 *      This class handles the main menu activity layout. It handles the switching between
 *      the different fragments used by the application.  It also controls de application's
 *      main menu.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      3:52pm 2/21/2019
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    enum TLfragments {HOME, COMPLETED, TASK, TYPE, REPORT, SETTING, HELP}

    // 2nd: Private member variables ----------------------------------------------------------
    int m_fragmentToDisplay;            // Fragment to display
    int m_currentFragment;              // Fragment being displayed
    DatabaseHelper m_databaseHelper;    // Database connector

    // 3rd: UI Components --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SetActionBarTitle --> Sets the Action(Top) Bar title.
     *
     * SYNOPSIS
     *      public void SetActionBarTitle(int a_titleResource)
     *      @param a_titleResource --> int: Title resource unique ID.
     *
     * DESCRIPTION
     *      This method sets the Action(Top) Bar title. It is called by fragments
     *      when they are initialized.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/24/2019
     */
    public void SetActionBarTitle(int a_titleResource)
    {
        setTitle(a_titleResource);
    }

    // 4th: Constructor -----------------------------------------------------------------------
    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the layout/activity opens.
     *
     * SYNOPSIS
     *      protected void onCreate(Bundle a_savedInstanceState)
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of your Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the layout/class.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    @Override
    protected void onCreate(Bundle a_savedInstanceState)
    {
        super.onCreate(a_savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Class
        Initialize();
    }

    /**
     * METHOD
     *
     * NAME
     *      onBackPressed --> Android Studio generated method, this method is called when the mobile
     *          device's back button is pressed.
     *
     * SYNOPSIS
     *      public void onBackPressed()
     *
     * DESCRIPTION
     *      This method handles the mobile device's back button.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    @Override
    public void onBackPressed()
    {
        // Hide the navigation menu if shown
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            // Default behaviour
            super.onBackPressed();
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      onNavigationItemSelected --> Android Studio generated method, listener for
     *          the Navigation menu.
     *
     * SYNOPSIS
     *      public boolean onNavigationItemSelected(@NonNull MenuItem a_item)
     *      @param a_item --> MenuItem: Menu item object pressed.
     *
     * DESCRIPTION
     *      This method handles the navigation menu item clicks. It also sets the m_fragmentToDisplay member
     *      variable according to the item clicked.
     *
     * RETURNS
     *      @return boolean --> True if successfully processed.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem a_item)
    {
        // Handle navigation menu item clicks.
        int id = a_item.getItemId();

        switch (id)
        {
            case R.id.nav_main:
                m_fragmentToDisplay = TLfragments.HOME.ordinal();
                break;
            case R.id.nav_completed:
                m_fragmentToDisplay = TLfragments.COMPLETED.ordinal();
                break;
            case R.id.nav_task:
                m_fragmentToDisplay = TLfragments.TASK.ordinal();
                break;
            case R.id.nav_type:
                m_fragmentToDisplay = TLfragments.TYPE.ordinal();
                break;
            case R.id.nav_report:
                m_fragmentToDisplay = TLfragments.REPORT.ordinal();
                break;
            case R.id.nav_setting:
                m_fragmentToDisplay = TLfragments.SETTING.ordinal();
                break;
            case R.id.nav_help:
                m_fragmentToDisplay = TLfragments.HELP.ordinal();
                break;
            case R.id.nav_rate:
                // TODO: Create call to Android Play Store
                break;
        }

        // Close the navigation menu
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SeedDatabase --> Seeds the database.
     *
     * SYNOPSIS
     *      private void SeedDatabase()
     *
     * DESCRIPTION
     *      This method seeds the database. First, it checks if there any type records, if not
     *      the method creates default type, task, and setting records.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      9:00pm 4/16/2019
     */
    private void SeedDatabase()
    {
        m_databaseHelper = new DatabaseHelper(this);
        List<Type> lstType;
        lstType = m_databaseHelper.GetAllTypes();

        // Check if the database is empty
        // Type is the most basic element, if there aren't any, it means the
        // database is empty
        if (lstType.size() == 0)
        {
            // Create 3 types and save them into the db
            Type jobType = new Type(1,"Job",0,
                        -948174,"f1af42");
            Type gymType = new Type(2,"Gym",0,
                        -9506335,"6ef1e1");
            Type driveType = new Type(3,"Drive",0,
                     -6688326,"99f1ba");
            boolean setType1 = m_databaseHelper.AddType(jobType);
            boolean setType2 = m_databaseHelper.AddType(gymType);
            boolean setType3 = m_databaseHelper.AddType(driveType);

            // Create 3 tasks and save them into the db
            Task jobTask = new Task(1,"My everyday job",
                    "Full time job from 9 to 5.",jobType);
            Task gymTask = new Task(2,"Gym session",
                    "My 1 hour sweat session.",gymType);
            Task driveTask = new Task(3,"Driving to the supermarket",
                    "The time it takes me to get to the different supermarkets.",
                        driveType);
            boolean setTask1 = m_databaseHelper.AddTask(jobTask);
            boolean setTask2 = m_databaseHelper.AddTask(gymTask);
            boolean setTask3 = m_databaseHelper.AddTask(driveTask);

            // Setting values
            boolean setSettings = m_databaseHelper.AddSettings();

            // Check if an error was returned
            if (!(setType1 && setType2 && setType3
                    && setTask1 && setTask2 && setTask3
                    && setSettings))
            {
                String message = "There was a problem initializing the Database. Please try " +
                        "again later";
                Toast.makeText(this, message,
                        Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      LoadFragment --> Loads the fragment corresponding to the Navigation menu chosen.
     *
     * SYNOPSIS
     *      private void LoadFragment()
     *
     * DESCRIPTION
     *      This method loads the fragment corresponding to the Navigation menu chosen. It
     *      swaps the fragment being displayed with another fragment.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void LoadFragment()
    {
        // Load fragment only if not previously loaded
        if (m_currentFragment == m_fragmentToDisplay) return;
        m_currentFragment = m_fragmentToDisplay;

        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (m_currentFragment)
        {
            case 0:
                HomeFragment home = new HomeFragment();
                fragmentManager.beginTransaction().replace(R.id.mainFragment, home).commit();
                break;
            case 1:
                CompletedFragment completed = new CompletedFragment();
                fragmentManager.beginTransaction().replace(R.id.mainFragment, completed).commit();
                break;
            case 2:
                TaskFragment task = new TaskFragment();
                fragmentManager.beginTransaction().replace(R.id.mainFragment, task).commit();
                break;
            case 3:
                TypeFragment type = new TypeFragment();
                fragmentManager.beginTransaction().replace(R.id.mainFragment, type).commit();
                break;
            case 4:
                ReportFragment report = new ReportFragment();
                fragmentManager.beginTransaction().replace(R.id.mainFragment, report).commit();
                break;
            case 5:
                SettingFragment setting = new SettingFragment();
                fragmentManager.beginTransaction().replace(R.id.mainFragment, setting).commit();
                break;
            case 6:
                HelpFragment help = new HelpFragment();
                fragmentManager.beginTransaction().replace(R.id.mainFragment, help).commit();
                break;
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      InitializeNavigationMenu --> Initializes the Home Navigation menu.
     *
     * SYNOPSIS
     *      private void InitializeNavigationMenu(Toolbar a_toolbar)
     *      @param a_toolbar --> Layout's top bar.
     *
     * DESCRIPTION
     *      This method initializes the Main Navigation menu.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/22/2019
     */
    private void InitializeNavigationMenu(Toolbar a_toolbar)
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, a_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        {
            /**
             * METHOD
             *
             * NAME
             *      onDrawerClosed --> Android Studio generated method. Runs when the menu drawer
             *          closes.
             *
             * SYNOPSIS
             *      public void onDrawerClosed(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs when the drawer close animation ends.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            public void onDrawerClosed(View a_view)
            {
                // Event after the menu close animation
                super.onDrawerClosed(a_view);

                // Load a Fragment only from options 0 to 6
                if (m_fragmentToDisplay >=0 && m_fragmentToDisplay <=6) LoadFragment();
            }

        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Sets the highlight on the Home menu option
        int navMenuIndex = TLfragments.HOME.ordinal();
        navigationView.getMenu().getItem(navMenuIndex).setChecked(true);

        // Loads the Home fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        HomeFragment home = new HomeFragment();
        fragmentManager.beginTransaction().replace(R.id.mainFragment, home).commit();
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current layout.
     *
     * SYNOPSIS
     *      private void Initialize()
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/22/2019
     */
    private void Initialize()
    {
        // Sets the top tool bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Sets the side navigation menu
        InitializeNavigationMenu(toolbar);

        // Seeds the database
        SeedDatabase();
    }
}
