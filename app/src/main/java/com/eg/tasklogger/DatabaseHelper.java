// Package and libraries used
package com.eg.tasklogger;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import org.json.JSONObject;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * CLASS
 *
 * NAME
 *      DatabaseHelper
 *
 * SYNOPSIS
 *      public class DatabaseHelper extends SQLiteOpenHelper
 *
 * DESCRIPTION
 *      This class handles the access to the Database.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00am 3/23/2019
 */
public class DatabaseHelper extends SQLiteOpenHelper
{
    /*
    Order in which this class is set
    1st: Database version and name
    2nd: Table names
    3rd: Column names
    4th: Table create statements
    5th: Constructor
    6th: Event handlers
    7th: Type table's Create, Read, Update, Delete (CRUD) methods
    8th: Task table's CRUD methods
    9th: Log table's CRUD methods
    10th: Setting table's RU methods
    11th: Report methods
    12th: Shared methods
    nth: Any utility (private) methods
    */

    // 1st: Database version and name ----------------------------------------------------------
    private static final int DATABASE_VERSION = 7; // Change version to update schema.
    private static final String DATABASE_NAME = "TaskLogger";

    // 2nd: Table names ------------------------------------------------------------------------
    private static final String TABLE_TYPE = "Type";
    private static final String TABLE_TASK = "Task";
    private static final String TABLE_LOG = "Log";
    private static final String TABLE_SETTING = "Setting";

    // 3rd: Column names -----------------------------------------------------------------------
    private static final String COL_ID = "ID";
    private static final String COL_TYPEID = "TypeID";
    private static final String COL_TASKID = "TaskID";
    private static final String COL_STATUS = "Status";
    private static final String COL_NAME = "Name";
    private static final String COL_DESCRIPTION = "Description";
    private static final String COL_NOTES = "Notes";
    private static final String COL_COLOR = "Color";
    private static final String COL_COLORHEX = "ColorHex";
    private static final String COL_CREATED_AT = "Created_at";
    private static final String COL_STARTED_AT = "Started_at";
    private static final String COL_ENDED_AT = "Ended_at";
    private static final String COL_ELAPSEDUHR = "Elapsed_time";
    private static final String COL_PROMPTDELETE = "Prompt_delete";
    private static final String COL_COMPLETEDDAYS = "Completed_days";

    // 4th: Table create statements ------------------------------------------------------------
    private static final String CREATE_TABLE_TYPE = "CREATE TABLE "
            + TABLE_TYPE
            + "("
            + COL_ID         + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_NAME       + " TEXT,"
            + COL_COLOR      + " INTEGER,"
            + COL_COLORHEX   + " TEXT,"
            + COL_CREATED_AT + " DATETIME"
            + ")";

    private static final String CREATE_TABLE_TASK = "CREATE TABLE "
            + TABLE_TASK
            + "("
            + COL_ID          + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_NAME        + " TEXT,"
            + COL_DESCRIPTION + " INTEGER,"
            + COL_TYPEID      + " INTEGER,"
            + COL_CREATED_AT  + " DATETIME"
            + ")";

    private static final String CREATE_TABLE_LOG = "CREATE TABLE "
            + TABLE_LOG
            + "("
            + COL_ID          + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_STATUS      + " INTEGER,"
            + COL_NOTES       + " TEXT,"
            + COL_TASKID      + " INTEGER,"
            + COL_STARTED_AT  + " DATETIME,"
            + COL_ENDED_AT    + " DATETIME,"
            + COL_ELAPSEDUHR  + " REAL,"
            + COL_CREATED_AT  + " DATETIME"
            + ")";

    private static final String CREATE_TABLE_SETTING = "CREATE TABLE "
            + TABLE_SETTING
            + "("
            + COL_ID            + " INTEGER,"
            + COL_PROMPTDELETE  + " INTEGER,"
            + COL_COMPLETEDDAYS + " INTEGER"
            + ")";
    // 5th: Constructor ------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      DatabaseHelper --> Class constructor.
     *
     * SYNOPSIS
     *      DatabaseHelper(Context a_context)
     *      @param a_context --> Context: Application's current context.
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    DatabaseHelper(Context a_context)
    {
        super(a_context, DATABASE_NAME ,null, DATABASE_VERSION);
    }

    // 6th: Event handlers ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the class instantiates.
     *
     * SYNOPSIS
     *      public void onCreate(SQLiteDatabase a_sqLiteDatabase)
     *      @param a_sqLiteDatabase --> SQLiteDatabase: Database to use.
     *
     * DESCRIPTION
     *      This method creates/loads the database to use.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    @Override
    public void onCreate(SQLiteDatabase a_sqLiteDatabase)
    {
        // Create tables
        a_sqLiteDatabase.execSQL(CREATE_TABLE_TYPE);
        a_sqLiteDatabase.execSQL(CREATE_TABLE_TASK);
        a_sqLiteDatabase.execSQL(CREATE_TABLE_LOG);
        a_sqLiteDatabase.execSQL(CREATE_TABLE_SETTING);
    }

    /**
     * METHOD
     *
     * NAME
     *      onUpgrade --> Android Studio generated method, this method runs when the DB version
     *          changes.
     *
     * SYNOPSIS
     *      public void onUpgrade(SQLiteDatabase a_sqLiteDatabase, int a_oldVersion
     *          , int a_newVersion)
     *      @param a_sqLiteDatabase --> SQLiteDatabase: Database to use.
     *      @param a_oldVersion --> SQLiteDatabase: Database's previous version number.
     *      @param a_newVersion --> SQLiteDatabase: Database's new version number.
     *
     * DESCRIPTION
     *      This method runs when the DB version changes. It drops(deletes) all tables and then
     *      calls the OnCreate method to re-create them.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    @Override
    public void onUpgrade(SQLiteDatabase a_sqLiteDatabase, int a_oldVersion, int a_newVersion)
    {
        // On upgrade drop older tables
        a_sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TYPE);
        a_sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);
        a_sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG);
        a_sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTING);

        // Create new tables
        onCreate(a_sqLiteDatabase);
    }

    // 7th: Type table's Create, Read, Update, Delete (CRUD) methods ---------------------------
    /**
     * METHOD
     *
     * NAME
     *      AddType --> Adds a new Type.
     *
     * SYNOPSIS
     *      public boolean AddType(Type a_type)
     *      @param a_type --> Type: Type to add.
     *
     * DESCRIPTION
     *      This method adds a new Type record.
     *
     * RETURNS
     *      @return boolean --> True if successfully added.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    boolean AddType(Type a_type)
    {
        SQLiteDatabase db =  this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_NAME, a_type.Getm_name());
        values.put(COL_COLOR, a_type.Getm_color());
        values.put(COL_COLORHEX, a_type.Getm_colorHex());
        values.put(COL_CREATED_AT, GetDateTime());

        // Insert data
        long result = db.insert(TABLE_TYPE, null, values);

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetType --> Gets a Type record.
     *
     * SYNOPSIS
     *      public Type GetType(int a_typeID)
     *      @param a_typeID --> int: Type ID to fetch.
     *
     * DESCRIPTION
     *      This method fetches a Type record.
     *
     * RETURNS
     *      @return Type --> Type object with record data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    Type GetType(int a_typeID)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_TYPE + " WHERE "
                + COL_ID + " = " + a_typeID;

        Type taskType = new Type();

        try (Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();
            taskType.Setm_id(c.getInt(c.getColumnIndex(COL_ID)));
            taskType.Setm_name(c.getString(c.getColumnIndex(COL_NAME)));
            taskType.Setm_color(c.getInt(c.getColumnIndex(COL_COLOR)));
            taskType.Setm_colorHex(c.getString(c.getColumnIndex(COL_COLORHEX)));

            // Get/Set the number of times being used
            int usedTimes = GetTypeUsageCount(c.getInt(c.getColumnIndex(COL_ID)));
            taskType.Setm_usedTimes(usedTimes);
        }
        catch (Exception e)
        {
            // Exception handling
            return taskType;
        }

        return taskType;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetAllTypes --> Gets all the Type records.
     *
     * SYNOPSIS
     *      public List<Type> GetAllTypes()
     *
     * DESCRIPTION
     *      This method fetches all the Type records sorted by type name.
     *
     * RETURNS
     *      @return List<Type> --> List of Type objects with record data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    List<Type> GetAllTypes()
    {
        List<Type> allTypes = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_TYPE
                + " ORDER BY " + COL_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        try(Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            // Loop through all rows and add to list
            c.moveToFirst();

            do {
                Type t = new Type();

                t.Setm_id(c.getInt(c.getColumnIndex(COL_ID)));
                t.Setm_name(c.getString(c.getColumnIndex(COL_NAME)));
                t.Setm_color(c.getInt(c.getColumnIndex(COL_COLOR)));
                t.Setm_colorHex(c.getString(c.getColumnIndex(COL_COLORHEX)));

                // Get/Set the number of times being used
                int usedTimes = GetTypeUsageCount(c.getInt(c.getColumnIndex(COL_ID)));
                t.Setm_usedTimes(usedTimes);

                // Add to allTypes list
                allTypes.add(t);
            } while (c.moveToNext());
        }
        catch (Exception e)
        {
            // Exception handling
            return allTypes;
        }

        return allTypes;
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateType --> Updates a Type.
     *
     * SYNOPSIS
     *      public boolean UpdateType(Type a_type)
     *      @param a_type --> Type: Type to update.
     *
     * DESCRIPTION
     *      This method updates a Type record.
     *
     * RETURNS
     *      @return boolean --> True if successfully updated.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    boolean UpdateType(Type a_type)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_NAME, a_type.Getm_name());
        values.put(COL_COLOR, a_type.Getm_color());
        values.put(COL_COLORHEX, a_type.Getm_colorHex());

        // Update
        long result = db.update(TABLE_TYPE, values, COL_ID + " = ?",
                new String[] { String.valueOf(a_type.Getm_id()) });

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetTypeUsageCount --> Counts how many times a type is being used by tasks.
     *
     * SYNOPSIS
     *      public int GetTypeUsageCount(int a_typeID)
     *      @param a_typeID --> int: Type ID number.
     *
     * DESCRIPTION
     *      This method calculates how many times a type is being used by tasks.
     *
     * RETURNS
     *      @return int --> Number of times being used.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    int GetTypeUsageCount(int a_typeID)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT COUNT(*) FROM " + TABLE_TASK + " WHERE "
                + COL_TYPEID + " = " + a_typeID;

        int count = 0;
        try (Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();
            count =  c.getInt(0);
        }
        catch (Exception e)
        {
            // Exception handling
            return count;
        }

        return count;
    }

    // 8th: Task table's CRUD methods ----------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      AddTask --> Adds a new Task.
     *
     * SYNOPSIS
     *      public boolean AddTask(Task a_task)
     *      @param a_task--> Task: Task to add.
     *
     * DESCRIPTION
     *      This method adds a new Task record.
     *
     * RETURNS
     *      @return boolean --> True if successfully added.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    boolean AddTask(Task a_task)
    {
        SQLiteDatabase db =  this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_NAME, a_task.Getm_name());
        values.put(COL_DESCRIPTION, a_task.Getm_description());
        values.put(COL_TYPEID, a_task.Getm_typeId());
        values.put(COL_CREATED_AT, GetDateTime());

        // Insert data
        long result = db.insert(TABLE_TASK, null, values);

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetTask --> Gets a Task record.
     *
     * SYNOPSIS
     *      public Type GetTask(int a_taskID)
     *      @param a_taskID --> int: Task ID to fetch.
     *
     * DESCRIPTION
     *      This method fetches a Task record.
     *
     * RETURNS
     *      @return Task --> Task object with record data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    Task GetTask(int a_taskID)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * " + " FROM " + TABLE_TASK + " WHERE "
                + COL_ID + " = " + a_taskID;

        Task task = new Task();

        try (Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();

            task.Setm_id(c.getInt(c.getColumnIndex(COL_ID)));
            task.Setm_name(c.getString(c.getColumnIndex(COL_NAME)));
            task.Setm_description(c.getString(c.getColumnIndex(COL_DESCRIPTION)));

            // Get/Set the task type
            Type typeRelated = GetType(c.getInt(c.getColumnIndex(COL_TYPEID)));
            task.Setm_type(typeRelated);
        }
        catch (Exception e)
        {
            // Exception handling
            return task;
        }

        return task;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetAllTasks --> Gets all the Tasks records.
     *
     * SYNOPSIS
     *      public List<Task> GetAllTasks()
     *
     * DESCRIPTION
     *      This method fetches all the Tasks records sorted by task name.
     *
     * RETURNS
     *      @return List<Task> --> List of Task objects with record data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    List<Task> GetAllTasks()
    {
        List<Task> allTasks = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_TASK
                + " ORDER BY " + COL_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        try (Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();
            do {
                Task t = new Task();
                t.Setm_id(c.getInt(c.getColumnIndex(COL_ID)));
                t.Setm_name(c.getString(c.getColumnIndex(COL_NAME)));
                t.Setm_description(c.getString(c.getColumnIndex(COL_DESCRIPTION)));

                // Get/Set the task type
                Type typeRelated = GetType(c.getInt(c.getColumnIndex(COL_TYPEID)));
                t.Setm_type(typeRelated);

                // Add to allTypes list
                allTasks.add(t);
            } while (c.moveToNext());
        }
        catch (Exception e)
        {
            // Exception handling
            return allTasks;
        }

        return allTasks;
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateTask --> Updates a Task.
     *
     * SYNOPSIS
     *      public boolean UpdateTask(Task a_task)
     *      @param a_task --> Type: Task to update.
     *
     * DESCRIPTION
     *      This method updates a Task record.
     *
     * RETURNS
     *      @return boolean --> True if successfully updated.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    boolean UpdateTask(Task a_task)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_NAME, a_task.Getm_name());
        values.put(COL_DESCRIPTION, a_task.Getm_description());
        values.put(COL_TYPEID, a_task.Getm_typeId());

        // Update
        long result = db.update(TABLE_TASK, values, COL_ID + " = ?",
                new String[] { String.valueOf(a_task.Getm_id()) });

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetTaskUsageCount --> Counts how many times a task definition is being used by logs.
     *
     * SYNOPSIS
     *      public int GetTaskUsageCount(int a_taskID)
     *      @param a_taskID --> int: Task ID number.
     *
     * DESCRIPTION
     *      This method calculates how many times a task definition is being used by logs.
     *
     * RETURNS
     *      @return int --> Number of times being used.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    int GetTaskUsageCount(int a_taskID)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT COUNT(*) FROM " + TABLE_LOG
                + " WHERE " + COL_TASKID + " = " + a_taskID;

        int count = 0;
        try (Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();
            count =  c.getInt(0);
        }
        catch (Exception e)
        {
            // Exception handling
            return count;
        }

        return count;
    }

    // 9th: Log table's CRUD methods
    /**
     * METHOD
     *
     * NAME
     *      AddLog --> Adds a new Log.
     *
     * SYNOPSIS
     *      public boolean AddLog(Log a_log)
     *      @param a_log--> Log: Log to add.
     *
     * DESCRIPTION
     *      This method adds a new Log record.
     *
     * RETURNS
     *      @return boolean --> True if successfully added.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/2/2019
     */
    boolean AddLog(Log a_log)
    {
        SQLiteDatabase db =  this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_TASKID, a_log.Getm_taskId());
        values.put(COL_STATUS, a_log.Getm_status());
        values.put(COL_NOTES, a_log.Getm_notes());
        values.put(COL_STARTED_AT, GetDateTime(a_log.Getm_start()));
        values.put(COL_ENDED_AT, GetDateTime(a_log.Getm_end()));
        values.put(COL_ELAPSEDUHR, a_log.Getm_elapsedTimeUHR());
        values.put(COL_CREATED_AT, GetDateTime());

        // Insert data
        long result = db.insert(TABLE_LOG, null, values);

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetLog --> Gets a Log record.
     *
     * SYNOPSIS
     *      public Task GetLog(int a_logID)
     *      @param a_logID --> int: Log ID to fetch.
     *
     * DESCRIPTION
     *      This method fetches a Log record.
     *
     * RETURNS
     *      @return Log --> Log object with record data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/2/2019
     */
    Log GetLog(int a_logID)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * " + " FROM " + TABLE_LOG
                + " WHERE " + COL_ID + " = " + a_logID;
        Log log = new Log();

        try(Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();
            log.Setm_id(c.getInt(c.getColumnIndex(COL_ID)));
            log.Setm_status(c.getInt(c.getColumnIndex(COL_STATUS)));
            log.Setm_notes(c.getString(c.getColumnIndex(COL_NOTES)));

            String startS = c.getString(c.getColumnIndex(COL_STARTED_AT));
            String endS = c.getString(c.getColumnIndex(COL_ENDED_AT));
            log.Setm_start(GetTimestamp(startS));
            log.Setm_end(GetTimestamp(endS));

            // Get/Set the task
            Task taskRelated = GetTask(c.getInt(c.getColumnIndex(COL_TASKID)));
            log.Setm_task(taskRelated);
        }
        catch (Exception e)
        {
            // Exception handling
            return log;
        }

        return log;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetMultipleLogs --> Get Multiple Log records.
     *
     * SYNOPSIS
     *      public List<Log> GetMultipleLogs(int a_status, String a_fromDate,
     *                                      String a_toDate, String a_sortOrder)
     *      @param a_status --> int: Log status (0 = Running, 1 = Complete)
     *      @param a_fromDate --> String: Range start date
     *      @param a_toDate --> String: Range end date
     *      @param a_sortOrder --> String: Start time sort order
     *
     * DESCRIPTION
     *      This method fetches multiple log records sorted by descending start date/time.
     *
     * RETURNS
     *      @return List<Log> --> List of Log objects with record data.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/2/2019
     */
    List<Log> GetMultipleLogs(int a_status, String a_fromDate,
                                     String a_toDate, String a_sortOrder)
    {
        List<Log> multipleLogs = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOG
                + " WHERE " + COL_STATUS + " = " + a_status
                + " AND " + COL_STARTED_AT + " >= '" + a_fromDate + "'"
                + " AND " + COL_ENDED_AT + " <= '" + a_toDate + "'"
                + " ORDER BY " + COL_STARTED_AT
                + " " + a_sortOrder;

        SQLiteDatabase db = this.getReadableDatabase();

        try(Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            // Loop through all rows and add to list
            c.moveToFirst();
            do {
                Log l = new Log();
                l.Setm_id(c.getInt(c.getColumnIndex(COL_ID)));
                l.Setm_status(c.getInt(c.getColumnIndex(COL_STATUS)));
                l.Setm_notes(c.getString(c.getColumnIndex(COL_NOTES)));

                String startS = c.getString(c.getColumnIndex(COL_STARTED_AT));
                String endS = c.getString(c.getColumnIndex(COL_ENDED_AT));
                l.Setm_start(GetTimestamp(startS));
                l.Setm_end(GetTimestamp(endS));

                // Get/Set the task
                Task taskRelated = GetTask(c.getInt(c.getColumnIndex(COL_TASKID)));
                l.Setm_task(taskRelated);

                // Add to multipleLogs list
                multipleLogs.add(l);
            } while (c.moveToNext());
        }
        catch (Exception e)
        {
            // Exception handling
            return multipleLogs;
        }

        return multipleLogs;
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateLog --> Updates a Log.
     *
     * SYNOPSIS
     *      public boolean UpdateLog(Log a_log)
     *      @param a_log --> Log: Log to update.
     *
     * DESCRIPTION
     *      This method updates a Log record.
     *
     * RETURNS
     *      @return boolean --> True if successfully updated.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/2/2019
     */
    boolean UpdateLog(Log a_log)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_STATUS, a_log.Getm_status());
        values.put(COL_NOTES, a_log.Getm_notes());
        values.put(COL_TASKID, a_log.Getm_taskId());
        values.put(COL_STARTED_AT, GetDateTime(a_log.Getm_start()));
        values.put(COL_ENDED_AT, GetDateTime(a_log.Getm_end()));
        values.put(COL_ELAPSEDUHR, a_log.Getm_elapsedTimeUHR());

        // Update
        long result = db.update(TABLE_LOG, values, COL_ID + " = ?",
                new String[] { String.valueOf(a_log.Getm_id()) });

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    // 10th: Setting table's RU methods -------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      AddSettings --> Adds a setting record.
     *
     * SYNOPSIS
     *      public boolean AddSettings()
     *
     * DESCRIPTION
     *      This method creates a settings record. The default values are true(1) for user
     *      deletion confirmation, and 7 for the number of priors days to show completed
     *      tasks (logs)
     *
     * RETURNS
     *      @return boolean --> True if successfully updated.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/16/2019
     */
    boolean AddSettings()
    {
        SQLiteDatabase db =  this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_ID, 1);
        values.put(COL_PROMPTDELETE, 1);
        values.put(COL_COMPLETEDDAYS, 7);

        // Insert data
        long result = db.insert(TABLE_SETTING, null, values);

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateSettings --> Updates a setting record.
     *
     * SYNOPSIS
     *      public boolean UpdateSettings(int a_promptDelete, int a_completedDays)
     *      @param a_promptDelete --> int: Indicates if a prompt to delete should be displayed.
     *      @param a_completedDays --> int: Prior number of days to show "Completed Tasks".
     *
     * DESCRIPTION
     *      This method updates a settings record.
     *
     * RETURNS
     *      @return boolean --> True if successfully updated.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/16/2019
     */
    boolean UpdateSettings(int a_promptDelete, int a_completedDays)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_PROMPTDELETE, a_promptDelete);
        values.put(COL_COMPLETEDDAYS, a_completedDays);
        int id = 1;

        // Update
        long result = db.update(TABLE_SETTING, values, COL_ID + " = ?",
                new String[] { String.valueOf(id) });

        // If there was an error, the preceding would return -1
        return result != -1;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetSettings --> Returns all settings.
     *
     * SYNOPSIS
     *      public JSONObject GetSettings()
     *
     * DESCRIPTION
     *      This method returns the settings. Since there are only 2 settings, a JSONObject
     *      is appropriate to use.
     *
     * RETURNS
     *      @return JSONObject --> JSON object with key, pair values.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/16/2019
     */
    JSONObject GetSettings()
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * " + " FROM " + TABLE_SETTING
                + " WHERE " + COL_ID + " = " + 1;

        JSONObject settings = new JSONObject();

        try (Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();

            // Get/Set settings
            settings.put("promptDelete", c.getInt(c.getColumnIndex(COL_PROMPTDELETE)));
            settings.put("completedDays", c.getInt(c.getColumnIndex(COL_COMPLETEDDAYS)));
        }
        catch (Exception e)
        {
            // Exception handling
            return settings;
        }

        return settings;
    }

    // 11th: Report methods --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      GetTypeReport --> Runs a report against logs based on task types.
     *
     * SYNOPSIS
     *      List<ReportType> GetTypeReport(String a_fromDate, String a_toDate)
     *      @param a_fromDate --> Start report date. Time when a log started.
     *      @param a_toDate --> End report date. Time when a log ended.
     *
     * DESCRIPTION
     *      This method executes a SQL statement against the log table to get a total number
     *      of hours grouped by task type, based on a range of dates.  It then, loops through
     *      the results to then populate and return a list of Type objects.
     *
     * RETURNS
     *      @return List<ReportType> --> List of ReportType objects.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/24/2019
     */
    List<ReportType> GetTypeReport(String a_fromDate, String a_toDate)
    {
        String selectQuery =  "SELECT " + TABLE_TYPE + "." + COL_ID +
                ", SUM(" + TABLE_LOG + "." + COL_ELAPSEDUHR +") AS TotalTime" +
                " FROM " + TABLE_LOG +
                " LEFT JOIN " + TABLE_TASK +
                " ON " + TABLE_LOG + "." + COL_TASKID + " = " + TABLE_TASK + "." + COL_ID +
                " LEFT JOIN " + TABLE_TYPE +
                " ON " + TABLE_TASK + "." + COL_TYPEID + " = " + TABLE_TYPE + "." + COL_ID +
                " WHERE " + TABLE_LOG + "." + COL_STARTED_AT + " >= '" + a_fromDate + "'" +
                " AND " + TABLE_LOG + "." + COL_ENDED_AT + " <= '" + a_toDate + "'" +
                " GROUP BY " + TABLE_TYPE + "." + COL_ID +
                " ORDER BY " + TABLE_TYPE + "." + COL_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        List<ReportType> typesFound = new ArrayList<>();
        try(Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            // Loop through all rows and add to list
            c.moveToFirst();

            do {

                int typeID = c.getInt(c.getColumnIndex(COL_ID));
                Double timeUHR = c.getDouble(c.getColumnIndex("TotalTime"));
                ReportType rt = new ReportType(GetType(typeID),timeUHR);

                // Add to list
                typesFound.add(rt);

            } while (c.moveToNext());
        }
        catch (Exception e)
        {
            // Exception handling
            return typesFound;
        }

        return typesFound;
    }

    /**
     * METHOD
     *
     * NAME
     *      GetTaskReport --> Runs a report against logs based on tasks.
     *
     * SYNOPSIS
     *      List<ReportTask> GetTaskReport(String a_fromDate, String a_toDate)
     *      @param a_fromDate --> Start report date. Time when a log started.
     *      @param a_toDate --> End report date. Time when a log ended.
     *
     * DESCRIPTION
     *      This method executes a SQL statement against the log table to get a total number
     *      of hours grouped by task, based on a range of dates.  It then, loops through
     *      the results to then populate and return a list of Task objects.
     *
     * RETURNS
     *      @return List<ReportTask> --> List of ReportTask objects.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    List<ReportTask> GetTaskReport(String a_fromDate, String a_toDate)
    {
        String selectQuery =  "SELECT " + TABLE_TASK + "." + COL_ID +
                ", SUM(" + TABLE_LOG + "." + COL_ELAPSEDUHR +") AS TotalTime" +
                " FROM " + TABLE_LOG +
                " LEFT JOIN " + TABLE_TASK +
                " ON " + TABLE_LOG + "." + COL_TASKID + " = " + TABLE_TASK + "." + COL_ID +
                " WHERE " + TABLE_LOG + "." + COL_STARTED_AT + " >= '" + a_fromDate + "'" +
                " AND " + TABLE_LOG + "." + COL_ENDED_AT + " <= '" + a_toDate + "'" +
                " GROUP BY " + TABLE_TASK + "." + COL_ID +
                " ORDER BY " + TABLE_TASK + "." + COL_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        List<ReportTask> tasksFound = new ArrayList<>();
        try(Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            // Loop through all rows and add to list
            c.moveToFirst();
            do {

                int taskID = c.getInt(c.getColumnIndex(COL_ID));
                Double timeUHR = c.getDouble(c.getColumnIndex("TotalTime"));
                ReportTask rt = new ReportTask(GetTask(taskID),timeUHR);

                // Add to list
                tasksFound.add(rt);

            } while (c.moveToNext());
        }
        catch (Exception e)
        {
            // Exception handling
            return tasksFound;
        }
        return tasksFound;
    }

    // 12th: Shared methods -------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      GetLastID --> Gets the ID of the last record added.
     *
     * SYNOPSIS
     *      public int GetLastID(String a_dataType)
     *      @param a_dataType --> String: Data type to fetch data from
     *
     * DESCRIPTION
     *      This method fetches the ID of the last record added. Due to the database
     *      assigning an ID, this method is usually called right after a new record is added.
     *      The id is used to correctly set a new element in the Recycler view's related
     *      array of elements.
     *
     * RETURNS
     *      @return int --> ID of the last record added.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    int GetLastID(String a_dataType)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + COL_ID + " FROM " + GetTableName(a_dataType)
                + " ORDER BY " + COL_ID + " DESC";

        try(Cursor c = db.rawQuery(selectQuery, null))
        {
            // Get data from cursor
            c.moveToFirst();
            return c.getInt(c.getColumnIndex(COL_ID));
        }
        catch (Exception e)
        {
            // Exception handling
            return -1;
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      Delete --> Deletes a record.
     *
     * SYNOPSIS
     *      public void DeleteType(int a_typeID)
     *      @param a_recordID --> Type: Record ID to delete.
     *      @param a_dataType --> String: Data type delete.
     *
     * DESCRIPTION
     *      This method deletes a record.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    void Delete(int a_recordID, String a_dataType)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(GetTableName(a_dataType), COL_ID + " = ?",
                new String[] { String.valueOf(a_recordID) });
    }

    // nth: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      GetDateTime --> Gets the current dateTime.
     *
     * SYNOPSIS
     *      private String GetDateTime()
     *
     * DESCRIPTION
     *      This method creates a dateTime string of current date and time.
     *
     * RETURNS
     *      @return String --> Current date and time.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    private String GetDateTime()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * METHOD
     *
     * NAME
     *      GetDateTime --> Overloaded method, returns the string conversion of a timestamp.
     *
     * SYNOPSIS
     *      private String GetDateTime(Timestamp a_timestamp)
     *      @param a_timestamp --> Timestamp: A timestamp
     *
     * DESCRIPTION
     *      This method creates a dateTime string from a timestamp.
     *
     * RETURNS
     *      @return String --> Date and time.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 4/2/2019
     */
    private String GetDateTime(Timestamp a_timestamp)
    {
        Date date = new Date();
        date.setTime(a_timestamp.getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date);
    }

    /**
     * METHOD
     *
     * NAME
     *      GetTimestamp --> Gets a timestamp from a string.
     *
     * SYNOPSIS
     *      private String GetDateTime(String a_stringTS)
     *      @param a_stringTS --> String: A string timestamp.
     *
     * DESCRIPTION
     *      This method creates a Timestamp from a string.
     *
     * RETURNS
     *      @return Timestamp --> Timestamp.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 4/2/2019
     */
    private Timestamp GetTimestamp(String a_stringTS)
    {
        Timestamp timestampBackUP = new Timestamp(System.currentTimeMillis());
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date parsedDate = dateFormat.parse(a_stringTS);

            return new java.sql.Timestamp(parsedDate.getTime());
        }
        catch(Exception e)
        {
            // If the try block does not work return the BackUp (current time)
            return timestampBackUP;
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      GetTableName --> Gets a table name.
     *
     * SYNOPSIS
     *      private String GetTableName(String a_dataType)
     *      @param a_dataType --> String: Data type associated to a table.
     *
     * DESCRIPTION
     *      This method returns a table name according to a data type.
     *
     * RETURNS
     *      @return String --> A table name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    private String GetTableName(String a_dataType)
    {
        switch (a_dataType)
        {
            case "type":
                return TABLE_TYPE;
            case "task":
                return TABLE_TASK;
            case "log":
                return TABLE_LOG;
        }
        return "";
    }
}