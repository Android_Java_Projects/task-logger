// Package and libraries used
package com.eg.tasklogger;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;
import static com.eg.tasklogger.TLApplication.NEWRECORD;
import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;

/**
 * CLASS
 *
 * NAME
 *      TaskFragment
 *
 * SYNOPSIS
 *      public class TaskFragment extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Task Descriptions fragment.  This is where the Task list
 *      is presented.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      10:00am 2/22/2019
 */

public class TaskFragment extends Fragment
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private List<Task> m_lstTask;                       // Holds all the tasks data
    private TaskRecyclerViewAdapter m_recyclerAdapter;  // Adapter that links recycler to data array
    DatabaseHelper m_databaseHelper;                    // Database connector

    // 3rd: UI Components ---------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      TaskFragment --> Class constructor.
     *
     * SYNOPSIS
     *      public TaskFragment()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    public TaskFragment()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *          when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_task, a_container, false);

        // Initialize Class
        Initialize(view);

        return view;
    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the fragment becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the fragment becomes active. It checks if there is
     *      any "pending" data that will update the array/recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Check if a new task type should be added/updated in the recycler
        if (getActivity() != null)
        {
            int pendingRecordID = ((TLApplication) getActivity().getApplication())
                    .Getm_pendingRecordID();
            if (pendingRecordID != NOTANINDEXNORID) UpdateRecycler(pendingRecordID);
        }
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      UpdateRecycler --> Updates the recycler view.
     *
     * SYNOPSIS
     *      private void UpdateRecycler(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the array related to the recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    private void UpdateRecycler(int a_pendingRecordID)
    {
        int pendingIndex = 0;
        int pendingAction = 0;

        if (getActivity() != null)
        {
            pendingIndex = ((TLApplication) getActivity().getApplication()).Getm_pendingIndex();
            pendingAction = ((TLApplication) getActivity().getApplication()).Getm_pendingAction();
        }
        switch (pendingAction)
        {
            case NEWRECORD:
                Task newTask = m_databaseHelper.GetTask(a_pendingRecordID);
                m_lstTask.add(newTask);
                break;

            case EDITRECORD:
                Task editedTask = m_databaseHelper.GetTask(a_pendingRecordID);
                m_lstTask.set(pendingIndex,editedTask);
                break;

            case DELETERECORD:
                m_lstTask.remove(pendingIndex);
                break;
        }

        // Update recycler and reset pending to process data.
        m_recyclerAdapter.notifyDataSetChanged();

        if (getActivity() != null)
            ((TLApplication) getActivity().getApplication()).ResetPending();
    }

    /**
     * METHOD
     *
     * NAME
     *      AddTask --> Calls the activity that adds a task.
     *
     * SYNOPSIS
     *      private void AddTask()
     *
     * DESCRIPTION
     *      This method calls the activity used to add a task.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/23/2019
     */
    private void AddTask()
    {
        if (getActivity() != null)
        {
            Intent myIntent = new Intent(getActivity(), TaskAEActivity.class);
            myIntent.putExtra("message", "new|0|0");
            getActivity().startActivity(myIntent);
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize(View a_view)
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        // Set member variables
        m_databaseHelper = new DatabaseHelper(getContext());
        m_lstTask = new ArrayList<>();
        m_lstTask = m_databaseHelper.GetAllTasks();

        // Update the top bar
        if (getActivity() != null)
            ((MainActivity) getActivity()).SetActionBarTitle(R.string.task_title);

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = a_view.findViewById(R.id.fabTask);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Add Task Definition action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                AddTask();
            }
        });

        // Set the Recycler/List
        RecyclerView recyclerView = a_view.findViewById(R.id.recyclerView_Task);
        m_recyclerAdapter = new TaskRecyclerViewAdapter(getContext(),m_lstTask );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(m_recyclerAdapter);
    }
}
