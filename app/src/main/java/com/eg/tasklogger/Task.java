// Package and libraries used
package com.eg.tasklogger;

/**
 * CLASS
 *
 * NAME
 *      Task
 *
 * SYNOPSIS
 *      public class Task
 *
 * DESCRIPTION
 *      This class is used to manage Task information.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      4:00pm 3/24/2019
 */
public class Task
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private int m_id;              // Task id
    private String m_name;         // Task name
    private String m_description;  // Task description
    private Type m_type;           // Task type

    // 3rd: UI Components ---------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Task --> Class constructor.
     *
     * SYNOPSIS
     *      public Task()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    public Task()
    {
        this.m_id = 0;
        this.m_name = "";
        this.m_description = "";
        this.m_type = new Type();
    }

    /**
     * METHOD
     *
     * NAME
     *      Task --> Overloaded Class constructor.
     *
     * SYNOPSIS
     *      public Task(int a_id, String a_name, String a_description, Type a_type)
     *      @param a_id --> int: Task id.
     *      @param a_name --> String: Task name to set.
     *      @param a_description --> String: Task description.
     *      @param a_type --> int: Task type.
     *
     * DESCRIPTION
     *      This method is an overloaded class constructor. Used to set member variables on
     *      instantiation.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    public Task(int a_id, String a_name, String a_description, Type a_type)
    {
        this.m_id = a_id;
        this.m_name = a_name;
        this.m_description = a_description;

        this.m_type = new Type();
        this.m_type = a_type;
    }

    // 5th: Event handlers --------------------------------------------------------------------
    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Getm_Id --> Returns the task id.
     *
     * SYNOPSIS
     *      public int Getm_id()
     *
     * DESCRIPTION
     *      This method returns the task id.
     *
     * RETURNS
     *      @return int --> Task id.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    public int Getm_id()
    {
        return m_id;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_name --> Returns the task name.
     *
     * SYNOPSIS
     *      public String Getm_name()
     *
     * DESCRIPTION
     *      This method returns the task name.
     *
     * RETURNS
     *      @return String --> Task name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    public String Getm_name()
    {
        return m_name;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_description --> Returns the task description.
     *
     * SYNOPSIS
     *      public String Getm_description()
     *
     * DESCRIPTION
     *      This method returns the task description.
     *
     * RETURNS
     *      @return String --> Task description.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    String Getm_description()
    {
        return m_description;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typeId --> Returns the task type id.
     *
     * SYNOPSIS
     *      public String Getm_typeId()
     *
     * DESCRIPTION
     *      This method returns the task type id.
     *
     * RETURNS
     *      @return int --> Task type id.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    int Getm_typeId()
    {
        return m_type.Getm_id();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typeColor --> Returns the task type color number.
     *
     * SYNOPSIS
     *      public int Getm_typeColor()
     *
     * DESCRIPTION
     *      This method returns the task type color number.
     *
     * RETURNS
     *      @return int --> Task type color number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    int Getm_typeColor()
    {
        return m_type.Getm_color();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typecolorHex --> Returns the task type Hex color as a string.
     *
     * SYNOPSIS
     *      public String Getm_typeColorHex()
     *
     * DESCRIPTION
     *      This method returns the task type Hex color as a string.
     *
     * RETURNS
     *      @return String --> String representing the task type Hex color.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    String Getm_typeColorHex()
    {
        return m_type.Getm_colorHex();
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_typeName --> Returns the task type name.
     *
     * SYNOPSIS
     *      public int Getm_typeName()
     *
     * DESCRIPTION
     *      This method returns the task type name.
     *
     * RETURNS
     *      @return String --> Task type name.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    String Getm_typeName()
    {
        return m_type.Getm_name();
    }

    // 7th: Mutators --------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Setm_id --> Sets the task id.
     *
     * SYNOPSIS
     *      public void Setm_id(int a_id)
     *      @param a_id --> int: Task id.
     *
     * DESCRIPTION
     *      This method sets the task id.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    void Setm_id(int a_id)
    {
        this.m_id = a_id;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_name --> Sets the task name.
     *
     * SYNOPSIS
     *      public void Setm_name(String a_name)
     *      @param a_name --> String: Task name.
     *
     * DESCRIPTION
     *      This method sets the task name.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    void Setm_name(String a_name)
    {
        this.m_name = a_name;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_description --> Sets the task description.
     *
     * SYNOPSIS
     *      public void Setm_description(String a_description)
     *      @param a_description --> String: Task description.
     *
     * DESCRIPTION
     *      This method sets the task description.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    void Setm_description(String a_description)
    {
        this.m_description = a_description;
    }

    /**
     * METHOD
     *
     * NAME
     *      Setm_type --> Sets the task type.
     *
     * SYNOPSIS
     *      public void Setm_type(Type a_type)
     *      @param a_type --> Type: Task type.
     *
     * DESCRIPTION
     *      This method sets the task type.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    void Setm_type(Type a_type)
    {
        this.m_type = a_type;
    }

    // 8th: Any utility (private) methods -----------------------------------------------------
}
