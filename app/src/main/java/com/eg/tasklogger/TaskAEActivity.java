// Package and libraries used
package com.eg.tasklogger;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;
import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;

/**
 * CLASS
 *
 * NAME
 *      TaskAEActivity
 *
 * SYNOPSIS
 *      public class TaskAEActivity extends TLBaseActivity
 *
 * DESCRIPTION
 *      This class handles the New/Edit Task Activity. On this activity, the user can add or
 *      edit a Task.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00am 3/2/2019
 */
public class TaskAEActivity extends TLBaseActivity
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private Task m_task;                            // A Task object
    private List<Type> m_lstType;                   // Holds all the task types data
    private List<String> m_types;                   // Holds all the task types names
    private int m_lstTypeChosenIndex;               // Index of the type chosen
    private ArrayAdapter<String> m_dropDownAdapter; // Adapter used by the type drop down list
    private Spinner m_dropDown;                     // Type drop down

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      DeleteRecord --> Deletes a Task record.
     *
     * SYNOPSIS
     *      public void DeleteRecord()
     *
     * DESCRIPTION
     *      This method sends a delete command to the database to remove the current Task record.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/25/2019
     */
    public void DeleteRecord()
    {
        // There has to be at least one task description in the database
        if (m_databaseHelper.GetAllTasks().size() < 2)
        {
            SimpleDialog("Wait!","There has to be at least one task " +
                    "description registered. Deletion of this task is not allowed...Sorry");
            return;
        }

        // Do not allow deletion if the task is being used.
        if (m_databaseHelper.GetTaskUsageCount(m_task.Getm_id()) > 0)
        {
            SimpleDialog("Wait!","This task definition is being used. " +
                    "If I allow you to delete it, the data will become inconsistent. Imagine " +
                    "having a log related to a task that no longer exists...Sorry!");
            return;
        }

        // Prompt the user, if required.
        if (Getm_promptDelete())
        {
            //Put up a Yes/No message box
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setIcon(R.drawable.ic_warning)
                    .setTitle("Delete Task Record")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //Yes button clicked
                            ExecuteDelete(m_task);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else ExecuteDelete(m_task);
    }

    // 4th: Constructor -----------------------------------------------------------------------
    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the layout/activity opens.
     *
     * SYNOPSIS
     *      protected void onCreate(Bundle a_savedInstanceState)
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of your Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the layout/class.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    @Override
    protected void onCreate(Bundle a_savedInstanceState)
    {
        super.onCreate(a_savedInstanceState);
        setContentView(R.layout.activity_task_ae);

        // Get the transferred data from calling activity.
        Intent intent = getIntent();
        final String message = intent.getStringExtra("message");

        // Initialize Class
        Initialize(message);

    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the activity becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the activity becomes active. It checks if there is
     *      any "pending" data that will update the spinner (Drop down list).
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/26/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Check if a new type was added and reset the spinner
        int pendingRecordID = ((TLApplication) this.getApplication()).Getm_pendingRecordID();
        if (pendingRecordID != NOTANINDEXNORID) UpdateSpinner(pendingRecordID);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SaveTaskData --> Saves the Task data entered.
     *
     * SYNOPSIS
     *      private void SaveTaskData()
     *
     * DESCRIPTION
     *      This method saves the Task data entered, into a database.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 3/24/2019
     */
    private void SaveTaskData()
    {
        // Check if the required data was entered
        if (!IsRequiredEntered()) return;

        // Set name, description, and type
        EditText taskName = findViewById(R.id.editTaskName);
        m_task.Setm_name(taskName.getText().toString());
        EditText taskDescription = findViewById(R.id.editTaskDescription);
        m_task.Setm_description(taskDescription.getText().toString());
        m_task.Setm_type(m_lstType.get(m_lstTypeChosenIndex));

        // Edit/Add a type in the database
        boolean updateResult;
        int action = Getm_actionType();
        if (action == EDITRECORD) updateResult = m_databaseHelper.UpdateTask(m_task);
        else
        {
            updateResult = m_databaseHelper.AddTask(m_task);
            m_task.Setm_id(m_databaseHelper.GetLastID("task"));
        }

        // Notify user
        if (updateResult)
        {
            String actionMessage = ((Getm_actionType() == EDITRECORD) ? "Updated" : "Saved");
            Toast.makeText(this, actionMessage + " successfully",
                    Toast.LENGTH_SHORT).show();

            // Set pending data
            if (getApplication() != null)
                ((TLApplication) this.getApplication()).Setm_pending(m_task.Getm_id()
                    ,Getm_position(),action);

            finish();
        }
        else Toast.makeText(this, "There was an error, please try again",
                Toast.LENGTH_SHORT).show();
    }

    /**
     * METHOD
     *
     * NAME
     *      IsRequiredEntered --> Checks if all required data was entered.
     *
     * SYNOPSIS
     *      private boolean IsRequiredEntered()
     *
     * DESCRIPTION
     *      This method checks if all required data was entered. If false, the application
     *      displays a warning to the user.
     *
     * RETURNS
     *      @return boolean --> True if all required data was entered.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/24/2019
     */
    private boolean IsRequiredEntered()
    {
        // Check if the name is empty
        EditText taskName = findViewById(R.id.editTaskName);
        if (taskName.getText().toString().equals(""))
        {
            TextView nameWarningLabel = findViewById(R.id.labelTaskNameWarning);
            nameWarningLabel.setVisibility(View.VISIBLE);

            Button nameWarningButton = findViewById(R.id.buttonTaskNameWarning);
            nameWarningButton.setVisibility(View.VISIBLE);

            return  false;
        }
        return true;
    }

    /**
     * METHOD
     *
     * NAME
     *      LoadTask --> Loads Task data into the appropriate objects.
     *
     * SYNOPSIS
     *      private void LoadTask()
     *
     * DESCRIPTION
     *      This method loads Task data into the appropriate objects.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    private void LoadTask()
    {
        // Check if new or edit
        if (Getm_actionType() == EDITRECORD)
        {
            // Trying to edit a record
            m_task = new Task();
            m_task = m_databaseHelper.GetTask(Getm_recordID());

            m_dropDown.setSelection(m_dropDownAdapter.getPosition(m_task.Getm_typeName()));
            m_lstTypeChosenIndex = m_dropDown.getSelectedItemPosition();
        }
        else
        {
            // This is a new record, set defaults.
            m_task = new Task(0,"",""
                    ,m_lstType.get(m_lstTypeChosenIndex));
        }

        // Populate name, description, and color
        EditText inputName = findViewById(R.id.editTaskName);
        inputName.setText(m_task.Getm_name());

        EditText inputDescription = findViewById(R.id.editTaskDescription);
        inputDescription.setText(m_task.Getm_description());

        SetTypeColor(m_task.Getm_typeColor());
    }

    /**
     * METHOD
     *
     * NAME
     *      SetTypeColor --> Sets the type color.
     *
     * SYNOPSIS
     *      private void SetTypeColor(int a_color)
     *      @param a_color --> int: color represented by an integer.
     *
     * DESCRIPTION
     *      This method sets the type color.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/24/2019
     */
    private void SetTypeColor(int a_color)
    {
        // Change button color
        Button colorButton = findViewById( R.id.buttonTaskTypeColor);
        GradientDrawable drawable = (GradientDrawable) colorButton.getBackground();
        drawable.setColor(a_color);
    }

    /**
     * METHOD
     *
     * NAME
     *      AddType --> Calls the activity that adds a task type.
     *
     * SYNOPSIS
     *      private void AddType()
     *
     * DESCRIPTION
     *      This method calls the activity used to add a task type.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/26/2019
     */
    private void AddType()
    {
        Intent myIntent = new Intent(this, TypeAEActivity.class);
        myIntent.putExtra("message", "new|0|0" );
        startActivity(myIntent);
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateSpinner --> Updates the spinner object.
     *
     * SYNOPSIS
     *      private void UpdateSpinner(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the spinner(Drop down list) object used to select a task type.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/26/2019
     */
    private void UpdateSpinner(int a_pendingRecordID)
    {
        // Get types from the DB
        m_lstType = m_databaseHelper.GetAllTypes();
        Type newType = m_databaseHelper.GetType(a_pendingRecordID);

        // Re-Build the string array needed by the spinner
        m_types.clear();
        for(Type type : m_lstType) m_types.add(type.Getm_name());
        m_types.add("Add New...");
        m_dropDownAdapter.notifyDataSetChanged();

        // Resets pending
        if (getApplication() != null)
            ((TLApplication) this.getApplication()).ResetPending();

        // Set the type to the new type added
        m_dropDown.setSelection(m_dropDownAdapter.getPosition(newType.Getm_name()));
        m_lstTypeChosenIndex = m_dropDown.getSelectedItemPosition();
        SetTypeColor(m_lstType.get(m_lstTypeChosenIndex).Getm_color());
    }

    /**
     * METHOD
     *
     * NAME
     *      InitializeTypeDropDown --> Initializes the type drop down.
     *
     * SYNOPSIS
     *      private void InitializeTypeDropDown()
     *
     * DESCRIPTION
     *      This method initializes the type drop down.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/25/2019
     */
    private void InitializeTypeDropDown()
    {
        // Get all types
        m_lstType = new ArrayList<>();
        m_lstType = m_databaseHelper.GetAllTypes();

        // Build the string array needed by the spinner
        m_types = new ArrayList<>();
        for(Type type : m_lstType) m_types.add(type.Getm_name());
        m_types.add("Add New...");

        // Set spinner
        m_dropDown = findViewById(R.id.spinnerTaskType);
        m_dropDownAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, m_types);
        m_dropDown.setAdapter(m_dropDownAdapter);

        m_dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onItemSelected --> Android Studio generated method, this method runs when an
             *          item is selected in the type drop down.
             *
             * SYNOPSIS
             *      public void onItemSelected(AdapterView<?> parent, View view,
             *          int position, long id)
             *      @param a_parent --> AdapterView<?>: Adapter View object.
             *      @param a_view --> View: Calling view object.
             *      @param a_position --> int: Position of a list item
             *      @param a_id --> long: Id of the list item
             *
             * DESCRIPTION
             *      This method runs when an item is selected in the type drop down.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00pm 3/25/2019
             */
            @Override
            public void onItemSelected(AdapterView<?> a_parent, View a_view,
                                       int a_position, long a_id)
            {
                // Check if Add New...was selected
                int addNewPosition = m_dropDownAdapter.getPosition("Add New...");
                if (m_dropDown.getSelectedItemPosition() == addNewPosition)
                {
                    m_dropDown.setSelection(0);
                    // Call the Add Task Type activity.
                    AddType();
                }
                else
                {
                    // Store the selected type index and update the type color
                    m_lstTypeChosenIndex = m_dropDown.getSelectedItemPosition();
                    SetTypeColor(m_lstType.get(m_lstTypeChosenIndex).Getm_color());
                }
            }

            // Empty required event definition, not used.
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current activity.
     *
     * SYNOPSIS
     *      private void Initialize(String a_message)
     *      @param a_message --> String: message sent by the calling activity.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void Initialize(String a_message)
    {
        // Update the top bar
        SetTopBar();

        // Set super member variables
        SetMemberVariables(a_message);

        // Set Activity title
        SetActivityTitle(R.string.taskNew_title, R.string.taskEdit_title);

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = findViewById(R.id.fabAETask);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Add New Task Definition action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                SaveTaskData();
            }
        });

        // Populate list of types
        InitializeTypeDropDown();

        // Set the data
        LoadTask();
    }
}
