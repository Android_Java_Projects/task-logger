// Package and libraries used
package com.eg.tasklogger;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;
import static com.eg.tasklogger.TLApplication.NEWRECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;

/**
 * CLASS
 *
 * NAME
 *      TypeFragment
 *
 * SYNOPSIS
 *      public class TypeFragment extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Types of Tasks fragment.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      10:00am 2/22/2019
 */
public class TypeFragment extends Fragment
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private List<Type> m_lstType; // Holds all the task types data
    private TypeRecyclerViewAdapter m_recyclerAdapter;  // Adapter that links recycler to data array
    DatabaseHelper m_databaseHelper; // Database connector

    // 3rd: UI Components --------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      TypeFragment --> Class constructor.
     *
     * SYNOPSIS
     *      public TypeFragment()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    public TypeFragment()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *          when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_type, a_container, false);

        // Initialize Class
        Initialize(view);

        return view;
    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the fragment becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the fragment becomes active. It checks if there is
     *      any "pending" data that will update the array/recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Check if a new task type should be added/updated in the recycler
        if (getActivity() != null)
        {
            int pendingRecordID = ((TLApplication) getActivity().getApplication()).Getm_pendingRecordID();
            if (pendingRecordID != NOTANINDEXNORID) UpdateRecycler(pendingRecordID);
        }
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      UpdateRecycler --> Updates the recycler view.
     *
     * SYNOPSIS
     *      private void UpdateRecycler(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the array related to the recycler view.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void UpdateRecycler(int a_pendingRecordID)
    {
        int pendingIndex = 0;
        int pendingAction = 0;

        if (getActivity() != null)
        {
            pendingIndex = ((TLApplication) getActivity().getApplication()).Getm_pendingIndex();
            pendingAction = ((TLApplication) getActivity().getApplication()).Getm_pendingAction();
        }
        switch (pendingAction)
        {
            case NEWRECORD:
                Type newType = m_databaseHelper.GetType(a_pendingRecordID);
                m_lstType.add(newType);
                break;

            case EDITRECORD:
                Type editedType = m_databaseHelper.GetType(a_pendingRecordID);
                m_lstType.set(pendingIndex,editedType);
                break;

            case DELETERECORD:
                m_lstType.remove(pendingIndex);
                break;
        }

        // Update recycler and reset pending to process data.
        m_recyclerAdapter.notifyDataSetChanged();
        ((TLApplication) getActivity().getApplication()).ResetPending();
    }

    /**
     * METHOD
     *
     * NAME
     *      AddType --> Calls the activity that adds a task type.
     *
     * SYNOPSIS
     *      private void AddType()
     *
     * DESCRIPTION
     *      This method calls the activity used to add/edit a task type.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void AddType()
    {
        if (getActivity() != null)
        {
            Intent myIntent = new Intent(getActivity(), TypeAEActivity.class);
            myIntent.putExtra("message", "new|0|0");
            getActivity().startActivity(myIntent);
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_view --> Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        // Set member variables
        m_databaseHelper = new DatabaseHelper(getContext());
        m_lstType = new ArrayList<>();
        m_lstType = m_databaseHelper.GetAllTypes();
        //m_lstType = m_databaseHelper.GetTypeReport("2019-04-22 00:00:01", "2019-04-24 23:59:59");

        // Update the top bar
        if (getActivity() != null)
            ((MainActivity) getActivity()).SetActionBarTitle(R.string.type_title);

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = a_view.findViewById(R.id.fabType);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Add New task type action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                // Execute when fab button is clicked
                AddType();
            }
        });

        // Set the Recycler/List
        RecyclerView recyclerView = a_view.findViewById(R.id.recyclerView_Type);
        m_recyclerAdapter = new TypeRecyclerViewAdapter(getContext(),m_lstType );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(m_recyclerAdapter);
    }
}
