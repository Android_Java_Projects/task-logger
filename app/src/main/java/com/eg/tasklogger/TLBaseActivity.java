// Package and libraries used
package com.eg.tasklogger;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import java.util.regex.Pattern;

import static com.eg.tasklogger.TLApplication.DELETERECORD;
import static com.eg.tasklogger.TLApplication.NORECORD;
import static com.eg.tasklogger.TLApplication.NEWRECORD;
import static com.eg.tasklogger.TLApplication.EDITRECORD;

/**
 * CLASS
 *
 * NAME
 *      TLBaseActivity
 *
 * SYNOPSIS
 *      public abstract class TLBaseActivity extends AppCompatActivity
 *
 * DESCRIPTION
 *      This class is the basis for all Activities, except MainActivity. It extends
 *      AppCompatActivity, which is Android generated and handles most activities (screens) in
 *      Android applications. No instantiation of this class is necessary.
 *      This Class is set as an abstract.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00am 3/2/2019
 */
public abstract class TLBaseActivity extends AppCompatActivity
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private int m_recordID;             // A record ID
    private int m_actionType;           // An action type
    private int m_position;             // Recycler's listArray position where a record is
    DatabaseHelper m_databaseHelper;    // Database connector
    private boolean m_promptDelete;     // True if the user has to confirm a record deletion

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      ExecuteDelete --> Executes the delete command.
     *
     * SYNOPSIS
     *      public void ExecuteDelete(Type a_type)
     *      @param a_type --> Type: Type object.
     *
     * DESCRIPTION
     *      Overloaded method that sends a delete command to the database to remove a
     *      type record. It also sets pending data for later reference.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/18/2019
     */
    public void ExecuteDelete(Type a_type)
    {
        m_databaseHelper.Delete(a_type.Getm_id(),"type");
        Toast.makeText(this, "Deleted successfully",
                Toast.LENGTH_SHORT).show();

        // Set pending data
        ((TLApplication) this.getApplication()).Setm_pending(a_type.Getm_id(),
                Getm_position(),DELETERECORD);

        finish();
    }

    /**
     * METHOD
     *
     * NAME
     *      ExecuteDelete --> Executes the delete command.
     *
     * SYNOPSIS
     *      public void ExecuteDelete(Task a_task)
     *      @param a_task --> Task: Task object.
     *
     * DESCRIPTION
     *      Overloaded method that sends a delete command to the database to remove a
     *      task record. It also sets pending data for later reference.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/18/2019
     */
    public void ExecuteDelete(Task a_task)
    {
        m_databaseHelper.Delete(a_task.Getm_id(),"task");
        Toast.makeText(this, "Deleted successfully",
                Toast.LENGTH_SHORT).show();

        // Set pending data
        ((TLApplication) this.getApplication()).Setm_pending(a_task.Getm_id(),
                Getm_position(),DELETERECORD);

        finish();
    }

    /**
     * METHOD
     *
     * NAME
     *      ExecuteDelete --> Executes the delete command.
     *
     * SYNOPSIS
     *      public void ExecuteDelete(Log a_log)
     *      @param a_log --> Log: Log object.
     *
     * DESCRIPTION
     *      Overloaded method that sends a delete command to the database to remove a
     *      log record. It also sets pending data for later reference.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/18/2019
     */
    public void ExecuteDelete(Log a_log)
    {
        m_databaseHelper.Delete(a_log.Getm_id(),"log");
        Toast.makeText(this, "Deleted successfully",
                Toast.LENGTH_SHORT).show();

        // Set pending data
        ((TLApplication) this.getApplication()).Setm_pending(a_log.Getm_id(),
                Getm_position(),DELETERECORD);

        finish();
    }

    /**
     * METHOD
     *
     * NAME
     *      DisplayTimeDifference --> Display time difference between start and end date.
     *
     * SYNOPSIS
     *      private void DisplayTimeDifference(Log a_log)
     *      @param a_log --> Log: A log object.
     *
     * DESCRIPTION
     *      This method displays the time difference between the start date/time and end
     *      date/time. A message is displayed in the toast object.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 4/4/2019
     */
    public void DisplayTimeDifference(Log a_log)
    {
        // Try to display the elapsed time as a string format
        try
        {
            String UHRString = ((TLApplication) this.getApplication())
                    .GetReadableTime(a_log.Getm_elapsedTimeUHR());

            //Display only if something was returned.
            if (!UHRString.equals(""))
                Toast.makeText(this, UHRString, Toast.LENGTH_SHORT).show();
        }
        catch (Exception e)
        {
            // Don't display anything
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      SetWarningVisibility --> Sets the visibility of warning text and buttons.
     *
     * SYNOPSIS
     *      private void SetWarningVisibility(int a_WarningTextView, int a_WarningButton
     *          , int a_visible)
     *      @param a_warningTextView --> int: Text View object id.
     *      @param a_warningButton --> int: Button View object id.
     *      @param a_visible --> int: Visibility code.
     *
     * DESCRIPTION
     *      This method Sets the visibility of warning text and buttons.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/9/2019
     */
    public void SetWarningVisibility(int a_warningTextView, int a_warningButton, int a_visible)
    {
        TextView warningLabel = findViewById(a_warningTextView);
        warningLabel.setVisibility(a_visible);

        Button warningButton = findViewById(a_warningButton);
        warningButton.setVisibility(a_visible);
    }

    /**
     * METHOD
     *
     * NAME
     *      SetMemberVariables --> Sets the member variables.
     *
     * SYNOPSIS
     *      public void SetMemberVariables(String a_message)
     *      @param a_message --> String: a message to parse.
     *
     * DESCRIPTION
     *      This method sets member variables according to a pipe delimited string.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    public void SetMemberVariables(String a_message)
    {
        m_actionType = NORECORD;

        String[] messageArray = a_message.split(Pattern.quote("|"));
        if (messageArray[0].equals("edit")) m_actionType = EDITRECORD;
        else if (messageArray[0].equals("new")) m_actionType = NEWRECORD;

        m_recordID = Integer.parseInt(messageArray[1]);
        m_position = Integer.parseInt(messageArray[2]);

        // Set the database
        m_databaseHelper = new DatabaseHelper(this);

        // Set the prompt before delete flag
        JSONObject currentSettings = m_databaseHelper.GetSettings();
        try
        {
            String delPrompt = currentSettings.getString("promptDelete");
            m_promptDelete = Integer.parseInt(delPrompt) == 1;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * METHOD
     *
     * NAME
     *      SetActivityTitle --> Sets the activity title according to a message passed.
     *
     * SYNOPSIS
     *      public void SetActivityTitle(int a_newTitle, int a_editTitle)
     *      @param a_newTitle --> int: Title to set if the action is "new"
     *      @param a_editTitle --> int: Title to se if the action is "edit"
     *
     * DESCRIPTION
     *      This method sets the activity title according to a message passed by the calling
     *      activity.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    public void SetActivityTitle(int a_newTitle, int a_editTitle)
    {
        if (m_actionType == NEWRECORD) setTitle(a_newTitle);
        else setTitle(a_editTitle);
    }

    /**
     * METHOD
     *
     * NAME
     *      SetTopBar --> Sets the action/top bar of an activity/layout.
     *
     * SYNOPSIS
     *      public void SetTopBar()
     *
     * DESCRIPTION
     *      This method sets the activity/layout action/top bar.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/16/2019
     */
    public void SetTopBar()
    {
        // Update the top bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.reportResult_title);

        // Enable up button
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * METHOD
     *
     * NAME
     *      EndActivity --> Ends the current activity.
     *
     * SYNOPSIS
     *      public void EndActivity()
     *
     * DESCRIPTION
     *      This method closes the current activity. The calling activity is then shown.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    public void EndActivity()
    {
        finish();
    }

    /**
     * METHOD
     *
     * NAME
     *      SimpleDialog --> Displays a simple dialog box.
     *
     * SYNOPSIS
     *      public void SimpleDialog(String a_title, String a_message)
     *      @param a_title --> String: Dialog box title.
     *      @param a_message --> String: Dialog box message.
     *
     * DESCRIPTION
     *      This method displays a simple dialog box with an Ok button.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/25/2019
     */
    public void SimpleDialog(String a_title, String a_message)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setIcon(R.drawable.ic_warning);
        alertDialogBuilder.setTitle(a_title);
        alertDialogBuilder.setMessage(a_message);
        alertDialogBuilder.setPositiveButton("Ok",null);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * METHOD
     *
     * NAME
     *      DeleteRecord --> Abstract method. On implementation it will delete records
     *
     * SYNOPSIS
     *      public abstract void DeleteRecord();
     *
     * DESCRIPTION
     *      Abstract method. All classes who inherit from this class
     *      will have their own implementation of this method. This is useful since most activities
     *      have the same options menu. They should all call the DeleteRecord method when
     *      the user tries to delete a record. The DeleteRecord method performs different
     *      delete record calls to the database.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 4/1/2019
     */
    public abstract void DeleteRecord();

    // 4th: Constructor -----------------------------------------------------------------------
    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateOptionsMenu --> Android Studio generated method, creates the default menu
     *          in the Action(Top) Bar.
     *
     * SYNOPSIS
     *      public boolean onCreateOptionsMenu(Menu a_menu)
     *      @param a_menu --> Menu: Object to create.
     *
     * DESCRIPTION
     *      This method creates the default menu in the Action Bar when the activity opens.
     *
     * RETURNS
     *      @return boolean --> True if successfully created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 3/19/2019
     */
    @Override
    public boolean onCreateOptionsMenu(Menu a_menu)
    {
        if (m_actionType == EDITRECORD){
            getMenuInflater().inflate(R.menu.genericmenu, a_menu);
        }

        return true;
    }

    /**
     * METHOD
     *
     * NAME
     *      finalize --> Android Studio generated method, executes when the activity finalizes.
     *
     * SYNOPSIS
     *      public void finalize()
     *
     * DESCRIPTION
     *      This method runs when the current activity finalizes.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04pm 3/19/2019
     */
    @Override
    public void finalize()
    {
        // Close the database
        m_databaseHelper.close();
    }

    /**
     * METHOD
     *
     * NAME
     *      onBackPressed --> Android Studio generated method, this method is called when the mobile
     *          device's back button is pressed.
     *
     * SYNOPSIS
     *      public void onBackPressed()
     *
     * DESCRIPTION
     *      This method handles the mobile device's back button.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:04am 3/3/2019
     */
    @Override
    public void onBackPressed()
    {
        EndActivity();
    }

    /**
     * METHOD
     *
     * NAME
     *      onOptionsItemSelected --> Android Studio generated method, listener for the
     *          Action Bar menu.
     *
     * SYNOPSIS
     *      public boolean onOptionsItemSelected(MenuItem a_item)
     *      @param a_item --> MenuItem: Menu item object pressed.
     *
     * DESCRIPTION
     *      This method handles the action bar menu item clicks.
     *
     * RETURNS
     *      @return boolean --> True if successfully processed.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem a_item)
    {
        switch (a_item.getItemId())
        {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button
                EndActivity();
                return true;
            case R.id.action_remove:
                // Delete pressed
                DeleteRecord();
        }
        return super.onOptionsItemSelected(a_item);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      Getm_actionType --> Returns the action type number.
     *
     * SYNOPSIS
     *      public int Getm_actionType()
     *
     * DESCRIPTION
     *      This method returns the action type number. The action type number is defined by the
     *      nature of the call, the user may be trying to update or edit a record. The action type
     *      number is set using the global constant set defined in TLApplication.
     *
     * RETURNS
     *      @return int --> Action type number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public int Getm_actionType()
    {
        return m_actionType;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_recordID --> Returns a record ID.
     *
     * SYNOPSIS
     *      public int Getm_recordID()
     *
     * DESCRIPTION
     *      This method returns a record ID. The record ID is set when the user tries to
     *      update a record.
     *
     * RETURNS
     *      @return int --> Record ID number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public int Getm_recordID()
    {
        return m_recordID;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_position --> Returns an index number.
     *
     * SYNOPSIS
     *      public int Getm_position()
     *
     * DESCRIPTION
     *      This method returns an index number. The index number is set when the user tries to
     *      update a record. This number represents the array position in a recycler view.
     *
     * RETURNS
     *      @return int --> Index number.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 3/23/2019
     */
    public int Getm_position()
    {
        return m_position;
    }

    /**
     * METHOD
     *
     * NAME
     *      Getm_promptDelete --> Returns the prompt delete flag.
     *
     * SYNOPSIS
     *      public boolean Getm_promptDelete()
     *
     * DESCRIPTION
     *      This method returns the prompt delete flag. This flag indicates if the user
     *      needs to be prompted before deleting a record.
     *
     * RETURNS
     *      @return boolean --> True if the user needs to confirm record deletion.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/17/2019
     */
    public boolean Getm_promptDelete()
    {
        return m_promptDelete;
    }

    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
}
