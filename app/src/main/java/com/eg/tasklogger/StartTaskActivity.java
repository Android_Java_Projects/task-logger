// Package and libraries used
package com.eg.tasklogger;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static com.eg.tasklogger.TLApplication.NOTANINDEXNORID;
import static com.eg.tasklogger.TLApplication.RUNNINGLOG;

/**
 * CLASS
 *
 * NAME
 *      StartTaskActivity
 *
 * SYNOPSIS
 *      public class StartTaskActivity extends TLBaseActivity
 *
 * DESCRIPTION
 *      This class handles the Start Task Activity.  On this activity, the user sets the initial
 *      parameters of a log.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00am 3/2/2019
 */
public class StartTaskActivity extends TLBaseActivity
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    private List<Task> m_lstTask;                   // Holds all the task data
    private List<String> m_tasks;                   // Holds all the task names
    private int m_lstTaskChosenIndex;               // Index of the task chosen
    private ArrayAdapter<String> m_dropDownAdapter; // Adapter used by the task drop down list
    private Spinner m_dropDown;                     // Task drop down

    // 3rd: UI Components ---------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      DeleteRecord --> No action to perform.
     *
     * SYNOPSIS
     *      public void DeleteRecord()
     *
     * DESCRIPTION
     *      No implementation necessary. This method definition is required since the base
     *      class has this method defined as an abstract. Record deletion is not required in
     *      this module.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/01/2019
     */
    public void DeleteRecord() { }

    // 4th: Constructor -----------------------------------------------------------------------
    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreate --> Android Studio generated method, this is the first method that executes
     *          when the layout/activity opens.
     *
     * SYNOPSIS
     *      protected void onCreate(Bundle a_savedInstanceState)
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of your Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the layout/class.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:04pm 2/21/2019
     */
    @Override
    protected void onCreate(Bundle a_savedInstanceState)
    {
        super.onCreate(a_savedInstanceState);
        setContentView(R.layout.activity_start_task);

        // Get the transferred data from calling activity.
        Intent intent = getIntent();
        final String message = intent.getStringExtra("message");

        // Initialize Class
        Initialize(message);
    }

    /**
     * METHOD
     *
     * NAME
     *      onResume --> Android Studio generated method, this method executes
     *          when the activity becomes active.
     *
     * SYNOPSIS
     *      public void onResume()
     *
     * DESCRIPTION
     *      This method executes when the activity becomes active. It checks if there is
     *      any "pending" data that will update the spinner (Drop down list).
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/5/2019
     */
    @Override
    public void onResume()
    {
        super.onResume();

        // Check if a new task was added. Updates the task spinner (drop down)
        int pendingRecordID = ((TLApplication) this.getApplication()).Getm_pendingRecordID();
        if (pendingRecordID != NOTANINDEXNORID) UpdateSpinner(pendingRecordID);
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      SaveLogData --> Saves the Log data entered.
     *
     * SYNOPSIS
     *      private void SaveLogData()
     *
     * DESCRIPTION
     *      This method saves the entered Log data into a database.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/8/2019
     */
    private void SaveLogData()
    {
        // Set the start and end stamp. Add a minute to the end timestamp
        long time = System.currentTimeMillis();
        Timestamp startTS = new Timestamp(time);
        Timestamp endTS = new Timestamp(time+60000);

        // Save a data into a log object.
        EditText logNotes = findViewById(R.id.editSLogNotes);
        Log startLog = new Log(0,RUNNINGLOG,logNotes.getText().toString()
                ,m_lstTask.get(m_lstTaskChosenIndex));
        startLog.Setm_start(startTS);
        startLog.Setm_end(endTS);

        // Add running log to the database
        boolean updateResult;
        updateResult = m_databaseHelper.AddLog(startLog);
        startLog.Setm_id(m_databaseHelper.GetLastID("log"));
        int action = Getm_actionType();

        // Notify user
        if (updateResult)
        {
            String actionMessage = "Running Log added successfully";
            Toast.makeText(this, actionMessage ,
                    Toast.LENGTH_SHORT).show();

            // Set pending data
            ((TLApplication) this.getApplication()).Setm_pending(startLog.Getm_id()
                    ,Getm_position(),action);

            finish();
        }
        else Toast.makeText(this, "There was an error, please try again",
                Toast.LENGTH_SHORT).show();
    }

    /**
     * METHOD
     *
     * NAME
     *      UpdateSpinner --> Updates the spinner object.
     *
     * SYNOPSIS
     *      private void UpdateSpinner(int a_pendingRecordID)
     *      @param a_pendingRecordID --> int: A DB record ID number.
     *
     * DESCRIPTION
     *      This method updates the spinner(Drop down list) object used to select a task.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/2/2019
     */
    private void UpdateSpinner(int a_pendingRecordID)
    {
        // Get tasks from the DB
        m_lstTask = m_databaseHelper.GetAllTasks();
        Task newTask = m_databaseHelper.GetTask(a_pendingRecordID);

        // Re-Build the string array needed by the spinner
        m_tasks.clear();
        for(Task task : m_lstTask) m_tasks.add(task.Getm_name());
        m_tasks.add("Add New...");
        m_dropDownAdapter.notifyDataSetChanged();

        // Resets pending
        ((TLApplication) this.getApplication()).ResetPending();

        // Set the task to the new task added
        m_dropDown.setSelection(m_dropDownAdapter.getPosition(newTask.Getm_name()));
        m_lstTaskChosenIndex = m_dropDown.getSelectedItemPosition();
        SetTypeData(m_lstTask.get(m_lstTaskChosenIndex));
    }

    /**
     * METHOD
     *
     * NAME
     *      InitializeTaskDropDown --> Initializes the task drop down.
     *
     * SYNOPSIS
     *      private void InitializeTaskDropDown()
     *
     * DESCRIPTION
     *      This method initializes the task drop down.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/5/2019
     */
    private void InitializeTaskDropDown()
    {
        // Get all tasks
        m_lstTask = new ArrayList<>();
        m_lstTask = m_databaseHelper.GetAllTasks();

        // Build the string array needed by the spinner
        m_tasks = new ArrayList<>();
        for(Task task : m_lstTask) m_tasks.add(task.Getm_name());
        m_tasks.add("Add New...");

        // Set spinner
        m_dropDown = findViewById(R.id.spinnerSLogTaskName);
        m_dropDownAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, m_tasks);
        m_dropDown.setAdapter(m_dropDownAdapter);

        m_dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onItemSelected --> Android Studio generated method, this method runs when an
             *          item is selected in the task drop down.
             *
             * SYNOPSIS
             *      public void onItemSelected(AdapterView<?> parent, View view,
             *          int position, long id)
             *      @param a_parent --> AdapterView<?>: Adapter View object.
             *      @param a_view --> View: Calling view object.
             *      @param a_position --> int: Position of a list item
             *      @param a_id --> long: Id of the list item
             *
             * DESCRIPTION
             *      This method runs when an item is selected in the task drop down.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00pm 4/5/2019
             */
            @Override
            public void onItemSelected(AdapterView<?> a_parent, View a_view,
                                       int a_position, long a_id)
            {
                // Check if Add New...was selected
                int addNewPosition = m_dropDownAdapter.getPosition("Add New...");
                if (m_dropDown.getSelectedItemPosition() == addNewPosition)
                {
                    m_dropDown.setSelection(0);
                    // Call the Add Task activity.
                    AddTask();
                }
                else
                {
                    // Store the selected task index and update the type data
                    m_lstTaskChosenIndex = m_dropDown.getSelectedItemPosition();
                    SetTypeData(m_lstTask.get(m_lstTaskChosenIndex));
                }
            }

            // Empty required event definition, not used.
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    /**
     * METHOD
     *
     * NAME
     *      AddTask --> Calls the activity that adds a task.
     *
     * SYNOPSIS
     *      private void AddTask()
     *
     * DESCRIPTION
     *      This method calls the activity used to add a task.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/5/2019
     */
    private void AddTask()
    {
        Intent myIntent = new Intent(this, TaskAEActivity.class);
        myIntent.putExtra("message", "new|0|0" );
        startActivity(myIntent);
    }

    /**
     * METHOD
     *
     * NAME
     *      SetTypeData --> Sets type data.
     *
     * SYNOPSIS
     *      private void SetTypeData(Task a_task)
     *      @param a_task --> int: Drop down selected task.
     *
     * DESCRIPTION
     *      This method sets the type name and color. The type is related to the task chosen.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/24/2019
     */
    private void SetTypeData(Task a_task)
    {
        int typeColor = a_task.Getm_typeColor();
        String typeName = a_task.Getm_typeName();

        // Set the type button color
        Button typeColorButton = findViewById( R.id.buttonSLogTypeColor);
        GradientDrawable drawable = (GradientDrawable) typeColorButton.getBackground();
        drawable.setColor(typeColor);

        // Set the type name
        TextView typeNameText = findViewById(R.id.labelSLogTypeName);
        typeNameText.setText(typeName);
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current activity.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_message --> String: Message passed by the caller.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00am 3/2/2019
     */
    private void Initialize(String a_message)
    {
        // Update the top bar
        SetTopBar();

        // Set super member variables
        SetMemberVariables(a_message);

        // Populate task list
        InitializeTaskDropDown();

        // Sets the floating action button (fab) located on the screen's lower right
        FloatingActionButton fab = findViewById(R.id.fabStart);
        fab.setOnClickListener(new View.OnClickListener()
        {
            /**
             * METHOD
             *
             * NAME
             *      onClick --> Android Studio generated method. Runs on click.
             *
             * SYNOPSIS
             *      public void onClick(View a_view)
             *      @param a_view --> View: Calling view object.
             *
             * DESCRIPTION
             *      This method runs whenever the Save Running task action button is clicked.
             *
             * RETURNS
             *      void
             *
             * AUTHOR
             *      Edgar Garcia
             *
             * DATE
             *      8:00am 3/23/2019
             */
            @Override
            public void onClick(View a_view)
            {
                SaveLogData();
            }
        });

        // Set the clock
        WebView tlClock = findViewById(R.id.start_clock);
        WebSettings webSettings = tlClock.getSettings();
        webSettings.setJavaScriptEnabled(true);
        tlClock.loadUrl("file:///android_asset/TLclockSmall.html");

    }
}
