// Package and libraries used
package com.eg.tasklogger;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * CLASS
 *
 * NAME
 *      ReportResultFragmentByTask
 *
 * SYNOPSIS
 *      public class ReportResultFragmentByTask extends Fragment
 *
 * DESCRIPTION
 *      This class handles the Report Results fragment based on tasks.
 *
 * AUTHOR
 *      Edgar Garcia
 *
 * DATE
 *      8:00pm 4/23/2019
 */
public class ReportResultFragmentByTask extends Fragment
{
    /*
    Order in which this class is set
    1st: Class Constants
    2nd: Class Variables
    3rd: UI Components
    4th: Constructor
    5th: Event handlers
    6th: Selectors
    7th: Mutators
    8th: Any utility (private) methods
    */

    // 1st: Constants -------------------------------------------------------------------------
    // 2nd: Private member variables ----------------------------------------------------------
    // 3rd: UI Components ---------------------------------------------------------------------
    // 4th: Constructor -----------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      ReportResultFragmentByTask --> Class constructor.
     *
     * SYNOPSIS
     *      public ReportResultFragmentByTask()
     *
     * DESCRIPTION
     *      This method is the class constructor.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/23/2019
     */
    public ReportResultFragmentByTask()
    {
        // Required empty public constructor
    }

    // 5th: Event handlers --------------------------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      onCreateView --> Android Studio generated method, this method executes
     *          when the fragment opens.
     *
     * SYNOPSIS
     *      public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
     *          Bundle a_savedInstanceState)
     *      @param a_inflater --> LayoutInflater: "Inflater" used to draw a fragment.
     *      @param a_container --> ViewGroup: Container in which the fragment is held.
     *      @param a_savedInstanceState --> Bundle: If NULL then it means this is a 'fresh' launch
     *          of the Activity. And when it's non-null (if your app saved the data in
     *          onSaveInstanceState(...)), it means the Activity state needs to be recreated.
     *
     * DESCRIPTION
     *      This method creates/updates all objects in the fragment.
     *
     * RETURNS
     *      @return View: Fragment created.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/23/2019
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater a_inflater, ViewGroup a_container,
                             Bundle a_savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = a_inflater.inflate(R.layout.fragment_report_result_fragment_by_task,
                a_container, false);

        // Initialize class
        Initialize(view);

        // Inflate the layout for this fragment
        return view;
    }

    // 6th: Selectors -------------------------------------------------------------------------
    // 7th: Mutators --------------------------------------------------------------------------
    // 8th: Any utility (private) methods -----------------------------------------------------
    /**
     * METHOD
     *
     * NAME
     *      DrawPieChart --> Draws the "By Task" pie chart.
     *
     * SYNOPSIS
     *      private void DrawPieChart(View a_view, List<ReportTask> a_lstTask)
     *      @param a_view --> View: Fragment where the pie chart will be drawn.
     *      @param a_lstTask --> List<ReportTask>: Report Task objects containing pie chart data.
     *
     * DESCRIPTION
     *      This method draws the "By Task" pie chart. It uses the list of ReportTask objects
     *      already calculated for the recycler underneath the pie chart. Basically, the
     *      pie chart is the graphical representation of that list.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      4:00pm 4/28/2019
     */
    private void DrawPieChart(View a_view, List<ReportTask> a_lstTask)
    {
        // Check if there is an activity running
        if (getActivity() == null) return;

        // Format the contents of a_lstType into a JSON object
        String JSONobject = GetListAsJSON(a_lstTask);

        // Get the raw text (html code) used to draw the pie chart
        String JSLibraries = ((ReportResultActivity) getActivity())
                .Getm_JSLibraries();
        String pieCodeData = ((ReportResultActivity) getActivity())
                .GetFileContents("TLoggerPieTemplate.txt");

        // Substitute the title and data tag for the title and JSON object
        String pieTitle = ((ReportResultActivity) getActivity()).Getm_fromToDatePieTitle();
        pieCodeData = pieCodeData.replace("/~#TLReportPieTitle#~/", pieTitle);
        pieCodeData = pieCodeData.replace("/~#TLReportJSON#~/", JSONobject);

        // Set the pie chart web viewer
        WebView pieChart = a_view.findViewById(R.id.reportTask_pie);
        WebSettings webSettings = pieChart.getSettings();
        webSettings.setJavaScriptEnabled(true);
        pieChart.setVerticalScrollBarEnabled(false);
        pieChart.setHorizontalScrollBarEnabled(false);
        pieChart.loadDataWithBaseURL(null,JSLibraries + pieCodeData,
                "text/html", "utf-8", null);
    }

    /**
     * METHOD
     *
     * NAME
     *      GetListAsJSON --> formats the ReporTask list as a JSON.
     *
     * SYNOPSIS
     *      private String GetListAsJSON(List<ReportTask> a_lstTask)
     *      @param a_lstTask --> List<ReportTask>: ReportTask objects containing the data to
     *          represent in the pie chart.
     *
     * DESCRIPTION
     *      This method formats the ReporTask list as a Javascript Object Notation (JSON).
     *      The JSON object is used to set the correct proportion in the pie chart drawn.
     *
     * RETURNS
     *      @return String --> JSON object.
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      8:00pm 4/28/2019
     */
    private String GetListAsJSON(List<ReportTask> a_lstTask)
    {
        String result = "";
        try
        {
            JSONArray jArray = new JSONArray();

            // Loop through all list items
            for (ReportTask rTask : a_lstTask)
            {
                JSONObject resultJSON = new JSONObject();

                // Round the hours to 2 decimal places
                double hours = rTask.Getm_totalTime() * 100;
                hours = Math.round(hours);
                hours = hours / 100;

                // Set and append JSON object to JSON array
                resultJSON.put("name", rTask.Getm_taskName());
                resultJSON.put("y", hours);
                resultJSON.put("color", "#" + rTask.Getm_typeColorHex());
                jArray.put(resultJSON);
            }
            return jArray.toString();
        }
        catch (JSONException jse)
        {
            jse.printStackTrace();
        }
        return result;
    }

    /**
     * METHOD
     *
     * NAME
     *      Initialize --> Initializes objects in the current fragment.
     *
     * SYNOPSIS
     *      private void Initialize()
     *      @param a_view --> View: Fragment being displayed.
     *
     * DESCRIPTION
     *      This method initializes the class and its components.
     *
     * RETURNS
     *      void
     *
     * AUTHOR
     *      Edgar Garcia
     *
     * DATE
     *      10:00am 2/24/2019
     */
    private void Initialize(View a_view)
    {
        // Get the From and To dates stored in the activity holding this fragment
        String fromDate="";
        String toDate="";
        if (getActivity() != null)
        {
            fromDate = ((ReportResultActivity) getActivity()).Getm_fromDate();
            toDate = ((ReportResultActivity) getActivity()).Getm_toDate();
        }

        // Get a report from the database
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        List<ReportTask> lstTask = databaseHelper.GetTaskReport(fromDate, toDate);

        // Set the Recycler/List
        RecyclerView recyclerView = a_view.findViewById(R.id.recyclerView_ReportResultTask);
        ReportTaskRecyclerViewAdapter recyclerAdapter =
                new ReportTaskRecyclerViewAdapter(getContext(),lstTask);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerAdapter);

        // Draw the Pie Chart
        DrawPieChart(a_view, lstTask);
    }
}
